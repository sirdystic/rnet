// rnet.cpp : Console wrapper around Windows Lan Manager Network Management APIs
//
// Copyright 2019, Sir Dystic of Cult of the Dead Cow.  All Rights Reserved.
//
//////////////////////////////////////////////////////////////


#include "Types.h"
#include "Commands.h"
#include "Utils.h"
#include "Help.h"
#include "Utils.h"

#include <Windows.h>
#include <iostream>

#pragma comment(lib, "Netapi32.lib")
#pragma comment(lib, "Advapi32.lib")
#pragma comment(lib, "Version.lib")

// Globals
wstring g_xmlFilename;
wstring g_jsonFilename;
bool g_showCalls = false;

bool ParseCommandLine(int argc, wchar_t** argv, wstring& command, wstring& server, vector<wstring>& arguments)
{
	if (1 == argc)
	{
		Usage();
		return false;
	}

	bool showVersion = false;
	int startArg = 2;

	if (argv[1][0] == L'\\' && argv[1][1] == L'\\')
	{
		if (2 == argc)
		{
			Usage();
			return false;
		}

		server = argv[1];
		command = StringUpper(argv[2]);
		startArg++;
	}
	else
	{
		command = StringUpper(argv[1]);
	}

	for (int x = startArg; x < argc; x++)
	{
		wstring arg(argv[x]);

		if (0 == StringUpper(arg).find(L"/XML:"))
		{
			g_xmlFilename = arg.substr(5);
		}
		else if (0 == StringUpper(arg).find(L"/JSON:"))
		{
			g_jsonFilename = arg.substr(6);
		}
		else if (L"/VERSION" == StringUpper(arg) || L"/VER" == StringUpper(arg))
		{
			showVersion = true;
		}
		else
		{
			arguments.push_back(argv[x]);
		}
	}

	if (showVersion || L"/VERSION" == command || L"/VER" == command)
	{
		ShowVersion();
		exit(0);
	}

	return true;
}


int wmain(int argc, wchar_t** argv)
{
	wstring command;
	wstring server;
	vector<wstring> arguments;

	if (!ParseCommandLine(argc, argv, command, server, arguments))
	{
		return ERROR_BAD_ARGUMENTS;
	}

	commandmap commands = {
		{ L"HELP", Help },
		{ L"SHARE", Share },
		{ L"USER", User },
		{ L"GROUP", Group },
		{ L"LOCALGROUP", LocalGroup },
		{ L"CONFIG", Config },
		{ L"USE", Use },
		{ L"SESSION", Session },
		{ L"SERVICE", Service },
		{ L"DOMAIN", Domain },
		{ L"VIEW", View },
		{ L"NAME", Name },
		{ L"LOGINS", Logins },
		};

	if (commands.find(command) == commands.end())
	{
		wcerr << L"Unknown command: " << command << endl;
		Usage();
		return ERROR_BAD_ARGUMENTS;
	}
	
	return commands[command](server, arguments);
}

