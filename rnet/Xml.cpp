/* Xml.cpp - XML output */

#include "Xml.h"
#include "Utils.h"

#include "tinyxml.h"

bool XmlOutputInfo(const wstring& filename, int level, const wstring& server, const wstring& object,
	const vector<pair<wstring, wstring>>& details, const vector<vector<wstring>>& values,
	const vector<pair<wstring, vector<wstring>>>& extraData)
{
	if (2 != values.size() || values[0].size() != values[1].size())
	{
		cerr << "INTERNAL ERROR IN XMLOUTPUTINFO: BAD VALUES VECTOR" << endl;
		return false;
	}

	TiXmlDocument doc;
	TiXmlDeclaration * decl = new TiXmlDeclaration("1.0", "", "");
	doc.LinkEndChild(decl);
	TiXmlElement * element = new TiXmlElement(WstringToString(object));

	// Add the server if there was one
	if (!server.empty())
		element->SetAttribute("server", WstringToString(server));

	// Add the level if there was one
	if (-1 != level)
		element->SetAttribute("level", level);

	// Add optional attributes
	for (const auto& attr : details)
	{
		element->SetAttribute(WstringToString(get<0>(attr)), WstringToString(get<1>(attr)));
	}

	// Add the actual values
	for (size_t n = 0; n < values[0].size(); n++)
	{
		TiXmlElement * entry = new TiXmlElement(WstringToString(ElementSafeString(values[0][n])));
		TiXmlText * text = new TiXmlText("Name?");
		text->SetValue(WstringToString(values[1][n]));
		entry->LinkEndChild(text);
		element->LinkEndChild(entry);
	}

	// Add optional extra data
	for (const auto& extra : extraData)
	{
		TiXmlElement * list = new TiXmlElement(WstringToString(get<0>(extra) + L"_list"));
		for (const auto& val : get<1>(extra))
		{
			TiXmlElement * entry = new TiXmlElement(WstringToString(get<0>(extra)));
			TiXmlText * text = new TiXmlText("Name?");
			text->SetValue(WstringToString(val));
			entry->LinkEndChild(text);
			list->LinkEndChild(entry);
		}
		element->LinkEndChild(list);
	}

	doc.LinkEndChild(element);
	
	return doc.SaveFile(WstringToString(filename));
}


bool XmlOutputEnum(const wstring& filename, int level, const wstring& server, const wstring& object,
	const vector<wstring>& headers, const vector<vector<wstring>>& values)
{
	TiXmlDocument doc;
	TiXmlDeclaration * decl = new TiXmlDeclaration("1.0", "", "");
	doc.LinkEndChild(decl);
	TiXmlElement * element = new TiXmlElement(WstringToString(object + L"_list"));

	if (!server.empty())
		element->SetAttribute("server", WstringToString(server));

	if (-1 != level)
		element->SetAttribute("level", level);

	for (size_t n = 0; n < values[0].size(); n++)
	{
		TiXmlElement * entry = new TiXmlElement(WstringToString(object));

		for (size_t x = 0; x < headers.size(); x++)
		{
			TiXmlElement * item = new TiXmlElement(WstringToString(ElementSafeString(headers[x])));
			TiXmlText * text = new TiXmlText("Name?");
			text->SetValue(WstringToString(values[x][n]));
			item->LinkEndChild(text);
			entry->LinkEndChild(item);
		}
		element->LinkEndChild(entry);
	}

	doc.LinkEndChild(element);

	return doc.SaveFile(WstringToString(filename));
}

