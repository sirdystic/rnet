/* NetService.cpp - Wrappers around NetService*() APIs */

#include "NetService.h"
#include "Error.h"
#include "Utils.h"
#include "Types.h"
#include "Macros.h"
#include "Xml.h"
#include "Json.h"
#include "Globals.h"

#include <Windows.h>
#include <LM.h>

#include <iostream>

// TODO?  Control services?  Install service?  Probably not.

void GetServiceInfoValues0(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetServiceInfoValues1(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetServiceInfoValues2(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);

vector<int> NetServiceEnumTryLevels = { 0, 1, 2 };
vector<int> NetServiceGetInfoTryLevels = { 0, 1, 2 };
levelmap NetServiceGetValuesMap = {
	{ 0, GetServiceInfoValues0 },
	{ 1, GetServiceInfoValues1 },
	{ 2, GetServiceInfoValues2 }
};


int ServiceEnum(const wstring& server, int level)
{
	wcout << L"NetServiceEnum(" << server << L") level " << level << endl;

	LPBYTE buffer = nullptr;
	DWORD entriesRead = 0;
	DWORD totalEntries = 0;
	DWORD resumeHandle = 0;

	NET_API_STATUS status = NetServiceEnum(
		server.c_str(),
		level,
		&buffer,
		MAX_PREFERRED_LENGTH,
		&entriesRead,
		&totalEntries,
		&resumeHandle
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" enumerating services: " << ErrorString(status) << endl;
		return status;
	}

	vector<wstring> headers;
	vector<vector<wstring>> values;

	char* ptr = reinterpret_cast<char*>(buffer);
	for (DWORD d = 0; d < entriesRead; d++)
	{
		vector<wstring> descriptions;
		vector<wstring> text;

		LEVELMAPCHECK(NetServiceGetValuesMap, level);
		NetServiceGetValuesMap[level](ptr, descriptions, text);

		if (0 == d)
		{
			headers = descriptions;
			values.resize(headers.size());
		}
		for (size_t n = 0; n < text.size(); n++)
		{
			values[n].push_back(text[n]);
		}

		switch (level)
		{
			POINTERSTRUCTINCCASE(0, ptr, SERVICE);
			POINTERSTRUCTINCCASE(1, ptr, SERVICE);
			POINTERSTRUCTINCCASE(2, ptr, SERVICE);
		default:
			wcerr << L"INTERNAL ERROR: MISSING LEVEL IN POINTER INCREMENT SWITCH" << endl;
			exit(ERROR_INTERNAL_ERROR);
		}
	}

	NetApiBufferFree(buffer);

	OutputTable(headers, values);

	if (!g_xmlFilename.empty())
	{
		XmlOutputEnum(g_xmlFilename, level, server, L"service", headers, values);
	}

	if (!g_jsonFilename.empty())
	{
		JsonOutputEnum(g_jsonFilename, level, server, L"service", headers, values);
	}

	return status;
}


wstring ServiceStatusToString(DWORD status)
{
	wstring ret;
	switch (status & SERVICE_INSTALL_STATE)
	{
	case SERVICE_UNINSTALLED:
		ret += L"UnInst ";
		break;
	case SERVICE_INSTALL_PENDING:
		ret += L"InstPend ";
		break;
	case SERVICE_UNINSTALL_PENDING:
		ret += L"UninstPend ";
		break;
	case SERVICE_INSTALLED:
		ret += L"Inst'ed ";
		break;
	}

	switch (status & SERVICE_PAUSE_STATE)
	{
	case LM20_SERVICE_ACTIVE:
		ret += L"Active ";
		break;
	case LM20_SERVICE_CONTINUE_PENDING:
		ret += L"ContPend ";
		break;
	case LM20_SERVICE_PAUSE_PENDING:
		ret += L"PausePend ";
		break;
	case LM20_SERVICE_PAUSED:
		ret += L"Paused ";
		break;
	}

	if (0 == (status & SERVICE_UNINSTALLABLE))
	{
		ret += L"NoUninst ";
	}

	if (0 == (status & SERVICE_PAUSABLE))
	{
		ret += L"NoPause ";
	}

	return ret;
}


int ServiceGetInfo(const wstring& server, const wstring& service, int level)
{
	wcout << L"NetServiceGetInfo(" << server << L", " << service << L") level " << level << endl;

	LPBYTE buffer = nullptr;

	NET_API_STATUS status = NetServiceGetInfo(
		server.c_str(),
		service.c_str(),
		level,
		&buffer
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" getting service info: " << ErrorString(status) << endl;
		return status;
	}

	vector<wstring> descriptions;
	vector<wstring> text;

	LEVELMAPCHECK(NetServiceGetValuesMap, level);
	NetServiceGetValuesMap[level](buffer, descriptions, text);

	vector<vector<wstring>> values;
	values.push_back(descriptions);
	values.push_back(text);

	OutputTable({}, values);

	wcout << endl;

	NetApiBufferFree(buffer);

	if (!g_xmlFilename.empty())
	{
		XmlOutputInfo(
			g_xmlFilename,
			level,
			server,
			L"service",
			{ make_pair(L"service", service) },
			values
		);
	}

	if (!g_jsonFilename.empty())
	{
		JsonOutputInfo(
			g_jsonFilename,
			level,
			server,
			L"service",
			{ make_pair(L"service", service) },
			values
		);
	}

	return status;
}


void GetServiceInfoValues0(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	LPSERVICE_INFO_0 pGroupInfo = reinterpret_cast<LPSERVICE_INFO_0>(buffer);
	descriptions.push_back(L"Name");
	text.push_back(pGroupInfo->svci0_name);
}


void GetServiceInfoValues1(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetServiceInfoValues0(buffer, descriptions, text);

	LPSERVICE_INFO_1 pGroupInfo = reinterpret_cast<LPSERVICE_INFO_1>(buffer);
	descriptions.push_back(L"Status");
	text.push_back(ServiceStatusToString(pGroupInfo->svci1_status));
	descriptions.push_back(L"Code");
	text.push_back(to_wstring(pGroupInfo->svci1_code));
	descriptions.push_back(L"PID");
	text.push_back(to_wstring(pGroupInfo->svci1_pid));
}


void GetServiceInfoValues2(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetServiceInfoValues1(buffer, descriptions, text);

	LPSERVICE_INFO_2 pGroupInfo = reinterpret_cast<LPSERVICE_INFO_2>(buffer);
	descriptions.push_back(L"Text");
	text.push_back(nullptr == pGroupInfo->svci2_text ? L"" : pGroupInfo->svci2_text);
	descriptions.push_back(L"SpecErr");
	text.push_back(to_wstring(pGroupInfo->svci2_specific_error));
	descriptions.push_back(L"Display name");
	text.push_back(nullptr == pGroupInfo->svci2_display_name ? L"" : pGroupInfo->svci2_display_name);
}

