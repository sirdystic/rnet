/* NetShare.cpp - Wrappers around NetShare*() */

#include "NetShare.h"
#include "Error.h"
#include "Utils.h"
#include "Security.h"
#include "Types.h"
#include "Macros.h"
#include "Xml.h"
#include "Json.h"
#include "Globals.h"

#include <Windows.h>
#include <LM.h>

#include <iostream>
#include <sstream>


void GetShareInfoValues0(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetShareInfoValues1(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetShareInfoValues2(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetShareInfoValues501(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetShareInfoValues502(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetShareInfoValues503(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);

vector<int> NetShareEnumTryLevels = { 0, 1, 2, 501, 502, 503 };
vector<int> NetShareGetInfoTryLevels = { 0, 1, 2, 501, 502, 503 };
levelmap NetShareGetValuesMap = { 
	{ 0, GetShareInfoValues0 },
	{ 1, GetShareInfoValues1 },
	{ 2, GetShareInfoValues2 },
	{ 501, GetShareInfoValues501 },
	{ 502, GetShareInfoValues502 },
	{ 503, GetShareInfoValues503 }
};


wstring ShareTypeToString(DWORD type)
{
	wstring ret;
	switch (type & 0xFFFFFF)
	{
	case STYPE_DISKTREE:
		ret = L"DISK";
		break;
	case STYPE_PRINTQ:
		ret = L"PRINTQ";
		break;
	case STYPE_DEVICE:
		ret = L"COMDEVICE";
		break;
	case STYPE_IPC:
		ret = L"IPC$";
		break;
	}

	if (ret.empty())
	{
		wstringstream sstr;
		sstr << L"UNKNOWN (" << hex << type << L")";
		ret = sstr.str();
	}

	if (type & STYPE_SPECIAL)
		ret += L" (Sys)";

	if (type & STYPE_TEMPORARY)
		ret += L" (Tmp)";

	return ret;
}


wstring ShareFlagsToString(DWORD flags)
{
	wstring ret;
	if (flags & STYPE_SPECIAL)
		ret += L"SPECIAL ";
	if (flags & STYPE_TEMPORARY)
		ret += L"TEMP ";
	return ret;
}


void GetShareInfoValues0(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	LPSHARE_INFO_0 pShareInfo = reinterpret_cast<LPSHARE_INFO_0>(buffer);
	descriptions.push_back(L"Share name");
	text.push_back(pShareInfo->shi0_netname);
}


void GetShareInfoValues1(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetShareInfoValues0(buffer, descriptions, text);

	LPSHARE_INFO_1 pShareInfo = reinterpret_cast<LPSHARE_INFO_1>(buffer);
	descriptions.push_back(L"Type");
	text.push_back(ShareTypeToString(pShareInfo->shi1_type));
	descriptions.push_back(L"Remark");
	text.push_back(pShareInfo->shi1_remark);

}

void GetShareInfoValues2(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetShareInfoValues1(buffer, descriptions, text);

	LPSHARE_INFO_2 pShareInfo = reinterpret_cast<LPSHARE_INFO_2>(buffer);
	descriptions.push_back(L"Maximum users");
	text.push_back(pShareInfo->shi2_max_uses == 0xFFFFFFFF ? L"No limit" : to_wstring(pShareInfo->shi2_max_uses));
	descriptions.push_back(L"Current users");
	text.push_back(to_wstring(pShareInfo->shi2_current_uses));
	descriptions.push_back(L"Path");
	text.push_back(pShareInfo->shi2_path);
}


void GetShareInfoValues501(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	LPSHARE_INFO_501 pShareInfo = reinterpret_cast<LPSHARE_INFO_501>(buffer);
	descriptions.push_back(L"Share name");
	text.push_back(pShareInfo->shi501_netname);
	descriptions.push_back(L"Type");
	text.push_back(ShareTypeToString(pShareInfo->shi501_type));
	descriptions.push_back(L"Remark");
	text.push_back(pShareInfo->shi501_remark);
	descriptions.push_back(L"Flags");
	text.push_back(ShareFlagsToString(pShareInfo->shi501_flags));
}


void GetShareInfoValues502(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetShareInfoValues2(buffer, descriptions, text);

	LPSHARE_INFO_502 pShareInfo = reinterpret_cast<LPSHARE_INFO_502>(buffer);
	descriptions.push_back(L"SID");
	text.push_back(nullptr == pShareInfo->shi502_security_descriptor ? L"null" : L"non-null");
}


void GetShareInfoValues503(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetShareInfoValues2(buffer, descriptions, text);

	LPSHARE_INFO_503 pShareInfo = reinterpret_cast<LPSHARE_INFO_503>(buffer);
	descriptions.push_back(L"Server");
	text.push_back(pShareInfo->shi503_servername);
	descriptions.push_back(L"SID");
	text.push_back(nullptr == pShareInfo->shi503_security_descriptor ? L"null" : L"non-null");
}


bool SetShareInfo503Options(const wstring& server, LPSHARE_INFO_503 pShareInfo, const vector<wstring>& options, vector<wstring>& stringTable, bool& sdSet)
{
	vector<tuple<wstring, DWORD>> users;

	for (size_t n = 0; n < options.size(); n++)
	{
		if (0 == StringUpper(options[n]).find(L"/REMARK:"))
		{
			stringTable.push_back(options[n].substr(8));
			pShareInfo->shi503_remark = (LPWSTR)stringTable.rbegin()->c_str();
		}
		else if (0 == StringUpper(options[n]).find(L"/USERS:"))
		{
			pShareInfo->shi503_max_uses = stoul(options[n].substr(7));
		}
		else if (L"/UNLIMITED" == StringUpper(options[n]))
		{
			pShareInfo->shi503_max_uses = 0xFFFFFFFF;
		}
		else if (0 == StringUpper(options[n]).find(L"/GRANT:"))
		{
			wstring entry = options[n].substr(7);
			size_t commaPos = entry.find(L',');
			if (string::npos == commaPos)
			{
				wcerr << L"Bad option: " << options[n] << endl;
				return false;
			}
			else
			{
				wstring user = entry.substr(0, commaPos);
				wstring permStr = StringUpper(entry.substr(commaPos + 1));
				DWORD perm = 0;
				if (L"FULL" == permStr)
				{
					perm = FILE_GENERIC_READ | FILE_GENERIC_EXECUTE | FILE_GENERIC_WRITE | FILE_DELETE_CHILD | DELETE | READ_CONTROL | WRITE_DAC | WRITE_OWNER;
				}
				else if (L"CHANGE" == permStr)
				{
					perm = FILE_GENERIC_READ | FILE_GENERIC_EXECUTE | FILE_GENERIC_WRITE | READ_CONTROL | DELETE;
				}
				else if (L"READ" == permStr)
				{
					perm = FILE_GENERIC_READ | FILE_GENERIC_EXECUTE;
				}
				else
				{
					wcerr << L"Bad option: " << options[n] << endl;
					return false;
				}

				users.push_back(make_tuple(user, perm));
			}
		}
		else
		{
			wcerr << L"Bad option: " << options[n] << endl;
			return false;
		}
	}

	if (!users.empty())
	{
		PSECURITY_DESCRIPTOR pSD = MakeSecurityDescriptor(server, users);
		if (nullptr == pSD)
		{
			return false;
		}
		else
		{
			pShareInfo->shi503_security_descriptor = pSD;
			sdSet = true;
		}
	}

	return true;
}


wstring ShareParamErrorToString(DWORD paramErr)
{
	switch (paramErr)
	{
	case SHARE_NETNAME_PARMNUM:
		return L"Net name";
	case SHARE_TYPE_PARMNUM:
		return L"Type";
	case SHARE_REMARK_PARMNUM:
		return L"Remark";
	case SHARE_PERMISSIONS_PARMNUM:
		return L"Permissions";
	case SHARE_MAX_USES_PARMNUM:
		return L"Max uses";
	case SHARE_CURRENT_USES_PARMNUM:
		return L"Current uses";
	case SHARE_PATH_PARMNUM:
		return L"Path";
	case SHARE_PASSWD_PARMNUM:
		return L"Password";
	case SHARE_FILE_SD_PARMNUM:
		return L"Security descriptor";
	}

	return L"Unknown";
}


int ShareEnum(const wstring& server, int level)
{
	wcout << L"NetShareEnum(" << server << L") level " << level << endl;

	LPBYTE buffer = nullptr;
	DWORD entriesRead = 0;
	DWORD totalEntries = 0;
	DWORD resumeHandle = 0;
	
	NET_API_STATUS status = NetShareEnum(
		(LPWSTR)(server.empty() ? nullptr : server.c_str()),
		level,
		&buffer,
		MAX_PREFERRED_LENGTH,
		&entriesRead,
		&totalEntries,
		&resumeHandle
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" enumerating shares: " << ErrorString(status) << endl;
		return status;
	}

	vector<wstring> headers;
	vector<vector<wstring>> values;

	char* ptr = reinterpret_cast<char*>(buffer);
	for (DWORD d = 0; d < entriesRead; d++)
	{
		vector<wstring> descriptions;
		vector<wstring> text;

		LEVELMAPCHECK(NetShareGetValuesMap, level);
		NetShareGetValuesMap[level](ptr, descriptions, text);

		if (0 == d)
		{
			headers = descriptions;
			values.resize(headers.size());
		}
		for (size_t n = 0; n < text.size(); n++)
		{
			values[n].push_back(text[n]);
		}

		switch (level)
		{
			POINTERSTRUCTINCCASE(0, ptr, SHARE);
			POINTERSTRUCTINCCASE(1, ptr, SHARE);
			POINTERSTRUCTINCCASE(2, ptr, SHARE);
			POINTERSTRUCTINCCASE(501, ptr, SHARE);
			POINTERSTRUCTINCCASE(502, ptr, SHARE);
			POINTERSTRUCTINCCASE(503, ptr, SHARE);
		default:
			wcerr << L"INTERNAL ERROR: MISSING LEVEL IN POINTER INCREMENT SWITCH" << endl;
			exit(ERROR_INTERNAL_ERROR);
			break;
		}
	}

	OutputTable(headers, values);

	if (!g_xmlFilename.empty())
	{
		XmlOutputEnum(g_xmlFilename, level, server, L"share", headers, values);
	}

	if (!g_jsonFilename.empty())
	{
		JsonOutputEnum(g_jsonFilename, level, server, L"share", headers, values);
	}

	NetApiBufferFree(buffer);

	return status;
}


int ShareGetInfo(const wstring& server, const wstring& share, int level)
{
	wcout << L"NetShareGetInfo(" << server << L", " << share << L") level " << level << endl;

	LPBYTE buffer = nullptr;

	NET_API_STATUS status = NetShareGetInfo(
		(LPWSTR)(server.empty() ? nullptr : server.c_str()),
		(LPWSTR)share.c_str(),
		level,
		&buffer
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" getting share info: " << ErrorString(status) << endl;
		return status;
	}

	vector<wstring> descriptions;
	vector<wstring> text;

	LEVELMAPCHECK(NetShareGetValuesMap, level);
	NetShareGetValuesMap[level](buffer, descriptions, text);

	vector<vector<wstring>> values;
	values.push_back(descriptions);
	values.push_back(text);

	OutputTable({}, values);

	if (!g_xmlFilename.empty())
	{
		XmlOutputInfo(
			g_xmlFilename,
			level,
			server,
			L"share",
			{ make_pair(L"share", share) },
			values
		);
	}

	if (!g_jsonFilename.empty())
	{
		JsonOutputInfo(
			g_jsonFilename,
			level,
			server,
			L"share",
			{ make_pair(L"share", share) },
			values
		);
	}

	if (level >= 502)
	{
		PSECURITY_DESCRIPTOR pSD = nullptr;
		switch (level)
		{
		case 502:
			pSD = reinterpret_cast<LPSHARE_INFO_502>(buffer)->shi502_security_descriptor;
			break;
		case 503:
			pSD = reinterpret_cast<LPSHARE_INFO_503>(buffer)->shi503_security_descriptor;
			break;
		default:
			wcerr << L"INTERNAL ERROR: MISSING LEVEL IN POINTER INCREMENT SWITCH" << endl;
			exit(ERROR_INTERNAL_ERROR);
		}

		if (nullptr == pSD)
		{
			wcout << L"Everyone, FULL (null security descriptor)" << endl;
		}
		else
		{
			vector<tuple<wstring, wstring, DWORD>> access = SecurityDescriptorDaclAcesToString(server, pSD);

			vector<wstring> names;
			vector<wstring> types;
			vector<wstring> permissions;

			for (const auto& entry : access)
			{
				names.push_back(get<0>(entry));
				types.push_back(get<1>(entry));
				DWORD perm = get<2>(entry);
				if ((perm & (FILE_GENERIC_WRITE | WRITE_DAC | WRITE_OWNER)) == (FILE_GENERIC_WRITE | WRITE_DAC | WRITE_OWNER))
					permissions.push_back(L"FULL");
				else if ((perm & FILE_GENERIC_WRITE) == FILE_GENERIC_WRITE)
					permissions.push_back(L"CHANGE");
				else if ((perm & FILE_GENERIC_READ) == FILE_GENERIC_READ)
					permissions.push_back(L"READ");
				else
					permissions.push_back(L"NONE");
			}

			vector<wstring> headers;
			headers.push_back(L"Name");
			headers.push_back(L"Type");
			headers.push_back(L"Permission");

			values.clear();
			values.push_back(names);
			values.push_back(types);
			values.push_back(permissions);

			OutputTable(headers, values);
		}
	}

	NetApiBufferFree(buffer);

	return status;
}


int ShareAdd(const wstring& server, const wstring& share, const wstring& path, vector<wstring>& options)
{
	wcout << L"NetShareAdd(" << server << L")" << endl;

	SHARE_INFO_503 shareInfo;
	DWORD paramErr = 0;

	shareInfo.shi503_netname = (LPWSTR)share.c_str();
	shareInfo.shi503_type = STYPE_DISKTREE;
	shareInfo.shi503_remark = nullptr;
	shareInfo.shi503_permissions = 0;
	shareInfo.shi503_max_uses = -1;
	shareInfo.shi503_current_uses = 0;
	shareInfo.shi503_path = (LPWSTR)path.c_str();
	shareInfo.shi503_passwd = nullptr;
	shareInfo.shi503_servername = nullptr;	// L"*" ?
	shareInfo.shi503_reserved = 0;
	shareInfo.shi503_security_descriptor = nullptr;

	vector<wstring> stringTable;
	stringTable.reserve(16);

	bool sdSet = false;
	if (!SetShareInfo503Options(server, &shareInfo, options, stringTable, sdSet))
	{
		return ERROR_INVALID_PARAMETER;
	}

	NET_API_STATUS status = NetShareAdd(
		(LPWSTR)server.c_str(),
		503,
		reinterpret_cast<LPBYTE>(&shareInfo),
		&paramErr
	);

	if (sdSet)
	{
		// Free security descriptor allocated in MakeSecurityDescriptor()
		LocalFree(shareInfo.shi503_security_descriptor);
	}

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" adding share: " << ErrorString(status) << endl;
		if (ERROR_INVALID_PARAMETER == status)
		{
			wcerr << L"Parameter error " << paramErr << L": " << ShareParamErrorToString(paramErr) << endl;
		}
	}
	else
	{
		wcout << L"Share '" << share << L"' added." << endl;
	}

	return status;
}


int ShareDel(const wstring& server, const wstring& share)
{
	wcout << L"NetShareDel(" << server << L", " << share << L")" << endl;

	NET_API_STATUS status = NetShareDel(
		(LPWSTR)server.c_str(),
		(LPWSTR)share.c_str(),
		0);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" deleting share: " << ErrorString(status) << endl;
	}
	else
	{
		wcout << L"Share '" << share << L"' deleted." << endl;
	}

	return status;
}


int ShareSetInfo(const wstring& server, const wstring& share, const vector<wstring>& options)
{
	wcout << L"NetShareGetInfo(" << server << L", " << share << L") level 503" << endl;

	LPBYTE buffer = nullptr;
	NET_API_STATUS status = NetShareGetInfo(
		(LPWSTR)server.c_str(),
		(LPWSTR)share.c_str(),
		503,
		reinterpret_cast<LPBYTE*>(&buffer)
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" getting share info: " << ErrorString(status) << endl;
		return status;
	}

	vector<wstring> stringTable;
	stringTable.reserve(16);

	bool sdSet = false;
	if (!SetShareInfo503Options(server, reinterpret_cast<LPSHARE_INFO_503>(buffer), options, stringTable, sdSet))
	{
		return ERROR_INVALID_PARAMETER;
	}

	wcout << L"NetShareSetInfo(" << server << L", " << share << L") level 503" << endl;

	DWORD paramErr = 0;
	status = NetShareSetInfo(
		(LPWSTR)server.c_str(),
		(LPWSTR)share.c_str(),
		503,
		buffer,
		&paramErr
	);

	if (sdSet)
	{
		// Free security descriptor allocated in MakeSecurityDescriptor()
		LocalFree(reinterpret_cast<LPSHARE_INFO_503>(buffer)->shi503_security_descriptor);
	}

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" setting share info: " << ErrorString(status) << endl;
		if (ERROR_INVALID_PARAMETER == status)
		{
			wcerr << L"Parameter error " << paramErr << L": " << ShareParamErrorToString(paramErr) << endl;
		}
	}
	else
	{
		wcout << L"Share '" << share << L"' info set." << endl;
	}

	NetApiBufferFree(buffer);

	return status;
}


