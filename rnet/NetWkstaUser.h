#pragma once

/* NetWkstaUser.h - Wrappers around NetWkstaUser*() APIs */

#include <string>
#include <vector>

using namespace std;

extern vector<int> NetWkstaUserEnumTryLevels;

int WkstaUserEnum(const wstring& server, int level);
