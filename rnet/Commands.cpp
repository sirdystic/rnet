/* Commands.cpp - Command processing functions */

#include "Types.h"
#include "Help.h"
#include "Commands.h"
#include "Utils.h"
#include "NetShare.h"
#include "NetUser.h"
#include "NetLocalGroup.h"
#include "NetGroup.h"
#include "NetSession.h"
#include "NetService.h"
#include "NetUse.h"
#include "NetServer.h"
#include "NetWksta.h"
#include "NetWkstaUser.h"
#include "NetComputerName.h"
#include "NetDomain.h"

#include <iostream>


int LevelCall_wrapped(int level, const wchar_t *levelName, const wchar_t* functionName,
	const vector<int>& tryLevels, const vector<int>& validLevels, const function<int(int)>& func)
{
	try
	{
		if (-1 == level)
		{
			int ret = 0;
			for (auto l = tryLevels.rbegin(); l != tryLevels.rend(); l++)
			{
				ret = func(*l);
#if 1
				if (0 == ret)
					return 0;
#else
				if (ERROR_INVALID_LEVEL != ret && ERROR_ACCESS_DENIED != ret)
					return ret;
#endif
			}
			return ret;
		}
		else
		{
			if (find(validLevels.begin(), validLevels.end(), level) == validLevels.end())
			{
				wcerr << L"Invalid " << functionName << " << level: " << level << endl;
				Levels(levelName);
				return ERROR_INVALID_LEVEL;
			}
			return func(level);
		}
	}
	catch (exception & e)
	{
		wcerr << L"EXCEPTION CALLING FUNCTION " << functionName << L": " << endl;
		cerr << e.what() << endl;
		exit(ERROR_INTERNAL_ERROR);
	}
}


int LevelCall(int level, const wchar_t *levelName, const wchar_t* functionName,
	const vector<int>& tryLevels, const vector<int>& validLevels, const function<int(int)>& func)
{
	__try
	{
		return LevelCall_wrapped(level, levelName, functionName,
			tryLevels, validLevels, func);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		wcerr << L"SEH EXCEPTION CALLING FUNCTION " << functionName << endl;
		exit(ERROR_INTERNAL_ERROR);
	}
}


int Group(const wstring& server, const vector<wstring>& arguments)
{
	int level = -1;
	wstring group;
	wstring comment;
	vector<wstring> users;
	bool addGroup = false;
	bool delGroup = false;
	bool addUsers = false;
	bool delUsers = false;
	bool setComment = false;

	for (size_t n = 0; n < arguments.size(); n++)
	{
		if (!arguments[n].empty() && arguments[n].at(0) != L'/')
		{
			if (group.empty())
			{
				group = arguments[n];
			}
			else
			{
				users.push_back(arguments[n]);
			}
		}
		else
		{
			if (0 == StringUpper(arguments[n]).find(L"/LEVEL:"))
			{
				level = stoi(arguments[n].substr(7));
			}
			else if (L"/ADD" == StringUpper(arguments[n]))
			{
				if (n == 1)
				{
					addGroup = true;
				}
				else
				{
					if (users.empty() || n != arguments.size() - 1)
					{
						Help(L"", { L"GROUP" });
						return ERROR_BAD_ARGUMENTS;
					}
					else
					{
						addUsers = true;
					}
				}
			}
			else if (L"/DELETE" == StringUpper(arguments[n]) ||
				L"/D" == StringUpper(arguments[n]))
			{
				if (n == 1)
				{
					delGroup = true;
				}
				else
				{
					if (users.empty() || n != arguments.size() - 1)
					{
						Help(L"", { L"GROUP" });
						return ERROR_BAD_ARGUMENTS;
					}
					else
					{
						delUsers = true;
					}
				}
			}
			else if (L"/COMMENT" == StringUpper(arguments[n]))
			{
				if (group.empty())
				{
					Help(L"", { L"GROUP" });
					return ERROR_BAD_ARGUMENTS;
				}
				else
				{
					comment = arguments[n];
					setComment = true;
				}
			}
			else
			{
				Help(L"", { L"GROUP" });
				return ERROR_BAD_ARGUMENTS;
			}
		}
	}

	if (addGroup)
	{
		return GroupAdd(server, group, comment);
	}
	else if (delGroup)
	{
		return GroupDel(server, group);
	}
	else if (setComment)
	{
		return GroupSetInfo(server, group, comment);
	}
	else if (addUsers)
	{
		return GroupAddUsers(server, group, users);
	}
	else if (delUsers)
	{
		return GroupDelUsers(server, group, users);
	}
	else if (group.empty())
	{
		return LevelCall(
			level,
			L"GROUP",
			L"NetGroupEnum",
			NetGroupEnumTryLevels,
			NetGroupEnumTryLevels,
			[server](int lvl) { return GroupEnum(server, lvl); }
		);
	}
	else
	{
		return LevelCall(
			level,
			L"GROUP",
			L"NetGroupGetInfo",
			NetGroupGetInfoTryLevels,
			NetGroupGetInfoTryLevels,
			[server, group](int lvl) { return GroupGetInfo(server, group, lvl); }
		);
	}

	return ERROR_BAD_ARGUMENTS;
}


int LocalGroup(const wstring& server, const vector<wstring>& arguments)
{
	int level = -1;
	wstring group;
	wstring comment;
	vector<wstring> members;
	bool addGroup = false;
	bool delGroup = false;
	bool addMembers = false;
	bool delMembers = false;
	bool setComment = false;

	for (size_t n = 0; n < arguments.size(); n++)
	{
		if (!arguments[n].empty() && arguments[n].at(0) != L'/')
		{
			if (group.empty())
			{
				group = arguments[n];
			}
			else
			{
				members.push_back(arguments[n]);
			}
		}
		else
		{
			if (0 == StringUpper(arguments[n]).find(L"/LEVEL:"))
			{
				level = stoi(arguments[n].substr(7));
			}
			else if (L"/ADD" == StringUpper(arguments[n]))
			{
				if (n == 1)
				{
					addGroup = true;
				}
				else
				{
					if (members.empty() || n != arguments.size() - 1)
					{
						Help(L"", { L"LOCALGROUP" });
						return ERROR_BAD_ARGUMENTS;
					}
					else
					{
						addMembers = true;
					}
				}
			}
			else if (L"/DELETE" == StringUpper(arguments[n]) ||
				L"/D" == StringUpper(arguments[n]))
			{
				if (n == 1)
				{
					delGroup = true;
				}
				else
				{
					if (members.empty() || n != arguments.size() - 1)
					{
						Help(L"", { L"LOCALGROUP" });
						return ERROR_BAD_ARGUMENTS;
					}
					else
					{
						delMembers = true;
					}
				}
			}
			else if (L"/COMMENT" == StringUpper(arguments[n]))
			{
				comment = arguments[n];
				setComment = true;
			}
			else
			{
				Help(L"", { L"LOCALGROUP" });
				return ERROR_BAD_ARGUMENTS;
			}
		}
	}

	if (addGroup)
	{
		return LocalGroupAdd(server, group, comment);
	}
	else if (delGroup)
	{
		return LocalGroupDel(server, group);
	}
	else if (setComment)
	{
		return LocalGroupSetInfo(server, group, comment);
	}
	else if (addMembers)
	{
		return LocalGroupAddMembers(server, group, members);
	}
	else if (delMembers)
	{
		return LocalGroupDelMembers(server, group, members);
	}
	else if (!group.empty())
	{
		return LevelCall(
			level,
			L"LOCALGROUP",
			L"NetLocalGroupGetInfo",
			NetLocalGroupGetInfoTryLevels,
			NetLocalGroupGetInfoValidLevels,
			[server, group](int lvl) { return LocalGroupGetInfo(server, group, lvl); }
		);
	}

	return LevelCall(
		level,
		L"LOCALGROUP",
		L"NetLocalGroupEnum",
		NetLocalGroupEnumTryLevels,
		NetLocalGroupEnumTryLevels,
		[server](int lvl) { return LocalGroupEnum(server, lvl); }
	);
}


int Config(const wstring& server, const vector<wstring>& arguments)
{
	int level = -1;
	wstring service;
	vector<wstring> options;
	bool setInfo = false;

	for (size_t n = 0; n < arguments.size(); n++)
	{
		if (0 == n)
		{
			service = StringUpper(arguments[n]);
		}
		else
		{
			if (!arguments[n].empty() && arguments[n].at(0) != L'/')
			{
				Help(L"", { L"CONFIG" });
				return ERROR_BAD_ARGUMENTS;
			}
			else if (0 == StringUpper(arguments[n]).find(L"/LEVEL:"))
			{
				level = stoi(arguments[n].substr(7));
			}
			else if (0 == StringUpper(arguments[n]).find(L"/AUTODISCONNECT:") ||
				0 == StringUpper(arguments[n]).find(L"/SRVCOMMENT:") ||
				0 == StringUpper(arguments[n]).find(L"/HIDDEN:"))
			{
				options.push_back(arguments[n]);
				setInfo = true;
			}
			else
			{
				Help(L"", { L"CONFIG" });
				return ERROR_BAD_ARGUMENTS;
			}
		}
	}

	if (L"SERVER" == service)
	{
		if (setInfo)
		{
			return ServerSetInfo(server, options);
		}
		else
		{
			return LevelCall(
				level,
				L"CONFIG",
				L"NetServerGetInfo",
				NetServerGetInfoTryLevels,
				NetServerGetInfoTryLevels,
				[server](int lvl) { return ServerGetInfo(server, lvl); }
			);
		}

	}
	else if (L"WORKSTATION" == service)
	{
		if (!options.empty())
		{
			Help(L"", { L"CONFIG", L"WORKSTATION" });
			return ERROR_BAD_ARGUMENTS;
		}
		else
		{
			return LevelCall(
				level,
				L"CONFIG",
				L"NetWkstaGetInfo",
				NetWkstaGetInfoTryLevels,
				NetWkstaGetInfoTryLevels,
				[server](int lvl) { return WkstaGetInfo(server, lvl); }
			);
		}
	}

	Help(L"", { L"CONFIG" });
	return ERROR_BAD_ARGUMENTS;
}


int View(const wstring& server, const vector<wstring>& arguments)
{
	int level = -1;
	wstring domain;

	for (size_t n = 0; n < arguments.size(); n++)
	{
		if (!arguments[n].empty() && arguments[n].at(0) != L'/')
		{
			wcout << L"Use RNET ENUM to view shares on a remote machine" << endl;
			Help(L"", { L"VIEW" });
			return ERROR_BAD_ARGUMENTS;
		}
		else if (0 == StringUpper(arguments[n]).find(L"/LEVEL:"))
		{
			level = stoi(arguments[n].substr(7));
		}
		else if (0 == StringUpper(arguments[n]).find(L"/DOMAIN:"))
		{
			if (!domain.empty())
			{
				Help(L"", { L"VIEW" });
				return ERROR_BAD_ARGUMENTS;
			}
			else
			{
				domain = arguments[n].substr(8);
			}
		}
		else
		{
			Help(L"", { L"VIEW" });
			return ERROR_BAD_ARGUMENTS;
		}
	}

	return LevelCall(
		level,
		L"VIEW",
		L"NetServerEnum",
		NetServerEnumTryLevels,
		NetServerEnumTryLevels,
		[server, domain](int lvl) { return ServerEnum(server, domain, lvl); }
	);
}


int Session(const wstring& server, const vector<wstring>& arguments)
{
	int level = -1;
	wstring computer;
	wstring user;
	bool del = false;

	for (size_t n = 0; n < arguments.size(); n++)
	{
		if (!arguments[n].empty() && arguments[n].at(0) != L'/')
		{
			if (arguments[n].length() > 2 && arguments[n][0] == L'\\' && arguments[n][1] == L'\\')
			{
				if (!computer.empty())
				{
					Help(L"", { L"SESSION" });
					return ERROR_BAD_ARGUMENTS;
				}
				else
				{
					computer = arguments[n];
				}
			}
			else
			{
				if (!user.empty())
				{
					Help(L"", { L"SESSION" });
					return ERROR_BAD_ARGUMENTS;
				}
				else
				{
					user = arguments[n];
				}
			}
		}
		else
		{
			if (0 == StringUpper(arguments[n]).find(L"/LEVEL:"))
			{
				level = stoi(arguments[n].substr(7));
			}
			else if (L"/DELETE" == StringUpper(arguments[n]) ||
				L"/D" == StringUpper(arguments[n]))
			{
				if (n != arguments.size() - 1)
				{
					Help(L"", { L"SESSION" });
					return ERROR_BAD_ARGUMENTS;
				}
				else
				{
					del = true;
				}
			}
			else
			{
				Help(L"", { L"SESSION" });
				return ERROR_BAD_ARGUMENTS;
			}
		}
	}


	if (del)
	{
		return SessionDel(server, computer, user);
	}

	if (!computer.empty() && !user.empty())
	{
		return LevelCall(
			level,
			L"SESSION",
			L"NetSessionGetInfo",
			NetSessionGetInfoTryLevels,
			NetSessionGetInfoTryLevels,
			[server, computer, user](int lvl) { return SessionGetInfo(server, computer, user, lvl); }
		);
	}

	return LevelCall(
		level,
		L"SESSION",
		L"NetSessionEnum",
		NetSessionEnumTryLevels,
		NetSessionEnumTryLevels,
		[server, computer, user](int lvl) { return SessionEnum(server, computer, user, lvl); }
	);
}


int Service(const wstring& server, const vector<wstring>& arguments)
{
	int level = -1;
	wstring service;

	for (size_t n = 0; n < arguments.size(); n++)
	{
		if (!arguments[n].empty() && arguments[n].at(0) != L'/')
		{
			service = arguments[n];
		}
		else if (0 == StringUpper(arguments[n]).find(L"/LEVEL:"))
		{
			level = stoi(arguments[n].substr(7));
		}

	}

	if (service.empty())
	{
		return LevelCall(
			level,
			L"SERVICE",
			L"NetServiceEnum",
			NetServiceEnumTryLevels,
			NetServiceEnumTryLevels,
			[server](int lvl) { return ServiceEnum(server, lvl); }
		);
	}
	else
	{
		return LevelCall(
			level,
			L"SERVICE",
			L"NetServiceGetInfo",
			NetServiceGetInfoTryLevels,
			NetServiceGetInfoTryLevels,
			[server, service](int lvl) { return ServiceGetInfo(server, service, lvl); }
		);
	}


	//return ERROR_BAD_ARGUMENTS;
}


int Share(const wstring& server, const vector<wstring>& arguments)
{
	int level = -1;
	wstring share;
	wstring path;
	bool add = false;
	bool del = false;
	vector<wstring> options;

	for (size_t n = 0; n < arguments.size(); n++)
	{
		if (!arguments[n].empty() && arguments[n].at(0) != L'/')
		{
			wstring arg = arguments[0];
			size_t eqPos = arg.find(L'=');
			if (string::npos == eqPos)
			{
				share = arg;
			}
			else
			{
				share = arg.substr(0, eqPos);
				path = arg.substr(eqPos + 1);
				add = true;
			}
		}
		else if (0 == StringUpper(arguments[n]).find(L"/LEVEL:"))
		{
			level = stoi(arguments[n].substr(7));
		}
		else if (L"/DELETE" == StringUpper(arguments[n]) ||
			L"/D" == StringUpper(arguments[n]))
		{
			del = true;
		}
		else if (0 == StringUpper(arguments[n]).find(L"/REMARK:") ||
			0 == StringUpper(arguments[n]).find(L"/GRANT:") ||
			0 == StringUpper(arguments[n]).find(L"/USERS:") ||
			0 == StringUpper(arguments[n]).find(L"/UNLIMITED"))
		{
			options.push_back(arguments[n]);
		}
		else
		{
			Help(L"", { L"SHARE" });
			return ERROR_BAD_ARGUMENTS;
		}
	}

	if (share.empty())
	{
		return LevelCall(
			level,
			L"SHARE",
			L"NetShareEnum",
			NetShareEnumTryLevels,
			NetShareEnumTryLevels,
			[server](int lvl) { return ShareEnum(server, lvl); }
		);
	}
	else if (1 == arguments.size() && !add)
	{
		return LevelCall(
			level,
			L"SHARE",
			L"NetShareGetInfo",
			NetShareGetInfoTryLevels,
			NetShareGetInfoTryLevels,
			[server, share](int lvl) { return ShareGetInfo(server, share, lvl); }
		);
	}
	else if (add)
	{
		return ShareAdd(server, share, path, options);
	}
	else if (del)
	{
		return ShareDel(server, share);
	}
	else
	{
		return ShareSetInfo(server, share, options);
	}

	return ERROR_BAD_ARGUMENTS;
}


int Use(const wstring& server, const vector<wstring>& arguments)
{
	int level = -1;
	wstring use;
	wstring remote;
	nullstring user;
	wstring domain;
	nullstring password;
	bool add = false;
	bool del = false;

	for (size_t n = 0; n < arguments.size(); n++)
	{
		if (arguments[n].empty() || (!arguments[n].empty() && arguments[n].at(0) != L'/'))
		{
			if (remote.empty() && use.empty())
			{
				if (arguments[n].length() > 2 && arguments[n][0] == L'\\' && arguments[n][1] == L'\\')
				{
					remote = arguments[n];
				}
				else
				{
					use = arguments[n];
				}
			}
			else if (remote.empty())
			{
				remote = arguments[n];
			}
			else if (!password.is_set())
			{
				password = arguments[n];
			}
			else
			{
				Help(L"", { L"USE" });
				return ERROR_BAD_ARGUMENTS;
			}
		}
		else
		{
			if (0 == StringUpper(arguments[n]).find(L"/LEVEL:"))
			{
				level = stoi(arguments[n].substr(7));
			}
			else if (0 == StringUpper(arguments[n]).find(L"/USER:"))
			{
				wstring arg = arguments[n].substr(6);
				size_t slashPos = arg.find(L'\\');
				if (string::npos == slashPos)
				{
					user = arg;
				}
				else
				{
					domain = arg.substr(0, slashPos);
					user = arg.substr(slashPos + 1);
				}
			}
			else if (L"/DELETE" == StringUpper(arguments[n]) ||
				L"/D" == StringUpper(arguments[n]))
			{
				if (use.empty() && remote.empty())
				{
					Help(L"", { L"USE" });
					return ERROR_BAD_ARGUMENTS;
				}
				else
				{
					del = true;
				}
			}
			else
			{
				Help(L"", { L"USE" });
				return ERROR_BAD_ARGUMENTS;
			}
		}
	}

	if (!del && !add && !remote.empty())
	{
		add = true;
	}

	if (password == L"*")
	{
		password = GetPassword(L"Type the password for " + remote + L":");
	}

	if (arguments.empty())
	{
		return LevelCall(
			level,
			L"USE",
			L"NetUseEnum",
			NetUseEnumTryLevels,
			NetUseEnumTryLevels,
			[server](int lvl) { return UseEnum(server, lvl); }
		);
	}
	else if (add)
	{
		return UseAdd(server, use, remote, user, domain, password);
	}
	else if (del)
	{
		return UseDel(server, use.empty() ? remote : use);
	}
	else
	{
		return LevelCall(
			level,
			L"USE",
			L"NetUseGetInfo",
			NetUseGetInfoTryLevels,
			NetUseGetInfoTryLevels,
			[server, use](int lvl) { return UseGetInfo(server, use, lvl); }
		);
	}

	return 0;
}


int User(const wstring& server, const vector<wstring>& arguments)
{
	int level = -1;
	wstring user;
	wstring password;
	bool setPassword = false;
	bool add = false;
	bool del = false;
	bool set = false;
	bool setactive = false;
	bool active = true;
	vector<wstring> options;

	for (size_t n = 0; n < arguments.size(); n++)
	{
		if (!arguments[n].empty() && arguments[n].at(0) != L'/')
		{
			if (user.empty())
			{
				user = arguments[n];
			}
			else if (password.empty())
			{
				password = arguments[n];
				setPassword = true;
			}
			else
			{
				// Password must be in second position
				Help(L"", { L"USER" });
				return ERROR_BAD_ARGUMENTS;
			}
		}
		else
		{
			if (0 == StringUpper(arguments[n]).find(L"/LEVEL:"))
			{
				level = stoi(arguments[n].substr(7));
			}
			else if (L"/ADD" == StringUpper(arguments[n]))
			{
				if (2 != n)
				{
					Help(L"", { L"USER" });
					return ERROR_BAD_ARGUMENTS;
				}
				else
				{
					add = true;
				}
			}
			else if (L"/DELETE" == StringUpper(arguments[n]) ||
				L"/D" == StringUpper(arguments[n]))
			{
				if (1 != n)
				{
					Help(L"", { L"USER" });
					return ERROR_BAD_ARGUMENTS;
				}
				else
				{
					del = true;
				}
			}
			else if (0 == StringUpper(arguments[n]).find(L"/ACTIVE"))
			{
				if (1 != n)
				{
					Help(L"", { L"USER" });
					return ERROR_BAD_ARGUMENTS;
				}
				else
				{
					setactive = true;
					if (L"/ACTIVE" == StringUpper(arguments[n]) ||
						L"/ACTIVE:YES" == StringUpper(arguments[n]))
					{
						active = true;
					}
					else if (L"/ACTIVE:NO" == StringUpper(arguments[n]))
					{
						active = false;
					}
					else
					{
						Help(L"", { L"USER" });
						return ERROR_BAD_ARGUMENTS;
					}
				}
			}
			else if (0 == StringUpper(arguments[n]).find(L"/COMMENT:") ||
				0 == StringUpper(arguments[n]).find(L"/FULLNAME:") ||
				0 == StringUpper(arguments[n]).find(L"/USERCOMMENT:") ||
				0 == StringUpper(arguments[n]).find(L"/HOMEDIR:") ||
				0 == StringUpper(arguments[n]).find(L"/PASSWORDCHG:") ||
				0 == StringUpper(arguments[n]).find(L"/PASSWORDREQ:") ||
				0 == StringUpper(arguments[n]).find(L"/LOGONPASSWORDCHG:") ||
				0 == StringUpper(arguments[n]).find(L"/PROFILEPATH") ||
				0 == StringUpper(arguments[n]).find(L"/PROFILEPATH:") ||
				0 == StringUpper(arguments[n]).find(L"/SCRIPTPATH:") ||
				0 == StringUpper(arguments[n]).find(L"/COUNTRYCODE:") ||
				0 == StringUpper(arguments[n]).find(L"/CODEPAGE:") ||
				0 == StringUpper(arguments[n]).find(L"/EXPIRES:") ||
				0 == StringUpper(arguments[n]).find(L"/WORKSTATIONS:")
				)
			{
				options.push_back(arguments[n]);
				set = true;
			}
			else
			{
				Help(L"", { L"USER" });
				return ERROR_BAD_ARGUMENTS;
			}
		}
	}

	if (password == L"*")
	{
		password = GetPassword(L"Enter the password for user " + user + L":");
	}

	if (user.empty())
	{
		return LevelCall(
			level,
			L"USER",
			L"NetUserEnum",
			NetUserEnumTryLevels,
			NetUserEnumTryLevels,
			[server](int lvl) { return UserEnum(server, lvl); }
		);
	}
	else if (del)
	{
		return UserDel(server, user);

	}
	else if (add)
	{
		return UserAdd(server, user, password, options);
	}
	else if (set || setPassword)
	{
		return UserSetInfo(server, user, password, setPassword, options);
	}

	return LevelCall(
		level,
		L"USER",
		L"NetUserGetInfo",
		NetUserGetInfoTryLevels,
		NetUserGetInfoValidLevels,
		[server, user](int lvl) { return UserGetInfo(server, user, lvl); }
	);
}


int Name(const wstring& server, const vector<wstring>& arguments)
{
	wstring name;
	bool add = false;
	bool del = false;

	if (0 != arguments.size() && 2 != arguments.size())
	{
		Help(L"", { L"NAME" });
		return ERROR_BAD_ARGUMENTS;
	}
	else if (2 == arguments.size())
	{
		if (L"/ADD" == StringUpper(arguments[1]))
		{
			add = true;
		}
		else if (L"/DELETE" == StringUpper(arguments[1]) ||
			L"/D" == StringUpper(arguments[1]))
		{
			del = true;
		}
		else
		{
			Help(L"", { L"NAME" });
			return ERROR_BAD_ARGUMENTS;
		}

		name = arguments[0];
	}

	if (arguments.empty())
	{
		EnumerateComputerNames(server);
	}
	else if (add)
	{
		AddAlternateComputerName(server, name);
	}
	else if (del)
	{
		RemoveAlternateComputerName(server, name);
	}

	return ERROR_BAD_ARGUMENTS;
}


int Domain(const wstring& server, const vector<wstring>& arguments)
{
	if (arguments.empty())
	{
		Help(L"", { L"DOMAIN" });
		return ERROR_BAD_ARGUMENTS;
	}

	wstring domain;
	nullstring user;
	nullstring password;
	bool getAnyDC = false;
	bool join = false;
	bool unjoin = false;

	for (size_t n = 0; n < arguments.size(); n++)
	{
		if (!arguments[n].empty() && arguments[n].at(0) != L'/')
		{
			if (0 == n)
			{
				domain = arguments[n];
			}
			else
			{
				if (user.is_set())
				{
					Help(L"", { L"DOMAIN" });
					return ERROR_BAD_ARGUMENTS;
				}
				else
				{
					password = arguments[n];
				}
			}
		}
		else if (L"/JOIN" == StringUpper(arguments[n]))
		{
			if (1 != n)
			{
				Help(L"", { L"DOMAIN" });
				return ERROR_BAD_ARGUMENTS;
			}

			join = true;
		}
		else if (L"/UNJOIN" == StringUpper(arguments[n]))
		{
			if (0 != n)
			{
				Help(L"", { L"DOMAIN" });
				return ERROR_BAD_ARGUMENTS;
			}

			unjoin = true;
		}
		else if (0 == StringUpper(arguments[n]).find(L"/USER:"))
		{
			user = arguments[n].substr(6);
		}
		else
		{
			Help(L"", { L"DOMAIN" });
			return ERROR_BAD_ARGUMENTS;
		}
	}

	if (password == L"*")
	{
		password = GetPassword(L"Enter the password for user " + user.str() + L":");
	}

	if (join)
	{
		return JoinDomain(server, domain, user, password);
	}
	else if (unjoin)
	{
		return UnjoinDomain(server, user, password);
	}
	else
	{
		GetDCName(server, domain);
		return GetAnyDCName(server, domain);
	}

	return ERROR_BAD_ARGUMENTS;
}


int Logins(const wstring& server, const vector<wstring>& arguments)
{
	int level = -1;

	for (size_t n = 0; n < arguments.size(); n++)
	{
		if (0 == StringUpper(arguments[n]).find(L"/LEVEL:"))
		{
			level = stoi(arguments[n].substr(7));
		}
		else
		{
			Help(L"", { L"LOGINS" });
			return ERROR_BAD_ARGUMENTS;
		}
	}

	return LevelCall(
		level, 
		L"LOGINS", 
		L"NetWkstaUserEnum",
		NetWkstaUserEnumTryLevels,
		NetWkstaUserEnumTryLevels,
		[server](int lvl) { return WkstaUserEnum(server, lvl); }
	);
}

