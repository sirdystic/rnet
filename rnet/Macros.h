#pragma once

/* Macros.h - Global macros */

#define POINTERSTRUCTINCCASE(level, ptr, type) case level: ptr += sizeof(type##_INFO_##level); break;
#define LEVELMAPCHECK(levelmap, level) if (levelmap.find(level) == levelmap.end()) { cerr << "INTERNAL ERROR LEVEL NOT IN LEVELMAP IN " << __FUNCTION__ << endl; exit(ERROR_INTERNAL_ERROR); }

