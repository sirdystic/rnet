#pragma once

/* Commands.h - Command processing functions */

#include <string>
#include <vector>

using namespace std;

int Group(const wstring& server, const vector<wstring>& arguments);
int LocalGroup(const wstring& server, const vector<wstring>& arguments);
int View(const wstring& server, const vector<wstring>& arguments);
int Config(const wstring& server, const vector<wstring>& arguments);
int Session(const wstring& server, const vector<wstring>& arguments);
int Service(const wstring& server, const vector<wstring>& arguments);
int Share(const wstring& server, const vector<wstring>& arguments);
int Use(const wstring& server, const vector<wstring>& arguments);
int User(const wstring& server, const vector<wstring>& arguments);
int Name(const wstring& server, const vector<wstring>& arguments);
int Domain(const wstring& server, const vector<wstring>& arguments);
int Logins(const wstring& server, const vector<wstring>& arguments);

