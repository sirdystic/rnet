/* Error.cpp - Error related functions */

#include "Error.h"

#include <Windows.h>

wstring ErrorString(DWORD error)
{
	LPWSTR buffer = nullptr;

	if (0 == FormatMessageW(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		nullptr,
		error,
		0,
		reinterpret_cast<LPWSTR>(&buffer),
		0,
		nullptr))
	{
		return L"(Unknown error)";
	}

	wstring ret(buffer);
	LocalFree(buffer);
	return ret;
}

