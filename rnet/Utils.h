#pragma once

/* Utils.h - Utility functions */

#include <Windows.h>

#include <string>
#include <vector>

using namespace std;

size_t LongestString(const vector<wstring>& vec);
void OutputRow(const vector<size_t>& lengths, const vector<wstring>& strings);
void OutputTable(const vector<wstring>& headers, const vector<vector<wstring>>& values);
wstring StringUpper(const wstring& str);
wstring StringLower(const wstring& str);
wstring TimeToString(DWORD time);
DWORD StringToTime(const wstring& str);
wstring SecondsToString(DWORD seconds);
wstring ServerVersionToString(DWORD verMajor, DWORD verMinor);
wstring GetCodePageName(DWORD codepage);
string WstringToString(const wstring& str);
wstring ElementSafeString(const wstring& str);
wstring GetPassword(const wstring& prompt, bool show_asterisk = false);
wstring GetVersionString();
