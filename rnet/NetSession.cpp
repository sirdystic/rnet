/* NetSession.cpp - Wrappers around NetSession*() APIs */

#include "NetSession.h"
#include "Utils.h"
#include "Error.h"
#include "Types.h"
#include "Macros.h"
#include "Xml.h"
#include "Json.h"
#include "Globals.h"

#include <Windows.h>
#include <LM.h>

#include <iostream>


void GetSessionInfoValues0(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetSessionInfoValues1(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetSessionInfoValues2(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetSessionInfoValues10(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetSessionInfoValues502(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);

vector<int> NetSessionEnumTryLevels = { 0, 10, 1, 2, 502 };
vector<int> NetSessionGetInfoTryLevels = { 0, 10, 1, 2, 502 };
levelmap NetSessionGetValuesMap = {
	{ 0, GetSessionInfoValues0 },
	{ 1, GetSessionInfoValues1 },
	{ 2, GetSessionInfoValues2 },
	{ 10, GetSessionInfoValues10 },
	{ 502, GetSessionInfoValues502 }
};


wstring SessionUserFlagsToString(DWORD userFlags)
{
	wstring ret;
	if (userFlags & SESS_GUEST)
		ret += L"GUEST ";
	if (userFlags & SESS_NOENCRYPTION)
		ret += L"UNENCRYPTED ";

	return ret;
}


void GetSessionInfoValues0(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	LPSESSION_INFO_0 pSessionInfo = reinterpret_cast<LPSESSION_INFO_0>(buffer);
	descriptions.push_back(L"Computer");
	text.push_back(pSessionInfo->sesi0_cname);
}


void GetSessionInfoValues1(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetSessionInfoValues0(buffer, descriptions, text);

	LPSESSION_INFO_1 pSessionInfo = reinterpret_cast<LPSESSION_INFO_1>(buffer);
	descriptions.push_back(L"User");
	text.push_back(pSessionInfo->sesi1_username);
	descriptions.push_back(L"Opens");
	text.push_back(to_wstring(pSessionInfo->sesi1_num_opens));
	descriptions.push_back(L"Time");
	text.push_back(SecondsToString(pSessionInfo->sesi1_time));
	descriptions.push_back(L"Idle");
	text.push_back(SecondsToString(pSessionInfo->sesi1_idle_time));
	descriptions.push_back(L"Flags");
	text.push_back(SessionUserFlagsToString(pSessionInfo->sesi1_user_flags));

}


void GetSessionInfoValues2(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetSessionInfoValues1(buffer, descriptions, text);

	LPSESSION_INFO_2 pSessionInfo = reinterpret_cast<LPSESSION_INFO_2>(buffer);
	descriptions.push_back(L"Client type");
	text.push_back(pSessionInfo->sesi2_cltype_name);
}


void GetSessionInfoValues10(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetSessionInfoValues0(buffer, descriptions, text);

	LPSESSION_INFO_10 pSessionInfo = reinterpret_cast<LPSESSION_INFO_10>(buffer);
	descriptions.push_back(L"User");
	text.push_back(pSessionInfo->sesi10_username);
	descriptions.push_back(L"Time");
	text.push_back(SecondsToString(pSessionInfo->sesi10_time));
	descriptions.push_back(L"Idle");
	text.push_back(SecondsToString(pSessionInfo->sesi10_idle_time));
}


void GetSessionInfoValues502(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetSessionInfoValues2(buffer, descriptions, text);

	LPSESSION_INFO_502 pSessionInfo = reinterpret_cast<LPSESSION_INFO_502>(buffer);
	descriptions.push_back(L"Transport");
	text.push_back(pSessionInfo->sesi502_transport);
}


int SessionEnum(const wstring& server, const wstring& computer, const wstring& user, int level)
{
	wcout << L"NetSessionEnum(" << server << L", " << computer << L", " << user << L") level " << level << endl;

	LPBYTE buffer = nullptr;
	DWORD entriesRead = 0;
	DWORD totalEntries = 0;
	DWORD resumeHandle = 0;

	NET_API_STATUS status = NetSessionEnum(
		(LPWSTR)server.c_str(),
		(LPWSTR)computer.c_str(),
		(LPWSTR)user.c_str(),
		level,
		&buffer,
		MAX_PREFERRED_LENGTH,
		&entriesRead,
		&totalEntries,
		&resumeHandle
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" enumerating sessions: " << ErrorString(status) << endl;
		return status;
	}

	vector<wstring> headers;
	vector<vector<wstring>> values;

	char* ptr = reinterpret_cast<char*>(buffer);
	for (DWORD d = 0; d < entriesRead; d++)
	{
		vector<wstring> descriptions;
		vector<wstring> text;

		LEVELMAPCHECK(NetSessionGetValuesMap, level);
		NetSessionGetValuesMap[level](ptr, descriptions, text);

		if (0 == d)
		{
			headers = descriptions;
			values.resize(headers.size());
		}
		for (size_t n = 0; n < text.size(); n++)
		{
			values[n].push_back(text[n]);
		}


		switch (level)
		{
			POINTERSTRUCTINCCASE(0, ptr, SESSION);
			POINTERSTRUCTINCCASE(1, ptr, SESSION);
			POINTERSTRUCTINCCASE(2, ptr, SESSION);
			POINTERSTRUCTINCCASE(10, ptr, SESSION);
			POINTERSTRUCTINCCASE(502, ptr, SESSION);
		default:
			wcerr << L"INTERNAL ERROR: MISSING LEVEL IN POINTER INCREMENT SWITCH" << endl;
			exit(ERROR_INTERNAL_ERROR);
		}
	}

	OutputTable(headers, values);

	if (!g_xmlFilename.empty())
	{
		XmlOutputEnum(g_xmlFilename, level, server, L"session", headers, values);
	}

	if (!g_jsonFilename.empty())
	{
		JsonOutputEnum(g_jsonFilename, level, server, L"session", headers, values);
	}

	NetApiBufferFree(buffer);

	return status;
}


int SessionDel(const wstring& server, const wstring& computer, const wstring& user)
{
	wcout << L"NetSessionDel(" << server << L", " << computer << L", " << user << L")" << endl;

	NET_API_STATUS status = NetSessionDel(
		(LPWSTR)server.c_str(),
		(LPWSTR)computer.c_str(),
		(LPWSTR)user.c_str()
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" deleting session: " << ErrorString(status) << endl;
	}
	else
	{
		wcout << L"Session deleted." << endl;
	}

	return status;
}


int SessionGetInfo(const wstring& server, const wstring& computer, const wstring& user, int level)
{
	wcout << L"NetSessionGetInfo(" << server << L", " << computer << L", " << user << L") level " << level << endl;

	LPBYTE buffer = nullptr;

	NET_API_STATUS status = NetSessionGetInfo(
		(LPWSTR)server.c_str(),
		(LPWSTR)computer.c_str(),
		(LPWSTR)user.c_str(),
		level,
		&buffer
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" getting session info: " << ErrorString(status) << endl;
		return status;
	}

	vector<wstring> descriptions;
	vector<wstring> text;

	LEVELMAPCHECK(NetSessionGetValuesMap, level);
	NetSessionGetValuesMap[level](buffer, descriptions, text);

	vector<vector<wstring>> values;
	values.push_back(descriptions);
	values.push_back(text);

	OutputTable({}, values);

	if (!g_xmlFilename.empty())
	{
		XmlOutputInfo(
			g_xmlFilename,
			level,
			server,
			L"session",
			{ make_pair(L"computer", computer), 
			  make_pair(L"user", user) },
			values
		);
	}

	if (!g_jsonFilename.empty())
	{
		JsonOutputInfo(
			g_jsonFilename,
			level,
			server,
			L"session",
			{ make_pair(L"computer", computer),
			  make_pair(L"user", user) },
			values
		);
	}

	NetApiBufferFree(buffer);

	return status;
}

