#pragma once

/* Security.h - Security related functions */

#include <Windows.h>

#include <string>
#include <vector>
#include <tuple>

using namespace std;

wstring SidToString(PSID pSID);
vector<tuple<wstring, wstring, DWORD>> SecurityDescriptorDaclAcesToString(const wstring& server, PSECURITY_DESCRIPTOR pSD);
PSECURITY_DESCRIPTOR MakeSecurityDescriptor(const wstring& server, const vector<tuple<wstring, DWORD>>& users);
pair<wstring, wstring> SecurityDescriptorOwnerToString(const wstring& server, PSECURITY_DESCRIPTOR pSD);

