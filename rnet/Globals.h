#pragma once

/* Globals.h - Global variable declarations */

#include <string>

using namespace std;

extern wstring g_xmlFilename;
extern wstring g_jsonFilename;
extern bool g_showCalls;