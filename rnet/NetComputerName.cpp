/* NetComputerName.cpp - Wrappers around Net*ComputerName?() APIs */

#include "NetComputerName.h"
#include "Error.h"
#include "Utils.h"
#include "Types.h"
#include "Xml.h"
#include "Json.h"
#include "Globals.h"

#include <Windows.h>
#include <LM.h>

#include <iostream>


int EnumerateComputerNames(const wstring& server)
{
	wcout << L"NetEnumerateComputerNames(" << server << L")" << endl;

	DWORD count = 0;
	LPWSTR* computerNamePtrs = nullptr;

	NET_API_STATUS status = NetEnumerateComputerNames(
		server.c_str(),
		NetAllComputerNames,
		0,
		&count,
		&computerNamePtrs
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" enumerating computer names: " << ErrorString(status) << endl;
		return status;
	}

	wcout << count << L" computer name" << (count == 1 ? L"" : L"s") << ":" << endl;

	vector<wstring> descriptions;
	vector<wstring> computerNames;
	for (DWORD d = 0; d < count; d++)
	{
		descriptions.push_back(L"Name");
		computerNames.push_back(computerNamePtrs[d]);
	}

	vector<vector<wstring>> values;
	values.push_back(descriptions);
	values.push_back(computerNames);

	OutputTable({}, values);

	if (!g_xmlFilename.empty())
	{
		XmlOutputInfo(
			g_xmlFilename,
			-1,
			server,
			L"computernames",
			{ },
			values
		);
	}

	if (!g_jsonFilename.empty())
	{
		JsonOutputInfo(
			g_jsonFilename,
			-1,
			server,
			L"computernames",
			{ },
			values
		);
	}

	return status;
}


int AddAlternateComputerName(const wstring& server, const wstring& name)
{
	wcout << L"NetAddAlternateComputerName(" << server << L", " << name << L")" << endl;

	NET_API_STATUS status = NetAddAlternateComputerName(
		server.c_str(),
		name.c_str(),
		nullptr,
		nullptr,
		0
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" adding computer name: " << ErrorString(status) << endl;
	}
	else
	{
		wcout << L"Computer name '" << name << L"' added." << endl;
	}

	return status;
}


int RemoveAlternateComputerName(const wstring& server, const wstring& name)
{
	wcout << L"NetRemoveAlternateComputerName(" << server << L", " << name << L")" << endl;

	NET_API_STATUS status = NetRemoveAlternateComputerName(
		server.c_str(),
		name.c_str(),
		nullptr,
		nullptr,
		0
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" removing computer name: " << ErrorString(status) << endl;
	}
	else
	{
		wcout << L"Computer name '" << name << L"' removed." << endl;
	}

	return status;
}


