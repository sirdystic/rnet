#pragma once

/* NetComputerName.h - Wrappers around Net*ComputerName?() APIs */

#include <string>

using namespace std;


int EnumerateComputerNames(const wstring& server);
int AddAlternateComputerName(const wstring& server, const wstring& name);
int RemoveAlternateComputerName(const wstring& server, const wstring& name);


