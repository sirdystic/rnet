/* NetUse.cpp - Wrappers around NetUse*() APIs */

#include "NetUse.h"
#include "Error.h"
#include "Utils.h"
#include "Types.h"
#include "Macros.h"
#include "Xml.h"
#include "Json.h"
#include "NullString.h"
#include "Globals.h"

#include <Windows.h>
#include <LM.h>

#include <iostream>


void GetUseInfoValues0(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetUseInfoValues1(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetUseInfoValues2(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);

vector<int> NetUseEnumTryLevels = { 0, 1, 2 };
vector<int> NetUseGetInfoTryLevels = { 0, 1, 2 };
levelmap NetUseGetValuesMap = {
	{ 0, GetUseInfoValues0 },
	{ 1, GetUseInfoValues1 },
	{ 2, GetUseInfoValues2 },
};


wstring UseStatusToString(DWORD status)
{
	switch (status)
	{
	case USE_OK:
		return L"OK";
	case USE_PAUSED:
		return L"Paused";
	//case USE_SESSLOST:
	case USE_DISCONN:
		return L"Disconnected";
	case USE_NETERR:
		return L"Error";
	case USE_CONN:
		return L"Connecting";
	case USE_RECONN:
		return L"Reconnecting";
	}

	return L"Unknown (" + to_wstring(status) + L")";
}


wstring UseTypeToString(DWORD type)
{
	switch (type)
	{
	case USE_WILDCARD:
		return L"*";
	case USE_DISKDEV:
		return L"DISK";
	case USE_SPOOLDEV:
		return L"PRINTER";
	case USE_IPC:
		return L"IPC";
	}

	return L"Unknown (" + to_wstring(type) + L")";
}


wstring UseParamErrorToString(DWORD paramErr)
{
	switch (paramErr)
	{
	case USE_LOCAL_PARMNUM:
		return L"Local name";
	case USE_REMOTE_PARMNUM:
		return L"Remote name";
	case USE_PASSWORD_PARMNUM:
		return L"Password";
	case USE_ASGTYPE_PARMNUM:
		return L"Ags type";
	case USE_USERNAME_PARMNUM:
		return L"User name";
	case USE_DOMAINNAME_PARMNUM:
		return L"Domain name";
	}

	return L"Unknown";
}


void GetUseInfoValues0(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	LPUSE_INFO_0 pUseInfo = reinterpret_cast<LPUSE_INFO_0>(buffer);
	descriptions.push_back(L"Local");
	text.push_back(pUseInfo->ui0_local);
	descriptions.push_back(L"Remote");
	text.push_back(pUseInfo->ui0_remote);
}


void GetUseInfoValues1(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetUseInfoValues0(buffer, descriptions, text);

	LPUSE_INFO_1 pUseInfo = reinterpret_cast<LPUSE_INFO_1>(buffer);
	// Skip password
	descriptions.push_back(L"Status");
	text.push_back(UseStatusToString(pUseInfo->ui1_status));
	descriptions.push_back(L"Type");
	text.push_back(UseTypeToString(pUseInfo->ui1_asg_type));
	descriptions.push_back(L"Opens");
	text.push_back(to_wstring(pUseInfo->ui1_refcount));
	descriptions.push_back(L"Uses");
	text.push_back(to_wstring(pUseInfo->ui1_usecount));
}


void GetUseInfoValues2(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetUseInfoValues1(buffer, descriptions, text);

	LPUSE_INFO_2 pUseInfo = reinterpret_cast<LPUSE_INFO_2>(buffer);
	descriptions.push_back(L"User name:");
	text.push_back(pUseInfo->ui2_username);
	descriptions.push_back(L"Remote domain name:");
	text.push_back(nullptr == pUseInfo->ui2_domainname ? L"" : pUseInfo->ui2_domainname);
}


int UseEnum(const wstring& server, int level)
{
	wcout << L"NetUseEnum(" << server << L") level " << level << endl;

	LPBYTE buffer = nullptr;
	DWORD entriesRead = 0;
	DWORD totalEntries = 0;
	DWORD resumeHandle = 0;

	NET_API_STATUS status = NetUseEnum(
		(LPWSTR)server.c_str(),
		level,
		&buffer,
		MAX_PREFERRED_LENGTH,
		&entriesRead,
		&totalEntries,
		&resumeHandle
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" enumerating uses: " << ErrorString(status) << endl;
		return status;
	}

	vector<wstring> headers;
	vector<vector<wstring>> values;

	char* ptr = reinterpret_cast<char*>(buffer);
	for (DWORD d = 0; d < entriesRead; d++)
	{
		vector<wstring> descriptions;
		vector<wstring> text;

		LEVELMAPCHECK(NetUseGetValuesMap, level);
		NetUseGetValuesMap[level](ptr, descriptions, text);

		if (0 == d)
		{
			headers = descriptions;
			values.resize(headers.size());
		}
		for (size_t n = 0; n < text.size(); n++)
		{
			values[n].push_back(text[n]);
		}

		switch (level)
		{
			POINTERSTRUCTINCCASE(0, ptr, USE);
			POINTERSTRUCTINCCASE(1, ptr, USE);
			POINTERSTRUCTINCCASE(2, ptr, USE);
		default:
			wcerr << L"INTERNAL ERROR: MISSING LEVEL IN POINTER INCREMENT SWITCH" << endl;
			exit(ERROR_INTERNAL_ERROR);
			break;
		}
	}

	OutputTable(headers, values);

	if (!g_xmlFilename.empty())
	{
		XmlOutputEnum(g_xmlFilename, level, server, L"use", headers, values);
	}

	if (!g_jsonFilename.empty())
	{
		JsonOutputEnum(g_jsonFilename, level, server, L"use", headers, values);
	}

	NetApiBufferFree(buffer);

	return status;
}


int UseAdd(const wstring& server, const wstring& local, const wstring& remote, const nullstring& user, const wstring& domain, const nullstring& password)
{
	wcout << L"NetUseAdd(" << server << L")" << endl;

	USE_INFO_2 useInfo;
	DWORD paramErr = 0;
	
	DWORD type = USE_WILDCARD;
	if (!local.empty())
	{
		if (local.length() > 3 && 0 == StringUpper(local).find(L"LPT"))
			type = USE_SPOOLDEV;
		else
			type = USE_DISKDEV;
	}

	useInfo.ui2_local = (LPWSTR)local.c_str();
	useInfo.ui2_remote = (LPWSTR)remote.c_str();
	useInfo.ui2_username = (LPWSTR)user.c_str();
	useInfo.ui2_domainname = (LPWSTR)domain.c_str();
	useInfo.ui2_password = (LPWSTR)password.c_str();
	useInfo.ui2_status = USE_OK;
	useInfo.ui2_asg_type = type;
	useInfo.ui2_refcount = 0;
	useInfo.ui2_usecount = 0;

	NET_API_STATUS status = NetUseAdd(
		(LPWSTR)server.c_str(),
		2,
		reinterpret_cast<LPBYTE>(&useInfo),
		&paramErr
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" adding use: " << ErrorString(status) << endl;
		if (ERROR_INVALID_PARAMETER == status)
		{
			wcerr << L"Parameter error " << paramErr << L": " << UseParamErrorToString(paramErr) << endl;
		}
	}
	else
	{
		wcout << L"Use added to '" << remote << L"'." << endl;
	}

	return status;
}


int UseDel(const wstring& server, const wstring use)
{
	wcout << L"NetUseAdd(" << server << L", " << use << L")" << endl;

	NET_API_STATUS status = NetUseDel(
		(LPWSTR)server.c_str(),
		(LPWSTR)use.c_str(),
		USE_LOTS_OF_FORCE
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" deleting use: " << ErrorString(status) << endl;
	}
	else
	{
		wcout << L"Use '" << use << L"' deleted." << endl;
	}

	return status;
}


int UseGetInfo(const wstring& server, const wstring& use, int level)
{
	wcout << L"NetUseGetInfo(" << server << L", " << use << L") level " << level;

	LPBYTE buffer = nullptr;

	NET_API_STATUS status = NetUseGetInfo(
		(LPWSTR)server.c_str(),
		(LPWSTR)use.c_str(),
		level,
		&buffer
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" getting use info: " << ErrorString(status) << endl;
		return status;
	}

	LPUSE_INFO_2 pUseInfo = reinterpret_cast<LPUSE_INFO_2>(buffer);

	vector<wstring> descriptions;
	vector<wstring> text;

	LEVELMAPCHECK(NetUseGetValuesMap, level);
	NetUseGetValuesMap[level](buffer, descriptions, text);

	vector<vector<wstring>> values;
	values.push_back(descriptions);
	values.push_back(text);

	OutputTable({}, values);

	if (!g_xmlFilename.empty())
	{
		XmlOutputInfo(
			g_xmlFilename,
			level,
			server,
			L"use",
			{ make_pair(L"use", use) },
			values
		);
	}

	if (!g_jsonFilename.empty())
	{
		JsonOutputInfo(
			g_jsonFilename,
			level,
			server,
			L"use",
			{ make_pair(L"use", use) },
			values
		);
	}

	NetApiBufferFree(buffer);

	return status;
}



