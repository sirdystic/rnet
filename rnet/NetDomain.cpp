/* NetDomain.cpp - Wrappers around domain related APIs */

#include "NetDomain.h"
#include "Error.h"

#include <Windows.h>
#include <LM.h>

#include <iostream>


int GetDCName(const wstring& server, const wstring& domain)
{
	wcout << L"NetGetDCName(" << server << L", " << domain << L")" << endl;

	LPBYTE buffer = nullptr;
	NET_API_STATUS status = NetGetDCName(
		server.c_str(),
		domain.c_str(),
		&buffer
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" getting DC name: " << ErrorString(status) << endl;
		return status;
	}

	wstring DC(reinterpret_cast<LPWSTR>(buffer));
	wcout << L"Primary domain controller: " << DC << endl;

	NetApiBufferFree(buffer);

	return status;
}


int GetAnyDCName(const wstring& server, const wstring& domain)
{
	wcout << L"NetGetAnyDCName(" << server << L", " << domain << L")" << endl;

	LPBYTE buffer = nullptr;
	NET_API_STATUS status = NetGetAnyDCName(
		server.c_str(),
		domain.c_str(),
		&buffer
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" getting any DC name: " << ErrorString(status) << endl;
		return status;
	}

	wstring DC(reinterpret_cast<LPWSTR>(buffer));
	wcout << L"Any domain controller: " << DC << endl;

	NetApiBufferFree(buffer);

	return status;
}


int JoinDomain(const wstring& server, const wstring& domain, const nullstring& user, const nullstring& password)
{
	wcout << L"NetJoinDomain(" << server << L", " << domain << L", " << user.string() << L", " << password.string() << L")" << endl;

	NET_API_STATUS status = NetJoinDomain(
		server.c_str(),
		domain.c_str(),
		nullptr,
		user.c_str(),
		password.c_str(),
		NETSETUP_JOIN_DOMAIN
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" joining domain '" << domain << L"': " << ErrorString(status) << endl;
	}
	else
	{
		wcout << L"Domain '" << domain << L"' successfully joined." << endl;
	}

	return status;
}


int UnjoinDomain(const wstring& server, const nullstring& user, const nullstring& password)
{
	wcout << L"NetUnjoinDomain(" << server << L", " << user.string() << L", " << password.string() << L")" << endl;

	NET_API_STATUS status = NetUnjoinDomain(
		server.c_str(),
		user.c_str(),
		password.c_str(),
		0
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" unjoining domain: " << ErrorString(status) << endl;
	}
	else
	{
		wcout << L"Domain successfully unjoined." << endl;
	}

	return status;
}


