/* Help.cpp - Help related functions */

#include "Help.h"
#include "Utils.h"
#include "NetShare.h"
#include "NetUser.h"
#include "NetLocalGroup.h"
#include "NetGroup.h"
#include "NetSession.h"
#include "NetService.h"
#include "NetUse.h"
#include "NetServer.h"
#include "NetWksta.h"
#include "NetWkstaUser.h"
#include "NetComputerName.h"
#include "NetDomain.h"

#include <iostream>


void Usage()
{
	wcout << L"The syntax of this command is:" << endl;
	wcout << endl;
	wcout << L"RNET [\\\\SERVER]" << endl;
	wcout << L"      { HELP | SHARE | USER | LOCALGROUP | GROUP | SESSION |" << endl;
	wcout << L"        USE | VIEW | CONFIG | NAME | DOMAIN | LOGINS | SERVICE }" << endl;
	wcout << endl;
}


void ShowVersion()
{
	wcout << "RNET version " << GetVersionString() << endl;
	wcout << "Copyright 2019, Sir Dystic of Cult of the Dead Cow" << endl;
}


int Levels(const wstring& command)
{
	if (L"USER" == command)
	{
		wcout << L"NetUserEnum try levels: ";
		for (auto l : NetUserEnumTryLevels)
			wcout << l << L" ";
		wcout << endl;
		wcout << L"NetUserGetInfo try levels: ";
		for (auto l : NetUserGetInfoTryLevels)
			wcout << l << L" ";
		wcout << endl;
		wcout << L"NetUserGetInfo valid levels: ";
		for (auto l : NetUserGetInfoValidLevels)
			wcout << l << L" ";
		wcout << endl;
		wcout << endl;
		wcout << L"0 - Return user account names." << endl;
		wcout << L"1 - Return detailed information about user accounts." << endl;
		wcout << L"2 - Return detailed information about user accounts, including authorization levels and logon information." << endl;
		wcout << L"3 - Return detailed information about user accounts, including authorization levels, logon information, RIDs for the user and the primary group, and profile information." << endl;
		wcout << L"10 - Return user and account names and comments." << endl;
		wcout << L"11 - Return detailed information about user accounts." << endl;
		wcout << L"20 - Return the user's name and identifier and various account attributes." << endl;
		wcout << L"23 - Return the user's name and identifier and various account attributes." << endl;
		wcout << L"24 - Return user account information for accounts which are connected to an Internet identity." << endl;

	}
	else if (L"SHARE" == command)
	{
		wcout << L"NetShareEnum try levels: ";
		for (auto l : NetShareEnumTryLevels)
			wcout << l << L" ";
		wcout << endl;
		wcout << L"NetShareGetInfo try levels: ";
		for (auto l : NetShareGetInfoTryLevels)
			wcout << l << L" ";
		wcout << endl;
		wcout << endl;
		wcout << L"0 - Return share names." << endl;
		wcout << L"1 - Return information about shared resources, including the name and type of the resource, and a comment associated with the resource." << endl;
		wcout << L"2 - Return information about shared resources, including name of the resource, type and permissions, password, and number of connections." << endl;
		wcout << L"501 - Return the name and type of the resource, and a comment associated with the resource." << endl;
		wcout << L"502 - Return information about shared resources, including name of the resource, type and permissions, number of connections, and other pertinent information." << endl;
		wcout << L"503 - Return information about shared resources, including the name of the resource, type and permissions, number of connections, and other pertinent information." << endl;
	}
	else if (L"SESSION" == command)
	{
		wcout << L"NetSessionEnum try levels: ";
		for (auto l : NetSessionEnumTryLevels)
			wcout << l << L" ";
		wcout << endl;
		wcout << L"NetSessionGetInfo try levels: ";
		for (auto l : NetSessionGetInfoTryLevels)
			wcout << l << L" ";
		wcout << endl;
		wcout << endl;
		wcout << L"0 - Return the name of the computer that established the session." << endl;
		wcout << L"1 - Return the name of the computer, name of the user, and open files, pipes, and devices on the computer." << endl;
		wcout << L"2 - In addition to the information indicated for level 1, return the type of client and how the user established the session." << endl;
		wcout << L"10 - Return the name of the computer, name of the user, and active and idle times for the session." << endl;
		wcout << L"502 - Return the name of the computer; name of the user; open files, pipes, and devices on the computer; and the name of the transport the client is using." << endl;
	}
	else if (L"SERVICE" == command)
	{
		wcout << L"NetServiceEnum try levels: ";
		for (auto l : NetServiceEnumTryLevels)
			wcout << l << L" ";
		wcout << endl;
		wcout << L"NetServiceGetInfo try levels: ";
		for (auto l : NetServiceGetInfoTryLevels)
			wcout << l << L" ";
		wcout << endl;
		wcout << endl;
		wcout << L"0 - Return the service name." << endl;
		wcout << L"1 - Return the service name, status, code and PID." << endl;
		wcout << L"2 - Return the above plus 'text', 'Specic error' and display name." << endl;
	}
	else if (L"GROUP" == command)
	{
		wcout << L"NetGroupEnum try levels: ";
		for (auto l : NetGroupEnumTryLevels)
			wcout << l << L" ";
		wcout << endl;
		wcout << L"NetGroupGetInfo try levels: ";
		for (auto l : NetGroupGetInfoTryLevels)
			wcout << l << L" ";
		wcout << endl;
		wcout << endl;
		wcout << L"0 - Return the global group name." << endl;
		wcout << L"1 - Return the global group name and a comment." << endl;
		wcout << L"2 - Return detailed information about the global group." << endl;
		wcout << L"3 - Return detailed information about the global group." << endl;
	}
	else if (L"LOCALGROUP" == command)
	{
		wcout << L"NetLocalGroupEnum try levels: ";
		for (auto l : NetLocalGroupEnumTryLevels)
			wcout << l << L" ";
		wcout << endl;
		wcout << L"NetLocalGroupGetInfo try levels: ";
		for (auto l : NetLocalGroupGetInfoTryLevels)
			wcout << l << L" ";
		wcout << endl;
		wcout << L"NetLocalGroupGetInfo valid levels: ";
		for (auto l : NetLocalGroupGetInfoValidLevels)
			wcout << l << L" ";
		wcout << endl;
		wcout << endl;
		wcout << L"0 - Return local group names." << endl;
		wcout << L"1 - Return local group names and the comment associated with each group." << endl;
		wcout << L"1002 - Return the comment associated with the local group." << endl;
	}
	else if (L"LOGINS" == command)
	{
		wcout << L"NetWkstaUserEnum try levels: ";
		for (auto l : NetWkstaUserEnumTryLevels)
			wcout << l << L" ";
		wcout << endl;
		wcout << endl;
		wcout << L"0 - Return the names of users currently logged on to the workstation." << endl;
		wcout << L"1 - Return the names of the current users and the domains accessed by the workstation." << endl;
	}
	else if (L"VIEW" == command)
	{
		wcout << L"NetServerEnum try levels: ";
		for (auto l : NetServerEnumTryLevels)
			wcout << l << L" ";
		wcout << endl;
		wcout << endl;
		wcout << L"100 - Return server names and platform information." << endl;
		wcout << L"101 - Return server names, types, and associated data." << endl;

	}
	else if (L"CONFIG" == command)
	{
		wcout << L"NetServerGetInfo try levels: ";
		for (auto l : NetServerGetInfoTryLevels)
			wcout << l << L" ";
		wcout << endl;
		wcout << endl;
		wcout << L"100 - Return the server name and platform information." << endl;
		wcout << L"101 - Return the server name, type, and associated software.." << endl;
		wcout << L"102 - Return the server name, type, associated software, and other attributes." << endl;
		wcout << endl;
		wcout << L"NetWkstaGetInfo try levels: ";
		for (auto l : NetWkstaGetInfoTryLevels)
			wcout << l << L" ";
		wcout << endl;
		wcout << endl;
		wcout << L"100 - Return information about the workstation environment, including platform-specific information, the name of the domain and the local computer, and information concerning the operating system." << endl;
		wcout << L"101 - In addition to level 100 information, return the path to the LANMAN directory." << endl;
		wcout << L"102 - In addition to level 101 information, return the number of users who are logged on to the local computer." << endl;
	}
	else if (L"USE" == command)
	{
		wcout << L"NetUseEnum try levels: ";
		for (auto l : NetUseEnumTryLevels)
			wcout << l << L" ";
		wcout << endl;
		wcout << L"NetUseGetInfo try levels: ";
		for (auto l : NetUseGetInfoTryLevels)
			wcout << l << L" ";
		wcout << endl;
		wcout << endl;
		wcout << L"0 - Specifies a local device name and the share name of a remote resource." << endl;
		wcout << L"1 - Specifies information about the connection between a local device and a shared resource, including connection status and type." << endl;
		wcout << L"2 - Specifies information about the connection between a local device and a shared resource. Information includes the connection status, connection type, user name, and domain name." << endl;
	}
	else
	{
		wcout << L"No levels are available for command: " << command << endl;
	}

	return 0;
}


int Help(const wstring& server, const vector<wstring>& arguments)
{
	if (arguments.empty())
	{
		wcout << L"The syntax of this command is:" << endl;
		wcout << endl;
		wcout << L"RNET HELP command" << endl;
		wcout << endl;
		wcout << L"Available commands are:" << endl;
		wcout << L" RNET SHARE       RNET USER        RNET LOCALGROUP" << endl;
		wcout << L" RNET GROUP       RNET SESSION     RNET USE" << endl;
		wcout << L" RNET VIEW        RNET CONFIG      RNET NAME" << endl;
		wcout << L" RNET DOMAIN      RNET LOGINS      RNET SERVICE" << endl;
		wcout << endl;
		wcout << L"Commands that query or enumerate info can include the options" << endl;
		wcout << L"/XML:filename and /JSON:filename to output the data to a formatted file." << endl;
		wcout << endl;
		wcout << L"RNET HELP INTRO explains some background about RNET and its functionality." << endl;
		wcout << L"RNET HELP SYNTAX explains how to read NET HELP syntax lines." << endl;
		wcout << L"RNET HELP REMOTE gives some info on using RNET with remote servers." << endl;
		wcout << L"RNET HELP LEVELS gives info about information levels." << endl;
		wcout << L"Also: RNET HELP {USERCODES | GROUPCODES | SERVERCODES}" << endl;
		wcout << L"RNET HELP command | MORE displays Help one screen at a time." << endl;
		wcout << L"RNET HELP cDc just because." << endl;
		wcout << endl;
		ShowVersion();
		return 0;
	}


	if (!(arguments.size() == 1 ||
		(arguments.size() == 2 && L"CONFIG" == StringUpper(arguments[0])) ||
		(arguments.size() == 2 && L"LEVELS" == StringUpper(arguments[0]))
		))
	{
		Usage();
		return ERROR_BAD_ARGUMENTS;
	}

	wstring command = StringUpper(arguments[0]);

	if (L"HELP" == command)
	{
		wcout << L"RNET HELP command" << endl;
		wcout << L" Display help about a command" << endl;
	}
	else if (L"INTRO" == command)
	{
		wcout << L"RNET.EXE is a command line tool exposing much of the functionality of" << endl;
		wcout << L"Lan Manager Networking Management Functions defined in LM.h.  It allows" << endl;
		wcout << L"querying, enumeration, manipulation, creation and deletion of networking" << endl;
		wcout << L"and security objects such as users, groups, shares and connections." << endl;
		wcout << endl;
		wcout << L"RNET.EXE duplicates much of the functionality of NET.EXE (included with" << endl;
		wcout << L"Windows) with the addition of an optional \\\\server parameter in the" << endl;
		wcout << L"second position allowing the operations to be performed on remote a computer." << endl;
		wcout << L"Without this parameter the operations will be performd on the local computer" << endl;
		wcout << L"as NET.EXE would.  The command line syntax for RNET.EXE is in most cases" << endl;
		wcout << L"compatible with NET.EXE with some exceptions.  In most cases documentation" << endl;
		wcout << L"for NET.EXE should provide additional information related to RNET.EXE commands." << endl;
		wcout << endl;
		wcout << L"RNET.EXE does not support the /DOMAIN option but allows you to specify a domain" << endl;
		wcout << L"controller for the \\\\server parameter.  Use the DOMAIN command to locate domain" << endl;
		wcout << L"servers." << endl;
		wcout << endl;
		wcout << L"RNET.EXE does not implement the START STOP PAUSE CONTINUE commands of NET.EXE." << endl;
		wcout << L"Windows now includes sc.exe (formerly part of the Resource Kit Tools) which" << endl;
		wcout << L"provides a robust command line interface to the Service Control Manager." << endl;
	}
	else if (L"REMOTE" == command)
	{
		wcout << L"Performing any operations on remote computers first requires being" << endl;
		wcout << L"connected to the IPC of target machine." << endl;
		wcout << L"To connect to the IPC of a remote machine use the USE command" << endl;
		wcout << L"to connect to the special IPC$ device:" << endl;
		wcout << endl;
		wcout << L"RNET USE \\\\desktop\\IPC$ /user:veggie \"My C0W 4evz!!\"" << endl;
		wcout << endl;
		wcout << L"Most operations require being connected as a privileged" << endl;
		wcout << L"user, usually an administrator level account.  However, every" << endl;
		wcout << L"version of Windows will provide different levels of information" << endl;
		wcout << L"from less privileged accounts, the older the version of Windows the" << endl;
		wcout << L"more information it may provide.  There is even a special" << endl;
		wcout << L"\"null session\" (anonymous) account built into Windows:" << endl;
		wcout << endl;
		wcout << L"RNET use \\\\desktop\\IPC$ /user: \"\"" << endl;
		wcout << endl;
		wcout << L"(Specifies an empty user name and empty pasword.)" << endl;
		wcout << L"Note: NET.exe allows you to connect to IPC$ on a machine by" << endl;
		wcout << L"'using' just the machine name or IP (ie \\\\HOST4) omitting '\\IPC$'" << endl;
		wcout << L"but with RNET you must explicitly specify the IPC$ endpoint." << endl;
		wcout << L"Note: As of Windows 10, by default almost nothing works remotely " << endl;
		wcout << L"even if connected as an administrator.  Remote administration functionality" << endl;
		wcout << L"can be enabled by setting the value 'LocalAccountTokenFilterPolicy' in the key:" << endl;
		wcout << L"\\\\HKLM\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\System" << endl;
		wcout << L"to DWORD 1." << endl;
	}
	else if (L"SYNTAX" == command)
	{
		wcout << L"The following conventions are used to indicate command syntax:" << endl;
		wcout << endl;
		wcout << L"- Capital letters represent words that must be typed as shown.  Lower -" << endl;
		wcout << L"case letters represent names of items that may vary, such as filenames." << endl;
		wcout << endl;
		wcout << L"- The [ and ] characters surround optional items that can be supplied" << endl;
		wcout << L"with the command." << endl;
		wcout << endl;
		wcout << L"- The { and } characters surround lists of items.  You must supply one" << endl;
		wcout << L"of the items with the command." << endl;
		wcout << endl;
		wcout << L"- The | character separates items in a list.  Only one of the items can" << endl;
		wcout << L"be supplied with the command." << endl;
		wcout << endl;
		wcout << L"For example, in the following syntax, you must type RNET COMMAND and" << endl;
		wcout << L"either SWITCH1 or SWITCH2.  Supplying a name is optional." << endl;
		wcout << L"RNET COMMAND [name] { SWITCH1 | SWITCH2 }" << endl;
		wcout << endl;
		wcout << L"-The [...] characters mean you can repeat the previous item." << endl;
		wcout << L"Separate items with spaces." << endl;
		wcout << endl;
		wcout << L"- The [,...] characters mean you can repeat the previous item, but" << endl;
		wcout << L"you must separate items with commas or semicolons, not spaces." << endl;
		wcout << endl;
		wcout << L"Parameters such as passwords with spaces in them should be enclosed in." << endl;
		wcout << L"quotes.  When following an option the first quote should follow the" << endl;
		wcout << L"colon.  Empty parameters can be passed with empty quotes or nothing" << endl;
		wcout << L"after the colon." << endl;
		wcout << L"RNET USE \\\\server\\ipc$ /user:\"Jack Black\" \"10A shushD\"" << endl;
		wcout << L"RNET USE \\\\server\\ipc$ /user: \"\"" << endl;
		wcout << endl;
	}
	else if (L"CDC" == command)
	{
		wcout << L"     _   _                                                    _   _" << endl;
		wcout << L"    ((___))                                                  ((___))" << endl;
		wcout << L"    [ x x ]       _ _/ cDc GRAND IMPERIAL DYNASTY \\_ _       [ x x ]" << endl;
		wcout << L"     \\   /        _ _        MCMLXXXIV A.D.        _ _        \\   /" << endl;
		wcout << L"     (' ')           \\  VOLANDO, REPTILIA SPERNO  /           (' ')" << endl;
		wcout << L"      (U)                                                      (U)" << endl;
		wcout << endl;
		wcout << L"Based in Lubbock, Texas, CULT OF THE DEAD COW (cDc) is the most-" << endl;
		wcout << L"accomplished and longest-running group in the computer underground.  " << endl;
		wcout << L"Founded in 1984 and widely considered to be the most elite people to " << endl;
		wcout << L"ever walk the face of the earth, this think tank has been referred to " << endl;
		wcout << L"as both \"a bunch of sickos\" (Geraldo Rivera) and \"the sexiest group " << endl;
		wcout << L"of computer hackers there ever was\" (Jane Pratt, _Sassy_ and _Jane_ " << endl;
		wcout << L"magazines).  The cDc is a leading developer of Internet privacy and " << endl;
		wcout << L"security tools, which are all free to the public.  In addition, the cDc " << endl;
		wcout << L"created the first electronic publication, which is still going strong." << endl;
		wcout << endl;
		wcout << L"The cDc Grand Imperial Dynasty includes a former Presidential Advisor" << endl;
		wcout << L"on computer security, a Harvard researcher, a former U.N. official, an " << endl;
		wcout << L"assistant district attorney, a professor of logic, an award-winning " << endl;
		wcout << L"filmmaker, several published authors, a video game developer, an Eagle" << endl;
		wcout << L"Scout, programmers of every sort, graphic artists, musicians, currency " << endl;
		wcout << L"traders, and a Merovingian.  And these are just the members who have " << endl;
		wcout << L"chosen to make their association with the cDc known to the public." << endl;
		wcout << endl;
		wcout << L"Several cDc members have also been (or currently are) members of other " << endl;
		wcout << L"high profile groups, both underground and otherwise.  These include the" << endl;
		wcout << L"L0pht, the Institute for Electrical and Electronics Engineers (IEEE), " << endl;
		wcout << L"the Legion of Doom (LOD/H), the Association of Computing Machinery " << endl;
		wcout << L"(ACM), the Hasty Pastry, Restricted Data Transmissions (RDT), the " << endl;
		wcout << L"Masters of Deception (MOD), the USENIX Association, the Walnut Factory," << endl;
		wcout << L"Soylent Communications, w00w00, the Institute for Operations Research " << endl;
		wcout << L"and the Management Sciences (INFORMS), New Hack City, the Sacrament of" << endl;
		wcout << L"Transition, r00t, and the Youth International Party Line/Technology " << endl;
		wcout << L"Assistance Program (YIPL/TAP)." << endl;
		wcout << endl;
		wcout << L"Rumors abound about the cDc:  that it has disrupted communications by " << endl;
		wcout << L"moving satellites; that it is at war with the \"Church\" of Scientology; " << endl;
		wcout << L"that it perpetrated the \"Good Times\" virus hoax; that it gave Ronald " << endl;
		wcout << L"Reagan Alzheimer's disease; that Slobodan Milosevic mentioned it while " << endl;
		wcout << L"cross-examining a witness during his War Crimes Tribunal; that it is " << endl;
		wcout << L"simply the modern incarnation of a gnostic order that dates back to the" << endl;
		wcout << L"cult of Hathor (the cow goddess) in ancient Egypt; that it meets in a " << endl;
		wcout << L"secret bunker under an abandoned military base in the Nevada desert." << endl;
		wcout << endl;
		wcout << L"These are all true. But there's so much more." << endl;
		wcout << endl;
		wcout << L"For thirty-five years, the cDc has proven itself as an innovative force " << endl;
		wcout << L"in the computer underground.  In 1984, the cDc invented the electronic " << endl;
		wcout << L"publication.  In 1990, the cDc's HoHoCon defined the modern computer " << endl;
		wcout << L"underground convention.  In every U.S. Presidential Election since 1992," << endl;
		wcout << L"the cDc has run a candidate.  In 1994, the cDc became the first computer" << endl;
		wcout << L"undergound group to have its own Usenet newsgroup.  Also in 1994, the" << endl;
		wcout << L"cDc coined the term \"hacktivism.\"  The Ninja Strike Force (cDc's elite " << endl;
		wcout << L"cadre of cheerleader-assassins) was founded in 1996.  In 1997, years " << endl;
		wcout << L"before everyone and their dog had jumped on the file sharing bandwagon," << endl;
		wcout << L"it was distributing original mp3-format music on its website.  In 1998" << endl;
		wcout << L"and 1999, the cDc's \"Back Orifice\" series was launched to open the eyes" << endl;
		wcout << L"of consumers regarding the security of their computer operating systems." << endl;
		wcout << L"To this day, Back Orifice and BO2k are among the most popular remote " << endl;
		wcout << L"system administration tools among hackers and IT professionals alike.  " << endl;
		wcout << L"Since 1999, Hacktivismo (a special projects group within the cDc) has " << endl;
		wcout << L"been at the forefront of the ongoing struggle for human rights in and" << endl;
		wcout << L"out of cyberspace.  In 2002, the cDc and Hacktivismo drafted their own" << endl;
		wcout << L"human rights-friendly software license and earned further distinction as" << endl;
		wcout << L"the only underground computer groups to ever receive U.S. Department of" << endl;
		wcout << L"Commerce approval to export strong encryption in software.  In 2004, the" << endl;
		wcout << L"cDc and the NSF launched the Bovine Dawn Dojo Forum, the greatest online" << endl;
		wcout << L"community of all time." << endl;
		wcout << endl;
		wcout << L"Additionally, nothing can compare to the money-throwing, stage-diving," << endl;
		wcout << L"crotch-grabbing, guitar-wailing, inter-species sex-depicting, computer-" << endl;
		wcout << L"smashing, and panty-wetting experience that is a live cDc performance." << endl;
		wcout << endl;
		wcout << L"       . .. ... .... _____ www.cultdeadcow.com _____ .... ... .. ." << endl;

	}
	else if (L"LEVELS" == command)
	{
		if (1 == arguments.size())
		{
			wcout << L"All of the network management enumeration and information retrieval APIs" << endl;
			wcout << L"accept an 'information level' parameter specifying the 'level of data to" << endl;
			wcout << L"be returned.  Some levels are not valid on some versions of Windows." << endl;
			wcout << L"Administrator accounts should be able to use any valid level but less" << endl;
			wcout << L"priveleged accounts (users and guests) may only have access to a subset of" << endl;
			wcout << L"levels.  Each version of Windows released typically allows access to" << endl;
			wcout << L"fewer levels to less priveleged accounts." << endl;
			wcout << endl;
			wcout << L"RNET by default will try each of the standard levels until one succeeds." << endl;
			wcout << L"The /LEVEL option can be used to specify a specific level to try including" << endl;
			wcout << L"in some cases non-standard levels that return different information," << endl;
			wcout << L"user level 24 for instance returns the user internet identity." << endl;
			wcout << endl;
			wcout << L"RNET HELP LEVELS command will show you levels for a specfic command" << endl;
			wcout << L"including additional levels you can specify with /LEVEL that may not" << endl;
			wcout << L"automatically be tried if no level is specified." << endl;
		}
		else
		{
			wstring levelCommand = StringUpper(arguments[1]);
			Levels(levelCommand);
		}
	}
	else if (L"USERCODES" == command)
	{
		wcout << L"User Flags Codes:" << endl;
		wcout << L"O - The logon script executed." << endl;
		wcout << L"D - The user's account is disabled." << endl;
		wcout << L"P - No password is required." << endl;
		wcout << L"p - The user cannot change the password." << endl;
		wcout << L"L - The account is currently locked out." << endl;
		wcout << L"X - The password should never expire on the account." << endl;
		wcout << L"E - The user's password is stored under reversible encryption in the Active Directory." << endl;
		wcout << L"g - Marks the account as \"sensitive\"; other users cannot act as delegates of this user account." << endl;
		wcout << L"s - Requires the user to log on to the user account with a smart card." << endl;
		wcout << L"d - Restrict this principal to use only Data Encryption Standard (DES) encryption types for keys." << endl;
		wcout << L"k - This account does not require Kerberos preauthentication for logon." << endl;
		wcout << L"t - The account is enabled for delegation." << endl;
		wcout << L"e - The user's password has expired." << endl;
		wcout << L"A - The account is trusted to authenticate a user outside of the Kerberos security package and delegate that user through constrained delegation." << endl;
		wcout << L"N - (Normal account) This is a default account type that represents a typical user." << endl;
		wcout << L"T - (Temp duplicate account) This is an account for users whose primary account is in another domain." << endl;
		wcout << L"W - (Workstation trust account) This is a computer account for a computer that is a member of this domain." << endl;
		wcout << L"S - (Server trust account) This is a computer account for a backup domain controller that is a member of this domain." << endl;
		wcout << L"I - (Interdomain trust account) This is a permit to trust account for a domain that trusts other domains." << endl;
	}
	else if (L"GROUPCODES" == command)
	{
		wcout << L"Group Attributes Codes:" << endl;
		wcout << L"e - SE_GROUP_ENABLED: Enabled for access checks.  Ignored unless the SE_GROUP_USE_FOR_DENY_ONLY attribute is also set." << endl;
		wcout << L"E - SE_GROUP_ENABLED_BY_DEFAULT: Enabled by default." << endl;
		wcout << L"i - SE_GROUP_INTEGRITY: Mandatory itegrity SID." << endl;
		wcout << L"I - SE_GROUP_INTEGRITY_ENABLED: Enabled for mandatory integrity checks." << endl;
		wcout << L"L - SE_GROUP_LOGON_ID: Logon SID that identifies the logon session associated with an access token." << endl;
		wcout << L"M - SE_GROUP_MANDATORY: Cannot have the SE_GROUP_ENABLED attribute cleared by a call to the AdjustTokenGroups function." << endl;
		wcout << L"O - SE_GROUP_OWNER: Identifies a group account for which the user of the token is the owner of the group, or the SID can be assigned as the owner of the token or objects." << endl;
		wcout << L"R - SE_GROUP_RESOURCE: Identifies a domain-local group." << endl;
		wcout << L"D - SE_GROUP_USE_FOR_DENY_ONLY: deny-only SID in a restricted token." << endl;
	}
	else if (L"SERVERCODES" == command)
	{
		wcout << L"Server Flags Codes:" << endl;
		wcout << L"WKS - A workstation" << endl;
		wcout << L"SRV - A server" << endl;
		wcout << L"SQL - A server running with Microsoft SQL Server" << endl;
		wcout << L"DOMCT - A primary domain controller" << endl;
		wcout << L"DOMBK - A backup domain controller" << endl;
		wcout << L"TIME - A server running the Timesource service" << endl;
		wcout << L"AFP - A server running the Apple Filing Protocol (AFP) file service" << endl;
		wcout << L"NOV - A Novell server" << endl;
		wcout << L"DOMMB - A LAN Manager 2.x domain member" << endl;
		wcout << L"PRINT - A server that shares a print queue" << endl;
		wcout << L"DIAL - A server that runs a dial-in service" << endl;
		wcout << L"XENIX - A Xenix or Unix server" << endl;
		wcout << L"NT - A workstation or server" << endl;
		wcout << L"WFW - A computer that runs Windows for Workgroups" << endl;
		wcout << L"MFPN - A server that runs the Microsoft File and Print for NetWare service" << endl;
		wcout << L"NTSRV - Any server that is not a domain controller" << endl;
		wcout << L"PTBRW - A computer that can run the browser service" << endl;
		wcout << L"BKBRW - A server running a browser service as backup" << endl;
		wcout << L"MSBRW - A server running the master browser service" << endl;
		wcout << L"DMBRW - A server running the domain master browser" << endl;
		wcout << L"OSF - A computer that runs OSF" << endl;
		wcout << L"VMS - A computer that runs VMS" << endl;
		wcout << L"WIN - A computer that runs Windows" << endl;
		wcout << L"DFS - A server that is the root of a DFS tree" << endl;
		wcout << L"CLUS - A server cluster available in the domain" << endl;
		wcout << L"TS - A server that runs the Terminal Server service" << endl;
		wcout << L"CLUSVS - Cluster virtual servers available in the domain" << endl;
		wcout << L"DCE - A server that runs the DCE Directory and Security Services or equivalent" << endl;
		wcout << L"XPORT - A server that is returned by an alternate transport" << endl;
		wcout << L"LIST - A server that is maintained by the browser" << endl;
		wcout << L"DOMAIN - A primary domain" << endl;
	}
	else if (L"SHARE" == command)
	{
		wcout << L"RNET [\\\\server] SHARE [/LEVEL:val]" << endl;
		wcout << L" Enumerate shares." << endl;
		wcout << L"RNET [\\\\server] SHARE sharename [/LEVEL:val]" << endl;
		wcout << L" Display info about a share." << endl;
		wcout << L"RNET [\\\\server] SHARE sharename options" << endl;
		wcout << L" Change info about a share." << endl;
		wcout << L"RNET [\\\\server] SHARE sharename=drive:path [options]" << endl;
		wcout << L" Add a share." << endl;
		wcout << L"RNET [\\\\server] SHARE sharename /DELETE" << endl;
		wcout << L" Delete a share." << endl;
		wcout << L"Option                        Description" << endl;
		wcout << L"--------------------------------------------------------------------" << endl;
		wcout << L"/REMARK:\"Text\"                Adds a descriptive comment about the resource." << endl;
		wcout << L"                              Enclose the text in quotation marks." << endl;
		wcout << L"/USERS:number                 Sets the maximum number of users who can" << endl;
		wcout << L"                              simultaneously access the shared resource." << endl;
		wcout << L"/UNLIMITED                    Specifies an unlimited number of users can" << endl;
		wcout << L"                              simultaneously access the shared resource." << endl;
		wcout << L"/GRANT:user,perm              Creates the share with a security descriptor that gives" << endl;
		wcout << L"                              the requested permissions to the specified user.  This" << endl;
		wcout << L"                              option may be used more than once to give share permissions" << endl;
		wcout << L"                              to multiple users.  Using /GRANT on an existing share will" << endl;
		wcout << L"                              replace any existing permissions." << endl;
	}
	else if (L"USER" == command)
	{
		wcout << L"RNET [\\\\server] USER [/LEVEL:num]" << endl;
		wcout << L" Enumerate users." << endl;
		wcout << L"RNET [\\\\server] USER username [/LEVEL:num]" << endl;
		wcout << L" Get info about a user." << endl;
		wcout << L"RNET [\\\\server] USER username {[password | *] | [options]}" << endl;
		wcout << L" Set the password and/or options for a user." << endl;
		wcout << L"RNET [\\\\server] USER username {password | *} /ADD [options]" << endl;
		wcout << L" Add a new user." << endl;
		wcout << L"RNET [\\\\server] USER username /DELETE" << endl;
		wcout << L" Delete a user." << endl;
		wcout << L"Option                        Description" << endl;
		wcout << L"--------------------------------------------------------------------" << endl;
		wcout << L"/ACTIVE:{YES | NO}            Activates or deactivates the account.If" << endl;
		wcout << L"                              the account is not active, the user cannot" << endl;
		wcout << L"                              access the server.The default is YES." << endl;
		wcout << L"/COMMENT:\"text\"               Provides a descriptive comment about the" << endl;
		wcout << L"                              user's account.  Enclose the text in" << endl;
		wcout << L"                              quotation marks." << endl;
		wcout << L"/COUNTRYCODE:nnn              Uses the operating system country/region code" << endl;
		wcout << L"                              to implement the specified language files for" << endl;
		wcout << L"                              a user's help and error messages.  A value of" << endl;
		wcout << L"                              0 signifies the default country/region code." << endl;
		wcout << L"/CODEPAGE:nnn                 Sets the account language code page." << endl;
		wcout << L"                              0 signifies the default code page." << endl;
		wcout << L"/EXPIRES:{date | NEVER}       Causes the account to expire if date is" << endl;
		wcout << L"                              set.  NEVER sets no time limit on the" << endl;
		wcout << L"                              account.  An expiration date is in the" << endl;
		wcout << L"                              form mm/dd/yy(yy).  Months can be a number," << endl;
		wcout << L"                              spelled out, or abbreviated with three" << endl;
		wcout << L"                              letters.  Year can be two or four numbers." << endl;
		wcout << L"                              Use slashes (/) (no spaces) to separate" << endl;
		wcout << L"                              parts of the date." << endl;
		wcout << L"/FULLNAME:\"name\"              Is a user's full name (rather than a" << endl;
		wcout << L"                              username).  Enclose the name in quotation" << endl;
		wcout << L"                              marks." << endl;
		wcout << L"/HOMEDIR:pathname             Sets the path for the user's home directory." << endl;
		wcout << L"                              The path must exist." << endl;
		wcout << L"/PASSWORDCHG:{YES | NO}       Specifies whether users can change their" << endl;
		wcout << L"                              own password.The default is YES." << endl;
		wcout << L"/PASSWORDREQ:{YES | NO}       Specifies whether a user account must have" << endl;
		wcout << L"                              a password.The default is YES." << endl;
		wcout << L"/LOGONPASSWORDCHG:{YES | NO}  Specifies whether user should change their" << endl;
		wcout << L"                              own password at the next logon.  The default is NO." << endl;
		wcout << L"/PROFILEPATH[:path]           Sets a path for the user's logon profile." << endl;
		wcout << L"/SCRIPTPATH:pathname          Is the location of the user's logon" << endl;
		wcout << L"                              script." << endl;
		wcout << L"/USERCOMMENT:\"text\"           Lets an administrator add or change the User" << endl;
		wcout << L"                              Comment for the account." << endl;
		wcout << L"/WORKSTATIONS:{computername[,...] | *}" << endl;
		wcout << L"                              Lists as many as eight computers from" << endl;
		wcout << L"                              which a user can log on to the network.  If" << endl;
		wcout << L"                              /WORKSTATIONS has no list or if the list is *," << endl;
		wcout << L"                              the user can log on from any computer." << endl;
	}
	else if (L"LOCALGROUP" == command)
	{
		wcout << L"RNET [\\\\server] LOCALGROUP" << endl;
		wcout << L" Enumerate localgroups." << endl;
		wcout << L"RNET [\\\\server] LOCALGROUP groupname [/COMMENT:\"text\"]" << endl;
		wcout << L" Enumerate members of a localgroup or set the comment for a localgroup." << endl;
		wcout << L"RNET [\\\\server] LOCALGROUP groupname /ADD [/COMMENT:\"text\"]" << endl;
		wcout << L" Add a localgroup." << endl;
		wcout << L"RNET [\\\\server] LOCALGROUP groupname /DELETE" << endl;
		wcout << L" Delete a localgroup." << endl;
		wcout << L"RNET [\\\\server] LOCALGROUP groupname name ... {/ADD | /DELETE}" << endl;
		wcout << L" Add or delete members to/from a localgroup." << endl;
	}
	else if (L"GROUP" == command)
	{
		wcout << L"RNET [\\\\server] GROUP" << endl;
		wcout << L" Enumerate groups." << endl;
		wcout << L"RNET [\\\\server] GROUP groupname [/COMENT:\"text\"]" << endl;
		wcout << L" Enumerate users of a group or set the comment for a group." << endl;
		wcout << L"RNET [\\\\server] GROUP groupname /ADD [/COMMENT:\"text\"]" << endl;
		wcout << L" Add a group." << endl;
		wcout << L"RNET [\\\\server] GROUP groupname /DELETE" << endl;
		wcout << L" Delete a group." << endl;
		wcout << L"RNET [\\\\server] GROUP groupname username ... {/ADD | /DELETE}" << endl;
		wcout << L" Add or delete users to/from a group." << endl;
	}
	else if (L"SESSION" == command)
	{
		wcout << L"RNET [\\\\server] SESSION [\\\\COMPUTER] [user] [/LEVEL:num]" << endl;
		wcout << L" Enumerate sessions, optionally for a specific computer or user." << endl;
		wcout << L"RNET [\\\\server] SESSION \\\\COMPUTER user [/LEVEL:num]" << endl;
		wcout << L" Gets info about a session from a specific computer and user." << endl;
		wcout << L"RNET [\\\\server] SESSION {\\\\COMPUTER | user} /DELETE" << endl;
		wcout << L" Delete sessions.  All sessions from a specific user from any computer can be" << endl;
		wcout << L" deleted or all sessions from a specific computer can be deleted, or just" << endl;
		wcout << L" a session from one user from a computer can be deleted if both server and" << endl;
		wcout << L" user are supplied." << endl;
	}
	else if (L"SERVICE" == command)
	{
		wcout << L"RNET [\\\\server] SERVICE [SERVICENAME] [/LEVEL:num]" << endl;
		wcout << L" Enumerate services or query a specific service." << endl;
		wcout << L"The functionality to install/uninstall and control (start/stop/pause/continue)" << endl;
		wcout << L"services is not implemented.  The Windows command line tool SC.exe can be" << endl;
		wcout << L"used to perform these operations which uses the Service Control Manager" << endl;
		wcout << L"API rather than Lan Manager." << endl;
	}
	else if (L"USE" == command)
	{
		wcout << L"RNET [\\\\server] USE [/LEVEL:num]" << endl;
		wcout << L" Enumerate uses." << endl;
		wcout << L"RNET [\\\\server] USE localdevice [/LEVEL:num]" << endl;
		wcout << L" Get info about a use." << endl;
		wcout << L"RNET [\\\\server] USE [localdevice] \\\\COMPUTER\\sharename" << endl;
		wcout << L"                      [/USER:[domainname\\]username] [password | *]" << endl;
		wcout << L" Connect a to a remote share optionally redirecting a local device.  Of no username or" << endl;
		wcout << L" password are provided the current credentials are used." << endl;
		wcout << L"RNET [\\\\server] USE { localdevice | \\\\COMPUTER\\sharename } /DELETE" << endl;
		wcout << L" Disconnect a local device or share." << endl;
		wcout << L"Localdevice can be a drive letter such as X: or a printer port such as LPT3:." << endl;
	}
	else if (L"VIEW" == command)
	{
		wcout << L"RNET [\\\\server] VIEW [/DOMAIN:domain] [/LEVEL:num]" << endl;
		wcout << L" Enumerate servers.  If no domain is supplied the machine primary domain is enumerated." << endl;
		wcout << L"Use RNET \\\\server SHARE to enumerate shares on a remote machine." << endl;
	}
	else if (L"CONFIG" == command)
	{
		if (arguments.size() != 2 ||
			(L"SERVER" != StringUpper(arguments[1]) &&
				L"WORKSTATION" != StringUpper(arguments[1])))
		{
			wcout << L"RNET [\\\\server] CONFIG { SERVER | WORKSTATION }" << endl;
			wcout << endl;
			wcout << L"RNET CONFIG displays configuration information of the Workstation or" << endl;
			wcout << L"Server service.  When used without the SERVER or WORKSTATION switch," << endl;
			wcout << L"it displays a list of configurable services.  To get help with" << endl;
			wcout << L"configuring a service, type NET HELP CONFIG service." << endl;
		}
		else if (L"SERVER" == StringUpper(arguments[1]))
		{
			wcout << L"RNET [\\\\server] CONFIG SERVER [/LEVEL:num]" << endl;
			wcout << L" Queries settings for the Server service." << endl;
			wcout << L"RNET [\\\\server] CONFIG SERVER options" << endl;
			wcout << L" Changes settings for the Server service." << endl;
			wcout << L"Option                        Description" << endl;
			wcout << L"--------------------------------------------------------------------" << endl;
			wcout << L"/AUTODISCONNECT:time          Sets the maximum number of minutes a user's" << endl;
			wcout << L"                              session can be inactive before it is disconnected." << endl;
			wcout << L"                              You can specify -1 to never disconnect.  The range" << endl;
			wcout << L"                              is -1 - 65535 minutes; the default is 15." << endl;
			wcout << L"/SRVCOMMENT:\"text\"            Adds a comment for the server that is displayed in" << endl;
			wcout << L"                              Windows Screens and with the NET VIEW command." << endl;
			wcout << L"                              Enclose the text in quotation marks." << endl;
			wcout << L"/HIDDEN:{YES | NO}            Specifies whether the server's computername" << endl;
			wcout << L"                              appears on display listings of servers.  Note that" << endl;
			wcout << L"                              hiding a server does not alter the permissions" << endl;
			wcout << L"                              on that server.  The default is NO." << endl;
		}
		else if (L"WORKSTATION" == StringUpper(arguments[1]))
		{
			wcout << L"RNET [\\\\server] CONFIG WORKSTATION [/LEVEL:num]" << endl;
			wcout << L" Displays settings for the Workstation service." << endl;
		}
	}
	else if (L"NAME" == command)
	{
		wcout << L"RNET [\\\\server] NAME" << endl;
		wcout << L" Enumerte the computer names." << endl;
		wcout << L"RNET [\\\\server] NAME computername { /ADD | /DELETE }" << endl;
		wcout << L" Add or remove an alternate computer name." << endl;
	}
	else if (L"DOMAIN" == command)
	{
		wcout << L"RNET [\\\\server] DOMAIN domain" << endl;
		wcout << L" Display info about the domain." << endl;
		wcout << L"RNET [\\\\server DOMAIN domain /JOIN [/USER:username {password | *}]" << endl;
		wcout << L" Join the computer to a domain.  If no userame or password are provided" << endl;
		wcout << L" the current credentials are used." << endl;
		wcout << L"RNET [\\\\server DOMAIN /UNJOIN [/USER:username {password | *}]" << endl;
		wcout << L" Unjoin the computer from the domain.  If no username or password is" << endl;
		wcout << L" provided the current credentials are used." << endl;
	}
	else if (L"LOGINS" == command)
	{
		wcout << L"RNET [\\\\server] LOGINS [/LEVEL:num]" << endl;
		wcout << L" Enumerate the users currently logged on to the workstation." << endl;
	}
	else
	{
		wcerr << L"Unknown command: " << command << endl;
		return ERROR_BAD_ARGUMENTS;
	}

	return 0;
}

