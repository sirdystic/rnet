/* NetGroup.cpp - Wrappers around NetGroup*() APIs */

#include "NetGroup.h"
#include "Utils.h"
#include "Error.h"
#include "Security.h"
#include "Types.h"
#include "Macros.h"
#include "Xml.h"
#include "Json.h"
#include "Globals.h"

#include <Windows.h>
#include <LM.h>

#include <iostream>


void GetGroupInfoValues0(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetGroupInfoValues1(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetGroupInfoValues2(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetGroupInfoValues3(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);

vector<int> NetGroupEnumTryLevels = { 0, 1, 2, 3 };
vector<int> NetGroupGetInfoTryLevels = { 0, 1, 2, 3 };
levelmap NetGroupGetValuesMap = {
	{ 0, GetGroupInfoValues0 },
	{ 1, GetGroupInfoValues1 },
	{ 2, GetGroupInfoValues2 },
	{ 3, GetGroupInfoValues3 },
};


wstring GroupAttributesToString(DWORD attributes)
{
	wstring ret;
	if (attributes & SE_GROUP_ENABLED)
		ret += L"e";
	if (attributes & SE_GROUP_ENABLED_BY_DEFAULT)
		ret += L"E";
	if (attributes & SE_GROUP_INTEGRITY)
		ret += L"i";
	if (attributes & SE_GROUP_INTEGRITY_ENABLED)
		ret += L"I";
	if (attributes & SE_GROUP_LOGON_ID)
		ret += L"L";
	if (attributes & SE_GROUP_MANDATORY)
		ret += L"M";
	if (attributes & SE_GROUP_OWNER)
		ret += L"O";
	if (attributes & SE_GROUP_RESOURCE)
		ret += L"R";
	if (attributes & SE_GROUP_USE_FOR_DENY_ONLY)
		ret += L"D";

	return ret;
}


void GetGroupInfoValues0(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	LPGROUP_INFO_0 pGroupInfo = reinterpret_cast<LPGROUP_INFO_0>(buffer);
	descriptions.push_back(L"Name");
	text.push_back(pGroupInfo->grpi0_name);
}


void GetGroupInfoValues1(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetGroupInfoValues0(buffer, descriptions, text);

	LPGROUP_INFO_1 pGroupInfo = reinterpret_cast<LPGROUP_INFO_1>(buffer);
	descriptions.push_back(L"Comment");
	text.push_back(pGroupInfo->grpi1_comment);
}


void GetGroupInfoValues2(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetGroupInfoValues1(buffer, descriptions, text);

	PGROUP_INFO_2 pGroupInfo = reinterpret_cast<PGROUP_INFO_2>(buffer);
	descriptions.push_back(L"GroupID");
	text.push_back(to_wstring(pGroupInfo->grpi2_group_id));
	descriptions.push_back(L"Attributes");
	text.push_back(GroupAttributesToString(pGroupInfo->grpi2_attributes));
}


void GetGroupInfoValues3(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetGroupInfoValues1(buffer, descriptions, text);

	PGROUP_INFO_3 pGroupInfo = reinterpret_cast<PGROUP_INFO_3>(buffer);
	descriptions.push_back(L"SID");
	text.push_back(SidToString(pGroupInfo->grpi3_group_sid));
	descriptions.push_back(L"Attributes");
	text.push_back(GroupAttributesToString(pGroupInfo->grpi3_attributes));
}


wstring GroupParamErrorToString(DWORD paramErr)
{
	switch (paramErr)
	{
	case GROUP_NAME_PARMNUM:
		return L"Name";
	case GROUP_COMMENT_PARMNUM:
		return L"Comment";
	case GROUP_ATTRIBUTES_PARMNUM:
		return L"Attributes";
	}

	return L"Unknown";
}


int GroupEnum(const wstring& server, int level)
{
	wcout << L"NetGroupEnum(" << server << L") level " << level << endl;

	LPBYTE buffer = nullptr;
	DWORD entriesRead = 0;
	DWORD totalEntries = 0;
	DWORD_PTR resumeHandle = 0;

	NET_API_STATUS status = NetGroupEnum(
		server.c_str(),
		level,
		&buffer,
		MAX_PREFERRED_LENGTH,
		&entriesRead,
		&totalEntries,
		&resumeHandle
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" enumerating groups: " << ErrorString(status) << endl;
		return status;
	}

	vector<wstring> headers;
	vector<vector<wstring>> values;

	char* ptr = reinterpret_cast<char*>(buffer);
	for (DWORD d = 0; d < entriesRead; d++)
	{
		vector<wstring> descriptions;
		vector<wstring> text;

		LEVELMAPCHECK(NetGroupGetValuesMap, level);
		NetGroupGetValuesMap[level](ptr, descriptions, text);

		if (0 == d)
		{
			headers = descriptions;
			values.resize(headers.size());
		}
		for (size_t n = 0; n < text.size(); n++)
		{
			values[n].push_back(text[n]);
		}

		switch (level)
		{
			POINTERSTRUCTINCCASE(0, ptr, GROUP);
			POINTERSTRUCTINCCASE(1, ptr, GROUP);
			POINTERSTRUCTINCCASE(2, ptr, GROUP);
			POINTERSTRUCTINCCASE(3, ptr, GROUP);
		default:
			wcerr << L"INTERNAL ERROR: MISSING LEVEL IN POINTER INCREMENT SWITCH" << endl;
			exit(ERROR_INTERNAL_ERROR);
		}
	}

	NetApiBufferFree(buffer);

	OutputTable(headers, values);

	if (!g_xmlFilename.empty())
	{
		XmlOutputEnum(g_xmlFilename, level, server, L"group", headers, values);
	}

	if (!g_jsonFilename.empty())
	{
		JsonOutputEnum(g_jsonFilename, level, server, L"group", headers, values);
	}

	return status;
}


vector<wstring> GroupGetUsers(const wstring& server, const wstring& group)
{
	LPBYTE buffer = nullptr;
	DWORD entriesRead = 0;
	DWORD totalEntries = 0;
	DWORD_PTR resumeHandle = 0;

	NET_API_STATUS status = NetGroupGetUsers(
		server.c_str(),
		group.c_str(),
		1,
		&buffer,
		MAX_PREFERRED_LENGTH,
		&entriesRead,
		&totalEntries,
		&resumeHandle
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" enumerating group users: " << ErrorString(status) << endl;
		return {};
	}

	vector<wstring> ret;

	LPGROUP_USERS_INFO_1 pGroupUsers = reinterpret_cast<LPGROUP_USERS_INFO_1>(buffer);
	for (DWORD d = 0; d < entriesRead; d++)
	{
		ret.push_back(pGroupUsers[d].grui1_name);
	}

	NetApiBufferFree(buffer);

	return ret;
}


int GroupGetInfo(const wstring& server, const wstring& group, int level)
{
	wcout << L"NetGroupGetInfo(" << server << L", " << group << L") level " << level << endl;

	LPBYTE buffer = nullptr;

	NET_API_STATUS status = NetGroupGetInfo(
		server.c_str(),
		group.c_str(),
		level,
		&buffer
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" getting group info: " << ErrorString(status) << endl;
		return status;
	}

	vector<wstring> descriptions;
	vector<wstring> text;

	LEVELMAPCHECK(NetGroupGetValuesMap, level);
	NetGroupGetValuesMap[level](buffer, descriptions, text);

	vector<vector<wstring>> values;
	values.push_back(descriptions);
	values.push_back(text);

	OutputTable({}, values);

	wcout << endl;

	NetApiBufferFree(buffer);

	vector<wstring> users = GroupGetUsers(server, group);

	if (!users.empty())
	{
		wcout << L"Users" << endl;
		wcout << wstring(20, L'-') << endl;
		for (const auto& u : users)
			wcout << u << endl;
	}

	if (!g_xmlFilename.empty())
	{
		XmlOutputInfo(
			g_xmlFilename,
			level,
			server,
			L"group",
			{ make_pair(L"group", group) },
			values,
			{ make_pair(L"member", users) }
		);
	}

	if (!g_jsonFilename.empty())
	{
		JsonOutputInfo(
			g_jsonFilename,
			level,
			server,
			L"group",
			{ make_pair(L"group", group) },
			values,
			{ make_pair(L"member", users) }
		);
	}

	return status;
}


int GroupAdd(const wstring& server, const wstring& group, const wstring& comment)
{
	GROUP_INFO_1 groupInfo;
	DWORD paramErr = 0;

	groupInfo.grpi1_name = (LPWSTR)group.c_str();
	groupInfo.grpi1_comment = (LPWSTR)comment.c_str();

	NET_API_STATUS status = NetGroupAdd(
		server.c_str(),
		1,
		reinterpret_cast<LPBYTE>(&groupInfo),
		&paramErr
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" adding group: " << ErrorString(status) << endl;
		if (ERROR_INVALID_PARAMETER == status)
		{
			wcerr << L"Parameter error " << paramErr << L": " << GroupParamErrorToString(paramErr) << endl;
		}
	}
	else
	{
		wcout << L"Group '" << group << L"' added." << endl;
	}

	return status;
}


int GroupDel(const wstring& server, const wstring& group)
{
	NET_API_STATUS status = NetGroupDel(
		server.c_str(),
		group.c_str()
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" deleting group: " << ErrorString(status) << endl;
	}
	else
	{
		wcout << L"Group '" << group << L"' deleted." << endl;
	}

	return status;

}


int GroupSetInfo(const wstring& server, const wstring& group, const wstring& comment)
{
	GROUP_INFO_1002 groupInfo;
	DWORD paramErr = 0;

	groupInfo.grpi1002_comment = (LPWSTR)comment.c_str();

	NET_API_STATUS status = NetGroupSetInfo(
		server.c_str(),
		group.c_str(),
		1002,
		reinterpret_cast<LPBYTE>(&groupInfo),
		&paramErr
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" setting group comment: " << ErrorString(status) << endl;
		if (ERROR_INVALID_PARAMETER == status)
		{
			wcerr << L"Parameter error " << paramErr << L": " << GroupParamErrorToString(paramErr) << endl;
		}
	}
	else
	{
		wcout << L"Group '" << group << L"' comment set." << endl;
	}

	return status;
}


int GroupAddUsers(const wstring& server, const wstring& group, const vector<wstring>& users)
{
	NET_API_STATUS status = 0;
	NET_API_STATUS ret = 0;
	
	for (const auto& user : users)
	{
		status = NetGroupAddUser(
			server.c_str(),
			group.c_str(),
			user.c_str()
		);

		if (NERR_Success != status)
		{
			wcerr << L"Error " << status << L" adding user '" << user << L"' to group '" << group << L"': " << ErrorString(status) << endl;
			ret = status;
		}
		else
		{
			wcout << L"User '" << user << L" added to group '" << group << L"'." << endl;
		}
	}

	return ret;
}


int GroupDelUsers(const wstring& server, const wstring& group, const vector<wstring>& users)
{
	NET_API_STATUS status = 0;
	NET_API_STATUS ret = 0;

	for (const auto& user : users)
	{
		wcout << L"NetGroupDelUser(" << server << L", " << group << L", " << user << L")" << endl;

		status = NetGroupDelUser(
			server.c_str(),
			group.c_str(),
			user.c_str()
		);

		if (NERR_Success != status)
		{
			wcerr << L"Error " << status << L" deleting user '" << user << L"' from group '" << group << L"': " << ErrorString(status) << endl;
			ret = status;
		}
		else
		{
			wcout << L"User '" << user << L" deleted from group '" << group << L"'." << endl;
		}
	}

	return ret;
}


