/* NetUser.cpp - NetUser*() wrappers */

#include "NetUser.h"
#include "Error.h"
#include "Utils.h"
#include "Security.h"
#include "Types.h"
#include "Macros.h"
#include "Xml.h"
#include "Json.h"
#include "Globals.h"

#include <Windows.h>
#include <LM.h>

#include <iostream>


void GetUserInfoValues0(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetUserInfoValues1(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetUserInfoValues2(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetUserInfoValues3(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetUserInfoValues10(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetUserInfoValues11(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetUserInfoValues20(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetUserInfoValues23(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetUserInfoValues24(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);

vector<int> NetUserEnumTryLevels = { 0, 20, 23, 10, 1, 2, 11, 3 };
vector<int> NetUserGetInfoTryLevels = { 0, 20, 23, 10, 1, 2, 11, 3 };
vector<int> NetUserGetInfoValidLevels = { 0, 1, 2, 3, 10, 11, 23, 24 };
levelmap NetUserGetValuesMap = {
	{ 0, GetUserInfoValues0 },
	{ 1, GetUserInfoValues1 },
	{ 2, GetUserInfoValues2 },
	{ 3, GetUserInfoValues3 },
	{ 10, GetUserInfoValues10 },
	{ 11, GetUserInfoValues11 },
	{ 20, GetUserInfoValues20 },
	{ 23, GetUserInfoValues23 },
	{ 24, GetUserInfoValues24 },
};


wstring UserFlagsToString(DWORD flags)
{
	wstring ret;
	if (flags & UF_SCRIPT)
		ret += L"O";
	if (flags & UF_ACCOUNTDISABLE)
		ret += L"D";
	if (flags & UF_PASSWD_NOTREQD)
		ret += L"P";
	if (flags & UF_PASSWD_CANT_CHANGE)
		ret += L"p";
	if (flags & UF_LOCKOUT)
		ret += L"L";
	if (flags & UF_DONT_EXPIRE_PASSWD)
		ret += L"X";
	if (flags & UF_ENCRYPTED_TEXT_PASSWORD_ALLOWED)
		ret += L"E";
	if (flags & UF_NOT_DELEGATED)
		ret += L"g";
	if (flags & UF_SMARTCARD_REQUIRED)
		ret += L"s";
	if (flags & UF_USE_DES_KEY_ONLY)
		ret += L"d";
	if (flags & UF_DONT_REQUIRE_PREAUTH)
		ret += L"k";
	if (flags & UF_TRUSTED_FOR_DELEGATION)
		ret += L"t";
	if (flags & UF_PASSWORD_EXPIRED)
		ret += L"e";
	if (flags & UF_TRUSTED_TO_AUTHENTICATE_FOR_DELEGATION)
		ret += L"A";
	if (flags & UF_NORMAL_ACCOUNT)
		ret += L"N";
	if (flags & UF_TEMP_DUPLICATE_ACCOUNT)
		ret += L"T";
	if (flags & UF_WORKSTATION_TRUST_ACCOUNT)
		ret += L"W";
	if (flags & UF_SERVER_TRUST_ACCOUNT)
		ret += L"S";
	if (flags & UF_INTERDOMAIN_TRUST_ACCOUNT)
		ret += L"I";

	return ret;
}


wstring UserPrivToString(DWORD priv)
{
	switch (priv)
	{
	case USER_PRIV_GUEST:
		return L"Guest";
	case USER_PRIV_USER:
		return L"User";
	case USER_PRIV_ADMIN:
		return L"Admin";
	}

	return L"? " + to_wstring(priv);
}


void GetUserInfoValues0(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	LPUSER_INFO_0 pUserInfo = reinterpret_cast<LPUSER_INFO_0>(buffer);
	descriptions.push_back(L"Name");
	text.push_back(pUserInfo->usri0_name);
}


void GetUserInfoValues1(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetUserInfoValues0(buffer, descriptions, text);

	LPUSER_INFO_1 pUserInfo = reinterpret_cast<LPUSER_INFO_1>(buffer);
	descriptions.push_back(L"Password age");
	text.push_back(TimeToString(pUserInfo->usri1_password_age));
	descriptions.push_back(L"Priv lvl");
	text.push_back(UserPrivToString(pUserInfo->usri1_priv));
	descriptions.push_back(L"Home dir");
	text.push_back(pUserInfo->usri1_home_dir);
	descriptions.push_back(L"Comment");
	text.push_back(pUserInfo->usri1_comment);
	descriptions.push_back(L"Flags");
	text.push_back(UserFlagsToString(pUserInfo->usri1_flags));
	descriptions.push_back(L"Script path");
	text.push_back(nullptr == pUserInfo->usri1_script_path ? L"" : pUserInfo->usri1_script_path);
}


void GetUserInfoValues2(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetUserInfoValues1(buffer, descriptions, text);

	LPUSER_INFO_2 pUserInfo = reinterpret_cast<LPUSER_INFO_2>(buffer);
	// Skip auth_flags?
	descriptions.push_back(L"Full name");
	text.push_back(pUserInfo->usri2_full_name);
	descriptions.push_back(L"User comment");
	text.push_back(pUserInfo->usri2_usr_comment);
	descriptions.push_back(L"Parms");
	text.push_back(pUserInfo->usri2_parms);
	descriptions.push_back(L"Workstations");
	text.push_back(pUserInfo->usri2_workstations);
	descriptions.push_back(L"Last logon");
	text.push_back(TimeToString(pUserInfo->usri2_last_logon));
	// Skip last logoff, unused
	descriptions.push_back(L"Account expires");
	text.push_back(TIMEQ_FOREVER == pUserInfo->usri2_acct_expires ? L"Never" : TimeToString(pUserInfo->usri2_acct_expires));
	descriptions.push_back(L"Max storrage");
	text.push_back(USER_MAXSTORAGE_UNLIMITED == pUserInfo->usri2_max_storage ? L"Unlimited" : to_wstring(pUserInfo->usri2_max_storage));
	// Skip units_per_week
	// Skip logon hours (TODO?)
	descriptions.push_back(L"Bad pw count");
	text.push_back(to_wstring(pUserInfo->usri2_bad_pw_count));
	descriptions.push_back(L"Logon count");
	text.push_back(to_wstring(pUserInfo->usri2_num_logons));
	descriptions.push_back(L"Logon server");
	text.push_back(pUserInfo->usri2_logon_server);
	descriptions.push_back(L"CC");
	text.push_back(to_wstring(pUserInfo->usri2_country_code));
	descriptions.push_back(L"CP");
	text.push_back(to_wstring(pUserInfo->usri2_code_page));
}


void GetUserInfoValues3(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetUserInfoValues2(buffer, descriptions, text);

	LPUSER_INFO_3 pUserInfo = reinterpret_cast<LPUSER_INFO_3>(buffer);
	descriptions.push_back(L"UserID");
	text.push_back(to_wstring(pUserInfo->usri3_user_id));
	descriptions.push_back(L"GroupID");
	text.push_back(to_wstring(pUserInfo->usri3_primary_group_id));
	descriptions.push_back(L"Profile");
	text.push_back(pUserInfo->usri3_profile);
	// Skip home dir drive
	descriptions.push_back(L"PW expired");
	text.push_back(0 == pUserInfo->usri3_password_expired ? L"No" : L"Yes");
}


void GetUserInfoValues10(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetUserInfoValues0(buffer, descriptions, text);

	LPUSER_INFO_10 pUserInfo = reinterpret_cast<LPUSER_INFO_10>(buffer);
	descriptions.push_back(L"Comment");
	text.push_back(pUserInfo->usri10_comment);
	descriptions.push_back(L"User comment");
	text.push_back(pUserInfo->usri10_usr_comment);
	descriptions.push_back(L"Full name");
	text.push_back(pUserInfo->usri10_full_name);
}


void GetUserInfoValues11(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetUserInfoValues10(buffer, descriptions, text);

	LPUSER_INFO_11 pUserInfo = reinterpret_cast<LPUSER_INFO_11>(buffer);
	descriptions.push_back(L"Priv lvl");
	text.push_back(UserPrivToString(pUserInfo->usri11_priv));
	// Skip auth_flags
	descriptions.push_back(L"Password age");
	text.push_back(TimeToString(pUserInfo->usri11_password_age));
	descriptions.push_back(L"Home dir");
	text.push_back(pUserInfo->usri11_home_dir);
	descriptions.push_back(L"Parms");
	text.push_back(pUserInfo->usri11_parms);
	descriptions.push_back(L"Last logon");
	text.push_back(TimeToString(pUserInfo->usri11_last_logon));
	// skip last_logoff, unused
	descriptions.push_back(L"Bad pw count");
	text.push_back(to_wstring(pUserInfo->usri11_bad_pw_count));
	descriptions.push_back(L"Logon count");
	text.push_back(to_wstring(pUserInfo->usri11_num_logons));
	descriptions.push_back(L"Logon server");
	text.push_back(pUserInfo->usri11_logon_server);
	descriptions.push_back(L"CC");
	text.push_back(to_wstring(pUserInfo->usri11_country_code));
	descriptions.push_back(L"Workstations");
	text.push_back(pUserInfo->usri11_workstations);
	descriptions.push_back(L"Max storrage");
	text.push_back(USER_MAXSTORAGE_UNLIMITED == pUserInfo->usri11_max_storage ? L"Unlimited" : to_wstring(pUserInfo->usri11_max_storage));
	descriptions.push_back(L"CP");
	text.push_back(to_wstring(pUserInfo->usri11_code_page));
}


void GetUserInfoValues20(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetUserInfoValues0(buffer, descriptions, text);

	LPUSER_INFO_20 pUserInfo = reinterpret_cast<LPUSER_INFO_20>(buffer);
	descriptions.push_back(L"Full name");
	text.push_back(pUserInfo->usri20_full_name);
	descriptions.push_back(L"Comment");
	text.push_back(pUserInfo->usri20_comment);
	descriptions.push_back(L"Flags");
	text.push_back(UserFlagsToString(pUserInfo->usri20_flags));
	descriptions.push_back(L"UserID");
	text.push_back(to_wstring(pUserInfo->usri20_user_id));
}


void GetUserInfoValues23(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetUserInfoValues0(buffer, descriptions, text);

	LPUSER_INFO_23 pUserInfo = reinterpret_cast<LPUSER_INFO_23>(buffer);
	descriptions.push_back(L"Full name");
	text.push_back(pUserInfo->usri23_full_name);
	descriptions.push_back(L"Comment");
	text.push_back(pUserInfo->usri23_comment);
	descriptions.push_back(L"Flags");
	text.push_back(UserFlagsToString(pUserInfo->usri23_flags));
	descriptions.push_back(L"SID");
	text.push_back(SidToString(pUserInfo->usri23_user_sid));
}


void GetUserInfoValues24(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	LPUSER_INFO_24 pUserInfo = reinterpret_cast<LPUSER_INFO_24>(buffer);
	if (FALSE == pUserInfo->usri24_internet_identity)
	{
		descriptions.push_back(L"Identity");
		text.push_back(L"Not internet");
	}
	else
	{
		descriptions.push_back(L"Identity");
		text.push_back(L"Internet");
		descriptions.push_back(L"Provider");
		text.push_back(pUserInfo->usri24_internet_provider_name);
		descriptions.push_back(L"Principal");
		text.push_back(pUserInfo->usri24_internet_principal_name);
		descriptions.push_back(L"SID");
		text.push_back(SidToString(pUserInfo->usri24_user_sid));
	}
}


wstring UserParamErrorToString(DWORD paramErr)
{
	switch (paramErr)
	{
	case USER_NAME_PARMNUM:
		return L"User name";
	case USER_PASSWORD_PARMNUM:
		return L"Password";
	case USER_PASSWORD_AGE_PARMNUM:
		return L"Password age";
	case USER_PRIV_PARMNUM:
		return L"Privilege";
	case USER_HOME_DIR_PARMNUM:
		return L"Home directory";
	case USER_COMMENT_PARMNUM:
		return L"Comment";
	case USER_FLAGS_PARMNUM:
		return L"Flags";
	case USER_SCRIPT_PATH_PARMNUM:
		return L"Script path";
	case USER_AUTH_FLAGS_PARMNUM:
		return L"Auth flags";
	case USER_FULL_NAME_PARMNUM:
		return L"Full name";
	case USER_USR_COMMENT_PARMNUM:
		return L"User comment";
	case USER_PARMS_PARMNUM:
		return L"Params";
	case USER_WORKSTATIONS_PARMNUM:
		return L"Workstations";
	case USER_LAST_LOGON_PARMNUM:
		return L"Last logon";
	case USER_LAST_LOGOFF_PARMNUM:
		return L"Last logoff";
	case USER_ACCT_EXPIRES_PARMNUM:
		return L"Acct expires";
	case USER_MAX_STORAGE_PARMNUM:
		return L"Max storage";
	case USER_UNITS_PER_WEEK_PARMNUM:
		return L"Units per week";
	case USER_LOGON_HOURS_PARMNUM:
		return L"Logon hours";
	case USER_PAD_PW_COUNT_PARMNUM:
		return L"Bad password count";
	case USER_NUM_LOGONS_PARMNUM:
		return L"Num logons";
	case USER_LOGON_SERVER_PARMNUM:
		return L"Logon server";
	case USER_COUNTRY_CODE_PARMNUM:
		return L"Country code";
	case USER_CODE_PAGE_PARMNUM:
		return L"Code page";
	case USER_PRIMARY_GROUP_PARMNUM:
		return L"Primary group";
	case USER_PROFILE_PARMNUM:
		return L"Profile";
	case USER_HOME_DIR_DRIVE_PARMNUM:
		return L"Home dir drive";
	}

	return L"Unknown";
}


vector<wstring> UserGetLocalGroups(const wstring& server, const wstring& user)
{
	wcout << L"NetUserGetLocalGroups(" << server << L", " << user << L")" << endl;

	LPBYTE buffer = nullptr;
	DWORD entriesRead = 0;
	DWORD totalEntries = 0;

	NET_API_STATUS status = NetUserGetLocalGroups(
		server.c_str(),
		user.c_str(),
		0,
		LG_INCLUDE_INDIRECT,
		&buffer,
		MAX_PREFERRED_LENGTH,
		&entriesRead,
		&totalEntries
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" enumerating user localgroups: " << ErrorString(status) << endl;
		return {};
	}

	vector<wstring> ret;

	LPLOCALGROUP_USERS_INFO_0 pLocalGroupUsersInfo = reinterpret_cast<LPLOCALGROUP_USERS_INFO_0>(buffer);
	for (DWORD d = 0; d < entriesRead; d++)
	{
		ret.push_back(pLocalGroupUsersInfo[d].lgrui0_name);
	}

	NetApiBufferFree(buffer);

	return ret;
}


vector<wstring> UserGetGroups(const wstring& server, const wstring& user)
{
	wcout << L"NetUserGetGroups(" << server << L", " << user << L")" << endl;

	LPBYTE buffer = nullptr;
	DWORD entriesRead = 0;
	DWORD totalEntries = 0;

	NET_API_STATUS status = NetUserGetGroups(
		server.c_str(),
		user.c_str(),
		0,
		&buffer,
		MAX_PREFERRED_LENGTH,
		&entriesRead,
		&totalEntries
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" enumerating user groups: " << ErrorString(status) << endl;
		return {};
	}

	vector<wstring> ret;

	LPGROUP_USERS_INFO_0 pLocalGroupUsersInfo = reinterpret_cast<LPGROUP_USERS_INFO_0>(buffer);
	for (DWORD d = 0; d < entriesRead; d++)
	{
		ret.push_back(pLocalGroupUsersInfo[d].grui0_name);
	}

	NetApiBufferFree(buffer);

	return ret;
}


int UserEnum(const wstring& server, int level)
{
	wcout << L"NetUserEnum(" << server << L") level " << level << endl;

	LPBYTE buffer = nullptr;
	DWORD entriesRead = 0;
	DWORD totalEntries = 0;
	DWORD resumeHandle = 0;

	NET_API_STATUS status = NetUserEnum(
		server.c_str(),
		level,
		0,
		&buffer,
		MAX_PREFERRED_LENGTH,
		&entriesRead,
		&totalEntries,
		&resumeHandle
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" enumerating users: " << ErrorString(status) << endl;
		return status;
	}

	vector<wstring> headers;
	vector<vector<wstring>> values;

	char* ptr = reinterpret_cast<char*>(buffer);
	for (DWORD d = 0; d < entriesRead; d++)
	{
		vector<wstring> descriptions;
		vector<wstring> text;

		LEVELMAPCHECK(NetUserGetValuesMap, level);
		NetUserGetValuesMap[level](ptr, descriptions, text);

		if (0 == d)
		{
			headers = descriptions;
			values.resize(headers.size());
		}
		for (size_t n = 0; n < text.size(); n++)
		{
			values[n].push_back(text[n]);
		}

		switch (level)
		{
			POINTERSTRUCTINCCASE(0, ptr, USER);
			POINTERSTRUCTINCCASE(1, ptr, USER);
			POINTERSTRUCTINCCASE(2, ptr, USER);
			POINTERSTRUCTINCCASE(3, ptr, USER);
			POINTERSTRUCTINCCASE(10, ptr, USER);
			POINTERSTRUCTINCCASE(11, ptr, USER);
			POINTERSTRUCTINCCASE(20, ptr, USER);
			POINTERSTRUCTINCCASE(23, ptr, USER);
		default:
			wcerr << L"INTERNAL ERROR: MISSING LEVEL IN POINTER INCREMENT SWITCH" << endl;
			exit(ERROR_INTERNAL_ERROR);
		}
	}

	NetApiBufferFree(buffer);

	OutputTable(headers, values);

	if (!g_xmlFilename.empty())
	{
		XmlOutputEnum(g_xmlFilename, level, server, L"user", headers, values);
	}

	if (!g_jsonFilename.empty())
	{
		JsonOutputEnum(g_jsonFilename, level, server, L"user", headers, values);
	}


	return status;
}


int UserGetInfo(const wstring& server, const wstring& user, int level)
{
	wcout << L"NetUserGetInfo(" << server << L", " << user << L") level " << level << endl;

	LPBYTE buffer = nullptr;

	NET_API_STATUS status = NetUserGetInfo(
		server.c_str(),
		user.c_str(),
		level,
		&buffer
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" getting user info: " << ErrorString(status) << endl;
		return status;
	}

	vector<wstring> descriptions;
	vector<wstring> text;

	LEVELMAPCHECK(NetUserGetValuesMap, level);
	NetUserGetValuesMap[level](buffer, descriptions, text);

	NetApiBufferFree(buffer);

	vector<vector<wstring>> values;
	values.push_back(descriptions);
	values.push_back(text);

	OutputTable({}, values);

	wcout << endl;

	vector<wstring> localgroups = UserGetLocalGroups(server, user);

	wcout << L"Local Group membership: ";
	for (const auto& g : localgroups)
		wcout << g << L"    ";
	wcout << endl;

	vector<wstring> globalgroups = UserGetGroups(server, user);

	wcout << L"Global Group membership: ";
	for (const auto& g : globalgroups)
		wcout << g << L"    ";
	wcout << endl;

	if (!g_xmlFilename.empty())
	{
		XmlOutputInfo(
			g_xmlFilename,
			level,
			server,
			L"user",
			{ make_pair(L"username", user) },
			values,
			{ make_pair(L"localgroup", localgroups),
			  make_pair(L"globalgroup", globalgroups) }
		);
	}

	if (!g_jsonFilename.empty())
	{
		JsonOutputInfo(
			g_jsonFilename,
			level,
			server,
			L"user",
			{ make_pair(L"username", user) },
			values,
			{ make_pair(L"localgroup", localgroups),
			make_pair(L"globalgroup", globalgroups) }
		);
	}

	return 0;
}


bool SetUserInfo3Options(LPUSER_INFO_3 pUserInfo, const vector<wstring>& options, vector<wstring>& stringTable)
{
	for (size_t n = 0; n < options.size(); n++)
	{
		if (L"/ACTIVE" == StringUpper(options[n]) ||
			L"/ACTIVE:YES" == StringUpper(options[n]))
		{
			pUserInfo->usri3_flags &= ~UF_ACCOUNTDISABLE;
		}
		else if (L"/ACTIVE:NO" == StringUpper(options[n]))
		{
			pUserInfo->usri3_flags |= UF_ACCOUNTDISABLE;
		}
		else if (0 == StringUpper(options[n]).find(L"/COMMENT:"))
		{
			stringTable.push_back(options[n].substr(9));
			pUserInfo->usri3_comment = (LPWSTR)stringTable.rbegin()->c_str();
		}
		else if (0 == StringUpper(options[n]).find(L"/FULLNAME:"))
		{
			stringTable.push_back(options[n].substr(10));
			pUserInfo->usri3_full_name = (LPWSTR)stringTable.rbegin()->c_str();
		}
		else if (0 == StringUpper(options[n]).find(L"/HOMEDIR:"))
		{
			stringTable.push_back(options[n].substr(9));
			pUserInfo->usri3_home_dir = (LPWSTR)stringTable.rbegin()->c_str();
		}
		else if (L"/PASSWORDCHG:NO" == StringUpper(options[n]))
		{
			pUserInfo->usri3_flags |= UF_PASSWD_CANT_CHANGE;
		}
		else if (L"/PASSWORDCHG:YES" == StringUpper(options[n]))
		{
			pUserInfo->usri3_flags &= ~UF_PASSWD_CANT_CHANGE;
		}
		else if (L"/PASSWORDREQ:NO" == StringUpper(options[n]))
		{
			pUserInfo->usri3_flags &= ~UF_PASSWD_NOTREQD;
		}
		else if (L"/PASSWORDREQ:YES" == StringUpper(options[n]))
		{
			pUserInfo->usri3_flags |= UF_PASSWD_NOTREQD;
		}
		else if (L"/LOGONPASSWORDCHG:YES" == StringUpper(options[n]))
		{
			pUserInfo->usri3_password_expired = 1;
		}
		else if (L"/LOGONPASSWORDCHG:NO" == StringUpper(options[n]))
		{
			pUserInfo->usri3_password_expired = 0;
		}
		else if (L"/PROFILEPATH" == StringUpper(options[n]))
		{
			stringTable.push_back(wstring());
			pUserInfo->usri3_profile = (LPWSTR)stringTable.rbegin()->c_str();
		}
		else if (0 == StringUpper(options[n]).find(L"/PROFILEPATH:"))
		{
			stringTable.push_back(options[n].substr(14));
			pUserInfo->usri3_profile = (LPWSTR)stringTable.rbegin()->c_str();
		}
		else if (0 == StringUpper(options[n]).find(L"/SCRIPTPATH:"))
		{
			stringTable.push_back(options[n].substr(12));
			pUserInfo->usri3_script_path = (LPWSTR)stringTable.rbegin()->c_str();
		}
		else if (0 == StringUpper(options[n]).find(L"/USERCOMMENT:"))
		{
			stringTable.push_back(options[n].substr(13));
			pUserInfo->usri3_usr_comment = (LPWSTR)stringTable.rbegin()->c_str();
		}
		else if (0 == StringUpper(options[n]).find(L"/COUNTRYCODE:"))
		{
			wstring val = options[n].substr(13);
			// validate?
			pUserInfo->usri3_country_code = stoul(val);
		}
		else if (0 == StringUpper(options[n]).find(L"/CODEPAGE:"))
		{
			wstring val = options[n].substr(10);
			// validate?
			pUserInfo->usri3_code_page = stoul(val);
		}
		else if (L"/EXPIRES:NEVER" == StringUpper(options[n]))
		{
			pUserInfo->usri3_acct_expires = TIMEQ_FOREVER;
		}
		else if (0 == StringUpper(options[n]).find(L"/EXPIRES:"))
		{
			DWORD time = StringToTime(options[n].substr(9));
			if (0 == time)
			{
				wcerr << L"Invalid time format: " << options[n] << endl;
				return false;
			}
			pUserInfo->usri3_acct_expires = time;
		}
		else if (L"/WORKSTATIONS:*" == StringUpper(options[n]))
		{
			pUserInfo->usri3_workstations = nullptr;
		}
		else if (0 == StringUpper(options[n]).find(L"/WORKSTATIONS:"))
		{
			stringTable.push_back(options[n].substr(14));
			pUserInfo->usri3_workstations = (LPWSTR)stringTable.rbegin()->c_str();
		}
		else
		{
			wcerr << L"Bad option: " << options[n] << endl;
			return false;
		}
	}

	return true;
}


int UserAdd(const wstring& server, const wstring& user, const wstring& password, const vector<wstring>& options)
{
	wcout << L"NetUserAdd(" << server << L")" << endl;

	USER_INFO_3 userInfo;
	DWORD paramErr = 0;

	userInfo.usri3_name = (LPWSTR)user.c_str();
	userInfo.usri3_password = (LPWSTR)password.c_str();
	userInfo.usri3_password_age = 0;
	userInfo.usri3_priv = USER_PRIV_ADMIN;
	userInfo.usri3_home_dir = nullptr;
	userInfo.usri3_comment = nullptr;
	userInfo.usri3_flags = UF_NORMAL_ACCOUNT;
	userInfo.usri3_script_path = nullptr;
	userInfo.usri3_auth_flags = 0;
	userInfo.usri3_full_name = nullptr;
	userInfo.usri3_usr_comment = nullptr;
	userInfo.usri3_parms = nullptr;
	userInfo.usri3_workstations = nullptr;
	userInfo.usri3_last_logon = 0;
	userInfo.usri3_last_logoff = 0;
	userInfo.usri3_acct_expires = TIMEQ_FOREVER;
	userInfo.usri3_max_storage = USER_MAXSTORAGE_UNLIMITED;
	userInfo.usri3_units_per_week = UNITS_PER_WEEK; // ignored
	userInfo.usri3_logon_hours = nullptr;
	userInfo.usri3_bad_pw_count = 0;
	userInfo.usri3_num_logons = 0;
	userInfo.usri3_logon_server = nullptr;
	userInfo.usri3_country_code = 0;
	userInfo.usri3_code_page = 0;
	userInfo.usri3_user_id = 0; // ignored
	userInfo.usri3_primary_group_id = DOMAIN_GROUP_RID_USERS;
	userInfo.usri3_profile = nullptr;
	userInfo.usri3_home_dir_drive = nullptr;
	userInfo.usri3_password_expired = 0;

	// Need SetUserInfo2Options to return some strings on the stack to pass to NetUserAdd
	// They just need to be in memory on the heap/stack (pointed to by userInfo)
	vector<wstring> stringTable;
	stringTable.reserve(32);

	if (!SetUserInfo3Options(&userInfo, options, stringTable))
	{
		return ERROR_INVALID_PARAMETER;
	}

	NET_API_STATUS status = NetUserAdd(
		server.c_str(),
		3,
		reinterpret_cast<LPBYTE>(&userInfo),
		&paramErr
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" adding user: " << ErrorString(status) << endl;
		if (ERROR_INVALID_PARAMETER == status)
		{
			wcerr << L"Parameter error " << paramErr << L": " << UserParamErrorToString(paramErr) << endl;
		}
	}
	else
	{
		wcout << L"User '" << user << L"' added." << endl;
	}

	return status;
}


int UserSetInfo(const wstring& server, const wstring& user, const wstring& password, bool setPassword, const vector<wstring>& options)
{
	wcout << L"NetUserGetInfo(" << server << L", " << user << L") level 3" << endl;

	LPBYTE buffer = nullptr;
	NET_API_STATUS status = NetUserGetInfo(
		server.c_str(),
		user.c_str(),
		3,
		&buffer
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" getting user info: " << ErrorString(status) << endl;
		return status;
	}

	LPUSER_INFO_3 pUserInfo = reinterpret_cast<LPUSER_INFO_3>(buffer);

	if (setPassword)
	{
		pUserInfo->usri3_password = (LPWSTR)password.c_str();
	}

	// Need SetUserInfo2Options to return some strings on the stack to pass to NetUserSetInfo
	// They just need to be in memory on the heap/stack (pointed to by userInfo)
	vector<wstring> stringTable;
	stringTable.reserve(32);

	if (!SetUserInfo3Options(pUserInfo, options, stringTable))
	{
		return ERROR_INVALID_PARAMETER;
	}

	wcout << L"NetUserSetInfo(" << server << L", " << user << L") level 3" << endl;

	DWORD paramErr = 0;
	status = NetUserSetInfo(
		server.c_str(),
		user.c_str(),
		3,
		buffer,
		&paramErr
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" setting user info: " << ErrorString(status) << endl;
		if (ERROR_INVALID_PARAMETER == status)
		{
			wcerr << L"Parameter error " << paramErr << L": " << UserParamErrorToString(paramErr) << endl;
		}
	}

	NetApiBufferFree(buffer);

	return status;
}


int UserDel(const wstring& server, const wstring& user)
{
	wcout << L"NetUserDel(" << server << L", " << user << L")" << endl;

	NET_API_STATUS status = NetUserDel(
		server.c_str(),
		user.c_str()
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" deleting users: " << ErrorString(status) << endl;
	}
	else
	{
		wcout << L"User '" << user << L"' deleted." << endl;
	}

	return status;
}


int SetUserPassword(const wstring& server, const wstring& user, const wstring& password)
{
	wcout << L"NetUserSetInfo(" << server << L", " << user << L") level 1003" << endl;

	USER_INFO_1003 userInfo;
	DWORD paramErr = 0;

	userInfo.usri1003_password = (LPWSTR)password.c_str();

	NET_API_STATUS status = NetUserSetInfo(
		server.c_str(),
		user.c_str(),
		1003,
		reinterpret_cast<LPBYTE>(&userInfo),
		&paramErr
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" setting password: " << ErrorString(status) << endl;
		if (ERROR_INVALID_PARAMETER == status)
		{
			wcerr << L"Parameter error " << paramErr << L": " << UserParamErrorToString(paramErr) << endl;
		}
	}
	else
	{
		wcout << L"Password set for user '" << user << L"'." << endl;
	}

	return status;
}



