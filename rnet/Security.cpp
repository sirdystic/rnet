/* Security.cpp - Seceurity related functions */

#include "Security.h"
#include "Error.h"

#include <Aclapi.h>
#include <AccCtrl.h>
#include <Sddl.h>

#include <iostream>


TRUSTEE_TYPE SidUseToTrusteeType(SID_NAME_USE use)
{
	switch (use)
	{
	case SidTypeUser:
		return TRUSTEE_IS_USER;
	case SidTypeGroup:
		return TRUSTEE_IS_GROUP;
	case SidTypeDomain:
		return TRUSTEE_IS_DOMAIN;
	case SidTypeAlias:
		return TRUSTEE_IS_ALIAS;
	case SidTypeWellKnownGroup:
		return TRUSTEE_IS_WELL_KNOWN_GROUP;
	case SidTypeDeletedAccount:
		return TRUSTEE_IS_DELETED;
	case SidTypeInvalid:
		return TRUSTEE_IS_INVALID;
	case SidTypeUnknown:
		return TRUSTEE_IS_UNKNOWN;
	case SidTypeComputer:
		return TRUSTEE_IS_COMPUTER;
	case SidTypeLabel:
		return TRUSTEE_IS_UNKNOWN;
	}

	return TRUSTEE_IS_UNKNOWN;
}


wstring SidUseToString(SID_NAME_USE use)
{
	switch (use)
	{
	case SidTypeUser:
		return L"User";
	case SidTypeGroup:
		return L"Group";
	case SidTypeDomain:
		return L"Domain";
	case SidTypeAlias:
		return L"Alias";
	case SidTypeWellKnownGroup:
		return L"WellKnownGroup";
	case SidTypeDeletedAccount:
		return L"DeletedAccount";
	case SidTypeInvalid:
		return L"Invalid";
	case SidTypeUnknown:
		return L"Unknown";
	case SidTypeComputer:
		return L"Computer";
	case SidTypeLabel:
		return L"Label";
	}

	return L"UnexpectedValue";
}


wstring TrusteeTypeToString(DWORD type)
{
	switch (type)
	{
	case TRUSTEE_IS_UNKNOWN:
		return L"Unknown";
		break;
	case TRUSTEE_IS_USER:
		return L"User";
		break;
	case TRUSTEE_IS_GROUP:
		return L"Group";
		break;
	case TRUSTEE_IS_DOMAIN:
		return L"Domain";
		break;
	case TRUSTEE_IS_ALIAS:
		return L"Alias";
		break;
	case TRUSTEE_IS_WELL_KNOWN_GROUP:
		return L"WellKnownGroup";
		break;
	case TRUSTEE_IS_DELETED:
		return L"Deleted";
		break;
	case TRUSTEE_IS_INVALID:
		return L"Invalid";
		break;
	case TRUSTEE_IS_COMPUTER:
		return L"Computer";
		break;
	}

	return L"UnexpectedValue";
}


wstring SidToString(PSID pSID)
{
	if (!IsValidSid(pSID))
	{
		return L"Invalid SID";
	}

	LPWSTR pstr = nullptr;
	if (!ConvertSidToStringSidW(pSID, &pstr))
	{
		//return L"Error " + to_wstring(GetLastError()) + L" converting SID to string";
		return ErrorString(GetLastError());
	}

	wstring ret(pstr);
	LocalFree(pstr);
	return ret;
}


pair<wstring, wstring> SidNameToString(const wstring& server, PSID pSID)
{
	if (nullptr == pSID)
	{
		return make_pair(wstring(), wstring());
	}

	// Get the length
	DWORD nameLen = 0;
	DWORD domainLen = 0;
	SID_NAME_USE sidNameUse;
	LookupAccountSidW(
		server.c_str(),
		pSID,
		nullptr,
		&nameLen,
		nullptr,
		&domainLen,
		&sidNameUse
	);

	vector<wchar_t> name(nameLen);
	vector<wchar_t> domain(domainLen);

	if (!LookupAccountSidW(
		server.c_str(),
		pSID,
		name.data(),
		&nameLen,
		domain.data(),
		&domainLen,
		&sidNameUse
	))
	{
		DWORD err = GetLastError();
		wcerr << L"Error " << err << L" looking up SID: " << ErrorString(err) << endl;
		return make_pair<wstring, wstring>(L"Error", L"");
	}

	return make_pair<wstring, wstring>(
		wstring(domain.data()) + L"\\" + wstring(name.data()),
		SidUseToString(sidNameUse));
}


vector<tuple<wstring, wstring, DWORD>> SecurityDescriptorDaclAcesToString(const wstring& server, PSECURITY_DESCRIPTOR pSD)
{
	if (nullptr == pSD)
	{
		return {};
	}

	vector<tuple<wstring, wstring, DWORD>> ret;

	BOOL aclPresent = FALSE;
	BOOL aclDefaulted = FALSE;
	PACL pAcl = nullptr;

	if (!GetSecurityDescriptorDacl(
		pSD,
		&aclPresent,
		&pAcl,
		&aclDefaulted
	))
	{
		DWORD err = GetLastError();
		wcerr << L"Error " << err << L" getting SACL: " << ErrorString(err) << endl;
		return {};
	}

	ULONG count = 0;
	PEXPLICIT_ACCESSW pExplicitAccess = nullptr;

	DWORD err = GetExplicitEntriesFromAclW(pAcl, &count, &pExplicitAccess);

	if (ERROR_SUCCESS != err)
	{
		wcerr << L"Error " << err << L" getting ACL entries: " << ErrorString(err) << endl;
		return {};
	}

	for (ULONG n = 0; n < count; n++)
	{
		wstring name;
		wstring type;
		DWORD permission = 0;

		switch (pExplicitAccess[n].Trustee.TrusteeForm)
		{
		case TRUSTEE_IS_SID:
			tie(name, type) = SidNameToString(server, pExplicitAccess[n].Trustee.ptstrName);
			break;
		case TRUSTEE_IS_NAME:
			name = pExplicitAccess[n].Trustee.ptstrName;
			type = TrusteeTypeToString(pExplicitAccess[n].Trustee.TrusteeType);
			break;
		case TRUSTEE_BAD_FORM:
			name = L"(Bad form)";
			break;
		case TRUSTEE_IS_OBJECTS_AND_SID:
		{
			POBJECTS_AND_SID pObjsAndSid = reinterpret_cast<POBJECTS_AND_SID>(pExplicitAccess[n].Trustee.ptstrName);
			tie(name, type) = SidNameToString(server, pObjsAndSid->pSid);
		}
			break;
		case TRUSTEE_IS_OBJECTS_AND_NAME:
		{
			POBJECTS_AND_NAME_W pObjsAndName = reinterpret_cast<POBJECTS_AND_NAME_W>(pExplicitAccess[n].Trustee.ptstrName);
			name = pObjsAndName->ptstrName;
			type = TrusteeTypeToString(pExplicitAccess[n].Trustee.TrusteeType);
		}
			break;
		}

		ret.push_back(make_tuple(name, type, pExplicitAccess[n].grfAccessPermissions));
	}

	LocalFree(pExplicitAccess);

	return ret;
}


PSECURITY_DESCRIPTOR MakeSecurityDescriptor(const wstring& server, const vector<tuple<wstring, DWORD>>& users)
{
	vector<EXPLICIT_ACCESSW> eaVec(users.size());
	vector<vector<BYTE>> sidBuffers(users.size());

	for (size_t n = 0; n < users.size(); n++)
	{
		DWORD sidSize = 0;
		DWORD domainSize = 0;
		SID_NAME_USE sidNameUse;

		wstring userName = get<0>(users[n]).c_str();

		// Get sizes
		if (!LookupAccountNameW(
			server.c_str(),
			userName.c_str(),
			nullptr,
			&sidSize,
			nullptr,
			&domainSize,
			&sidNameUse
		))
		{
			DWORD err = GetLastError();
			if (ERROR_INSUFFICIENT_BUFFER != err)
			{
				wcerr << L"Error " << err << L" looking up account sizes '" << userName << L"': " << ErrorString(err) << endl;
				return nullptr;
			}
		}

		sidBuffers[n].resize(sidSize);
		vector<wchar_t> domain(domainSize);

		// Get SID
		if (!LookupAccountNameW(
			server.c_str(),
			userName.c_str(),
			reinterpret_cast<PSID>(sidBuffers[n].data()),
			&sidSize,
			domain.data(),
			&domainSize,
			&sidNameUse
		))
		{
			DWORD err = GetLastError();
			wcerr << L"Error " << err << L" looking up account '" << userName << L"': " << ErrorString(err) << endl;
			return nullptr;
		}

		eaVec[n].grfAccessMode = SET_ACCESS;
		eaVec[n].grfAccessPermissions = get<1>(users[n]);
		eaVec[n].grfInheritance = NO_INHERITANCE;
		eaVec[n].Trustee.TrusteeForm = TRUSTEE_IS_SID;
		eaVec[n].Trustee.TrusteeType = SidUseToTrusteeType(sidNameUse);
		eaVec[n].Trustee.ptstrName = reinterpret_cast<LPWCH>(sidBuffers[n].data());
		eaVec[n].Trustee.pMultipleTrustee = nullptr;
		eaVec[n].Trustee.MultipleTrusteeOperation = NO_MULTIPLE_TRUSTEE;
	}

	vector<BYTE> pSD(SECURITY_DESCRIPTOR_MIN_LENGTH);

	if (!InitializeSecurityDescriptor(
		reinterpret_cast<PSECURITY_DESCRIPTOR>(pSD.data()),
		SECURITY_DESCRIPTOR_REVISION))
	{
		DWORD err = GetLastError();
		wcerr << L"Error " << err << L" initializing security descriptor: " << ErrorString(err) << endl;
		return nullptr;
	}

	PACL pACL = nullptr;

	DWORD result = SetEntriesInAclW(
		(ULONG)eaVec.size(),
		eaVec.data(),
		nullptr,
		&pACL
	);
	if (ERROR_SUCCESS != result)
	{
		DWORD err = GetLastError();
		wcerr << L"Error " << err << L" setting entries in Acl: " << ErrorString(err);
		return nullptr;
	}

	// Add the ACL to the security descriptor. 
	if (!SetSecurityDescriptorDacl(
		reinterpret_cast<PSECURITY_DESCRIPTOR>(pSD.data()),
		TRUE,     // bDaclPresent flag   
		pACL,
		FALSE))   // not a default DACL 
	{
		DWORD err = GetLastError();
		wcerr << L"Error " << err << L" setting DACL in security descriptor: " << ErrorString(err) << endl;
		LocalFree((HLOCAL)pACL);
		return nullptr;
	}

	DWORD sdSize = 0;
	MakeSelfRelativeSD(
		reinterpret_cast<PSECURITY_DESCRIPTOR>(pSD.data()),
		nullptr,
		&sdSize
	);

	PSECURITY_DESCRIPTOR pSelfRelativeSD = reinterpret_cast<PSECURITY_DESCRIPTOR>(LocalAlloc(LPTR, sdSize));
	if (nullptr == pSelfRelativeSD)
	{
		wcerr << L"Failed to allocate memory for self relative SD size " << sdSize << endl;
		LocalFree((HLOCAL)pACL);
		return nullptr;
	}

	if (!MakeSelfRelativeSD(
		reinterpret_cast<PSECURITY_DESCRIPTOR>(pSD.data()),
		pSelfRelativeSD,
		&sdSize
	))
	{
		DWORD err = GetLastError();
		wcerr << L"Error " << err << L" making self relative security descriptor: " << ErrorString(err) << endl;
		LocalFree((HLOCAL)pACL);
		return nullptr;
	}

	LocalFree((HLOCAL)pACL);

	return pSelfRelativeSD;
}


pair<wstring, wstring> SecurityDescriptorOwnerToString(const wstring& server, PSECURITY_DESCRIPTOR pSD)
{
	if (nullptr == pSD)
	{
		return make_pair(wstring(), wstring());
	}

	if (!IsValidSecurityDescriptor(pSD))
	{
		return make_pair(L"Invalid SD", wstring());
	}

	PSID pSID = nullptr;
	BOOL ownerDefaulted = FALSE;

	if (!GetSecurityDescriptorOwner(
		pSD,
		&pSID,
		&ownerDefaulted
	))
	{
		DWORD err = GetLastError();
		wcerr << L"Error " << err << L" getting security descriptor owner: " << ErrorString(err) << endl;
		return make_pair(wstring(), wstring());
	}

	if (nullptr == pSID)
	{
		return make_pair(L"None", wstring());
	}

	if (!IsValidSid(pSID))
	{
		return make_pair(L"???", wstring());
	}

	return SidNameToString(server, pSID);
}

