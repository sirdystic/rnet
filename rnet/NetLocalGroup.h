#pragma once

/* NetLocalGroup.h - Wrappers around NetLocalGroup*() APIs */

#include <string>
#include <vector>

using namespace std;


extern vector<int> NetLocalGroupEnumTryLevels;
extern vector<int> NetLocalGroupGetInfoTryLevels;
extern vector<int> NetLocalGroupGetInfoValidLevels;

int LocalGroupEnum(const wstring& server, int level);
int LocalGroupGetInfo(const wstring& server, const wstring& group, int level);
int LocalGroupAddMembers(const wstring& server, const wstring& group, vector<wstring>& members);
int LocalGroupDelMembers(const wstring& server, const wstring& group, vector<wstring>& members);
int LocalGroupAdd(const wstring& server, const wstring& group, const wstring& comment);
int LocalGroupSetInfo(const wstring& server, const wstring& group, const wstring& comment);
int LocalGroupDel(const wstring& server, const wstring& group);

