/* Utils.cpp - Utility functions */

#include "Utils.h"
#include "Error.h"

#include <Windows.h>
#include <LM.h>

#include <iostream>
#include <algorithm>
#include <numeric>
#include <cwctype>
#include <sstream>
#include <iomanip>
#include <time.h>


#define SECPERMIN (60)
#define SECPERHOUR (60 * SECPERMIN)
#define SECPERDAY (24 * SECPERHOUR)
#define SECPERYEAR (364 * SECPERDAY)


// Returns the length of the longest string in a vector of strings
size_t LongestString(const vector<wstring>& vec)
{
	size_t longest = 0;
	for (const auto& str : vec)
	{
		if (str.length() > longest)
			longest = str.length();
	}
	return longest;
}


// Outputs a single row of values to the console
void OutputRow(const vector<size_t>& lengths, const vector<wstring>& strings)
{
	if (lengths.size() != strings.size())
	{
		wcerr << L"INTERNAL ERROR OutputRow() length mismatch" << endl;
		return;
	}

	for (size_t n = 0; n < lengths.size(); n++)
	{
		wcout << strings[n] << wstring(lengths[n] - strings[n].length(), L' ');
	}
	wcout << endl;
}


// Outputs a formatted table of values optionally with headers to the console
void OutputTable(const vector<wstring>& headers, const vector<vector<wstring>>& values)
{
	if (values.empty())
		return;

	vector<size_t> lengths;

	if (headers.empty())
	{
		transform(values.begin(), values.end(), back_inserter(lengths), [](const vector<wstring>& v)
			{ return LongestString(v) + 2; });
	}
	else
	{
		if (headers.size() != values.size())
		{
			wcerr << L"INTERNAL ERROR: OutputTable() length mismatch" << endl;
			return;
		}

		for (size_t n = 0; n < headers.size(); n++)
		{
			lengths.push_back(max(headers[n].length(), LongestString(values[n])) + 2);
		}

		OutputRow(lengths, headers);
		wcout << wstring(accumulate(lengths.begin(), lengths.end(), (size_t)0), L'-') << endl;
	}

	for (size_t n = 0; n < values[0].size(); n++)
	{
		vector<wstring> vec;
		transform(values.begin(), values.end(), back_inserter(vec), [n](const vector<wstring>& v) { return v[n]; } );
		OutputRow(lengths, vec);
	}
}


// Returns uppercase version of str
wstring StringUpper(const wstring& str)
{
	wstring ret(str);
	if (ret.end() != transform(ret.begin(), ret.end(), ret.begin(), std::towupper))
	{
		// Avoid warning
	}
	return ret;
}


// Returns lowercase version of str
wstring StringLower(const wstring& str)
{
	wstring ret(str);
	if (ret.end() != transform(ret.begin(), ret.end(), ret.begin(), std::towlower))
	{
		// Avoid warning
	}
	return ret;
}


// Returns a string representation of an epoch DWORD time value in local timezone
wstring TimeToString(DWORD time)
{
	time_t t;
	t = time;
	struct tm timeinfo;

	localtime_s(&timeinfo, &t);

	wstringstream sstr;
	sstr << std::setfill(L'0') << std::setw(2) << timeinfo.tm_hour 
		<< L":" << std::setfill(L'0') << std::setw(2) << timeinfo.tm_min 
		<< L":" << std::setfill(L'0') << std::setw(2) << timeinfo.tm_sec 
		<< L" " << std::setfill(L'0') << std::setw(2) << timeinfo.tm_mon + 1
		<< L"-" << std::setfill(L'0') << std::setw(2) << timeinfo.tm_mday 
		<< L"-" << std::setfill(L'0') << std::setw(4) << timeinfo.tm_year + 1900;

	return sstr.str();
}


// Returns the month value (0 - 11) of a month name or prefix
int MonthStringToInt(const wstring& str)
{
	wstring month(StringUpper(str));

	if (L"JAN" == month || 
		L"JANUARY" == month)
		return 0;
	else if (L"FEB" == month || 
		L"FEBRUARY" == month)
		return 1;
	else if (L"MAR" == month ||
		L"MARCH" == month)
		return 2;
	else if (L"APR" == month ||
		L"APRIL" == month)
		return 3;
	else if (L"MAY" == month)
		return 4;
	else if (L"JUN" == month ||
		L"JUNE" == month)
		return 5;
	else if (L"JUL" == month ||
		L"JULY" == month)
		return 6;
	else if (L"AUG" == month || 
		L"AUGUST" == month)
		return 7;
	else if (L"SEP" == month ||
		L"SEPTEMPER" == month)
		return 8;
	else if (L"OCT" == month ||
		L"OCTOBER" == month)
		return 9;
	else if (L"NOV" == month ||
		L"NOVEMBER" == month)
		return 10;
	else if (L"DEC" == month ||
		L"DECEMBER" == month)
		return 11;

	return -1;
}


// Returns a DWORD epoch value (seconds since 01/01/1970) from a time string in one of the folloing formats:
// Jan/01/2010 
// January/01/2010
// 01/01/2010
// 01/01/10
// Jan/01/10
// January/01/2010
DWORD StringToTime(const wstring& str)
{
	if (count(str.begin(), str.end(), L'/') != 2)
		return 0;
	size_t slash1 = str.find(L'/');
	size_t slash2 = str.rfind(L'/');
	wstring monthStr = str.substr(0, slash1);
	wstring dayStr = str.substr(slash1 + 1, slash2 - slash1 - 1);
	wstring yearStr = str.substr(slash2 + 1);

	if (2 != dayStr.length() ||
		!(2 == yearStr.length() || 4 == yearStr.length()))
	{
		return 0;
	}

	int month = 0;
	if (2 == monthStr.length())
	{
		month = stoi(monthStr) - 1;
	}
	else
	{
		month = MonthStringToInt(monthStr);
	}
	int day = stoi(dayStr);
	int year = 0;
	if (2 == yearStr.length())
	{
		year = stoi(yearStr) + 100;
	}
	else if (4 == yearStr.length())
	{
		year = stoi(yearStr) - 1900;
	}

	if (month < 0 || month > 11 ||
		day < 1 || day > 31 ||
		year < 110)
	{
		return 0;
	}

	struct tm tmstruct;
	memset(&tmstruct, 0, sizeof(tmstruct));
	tmstruct.tm_mon = month;
	tmstruct.tm_mday = day;
	tmstruct.tm_year = year;
	return (DWORD)mktime(&tmstruct);
}


// Returns a string representing the time length of a total of seconds
wstring SecondsToString(DWORD seconds)
{
	wstring ret;

	if (seconds >= SECPERYEAR)
	{
		DWORD val = seconds / SECPERYEAR;
		ret += to_wstring(val) + L" year";
		if (val > 1)
			ret += L"s";
		seconds %= SECPERYEAR;
	}

	if (seconds >= SECPERDAY)
	{
		if (!ret.empty())
			ret += L" ";

		DWORD val = seconds / SECPERDAY;
		ret += to_wstring(val) + L" day";
		if (val > 1)
			ret += L"s";
		seconds %= SECPERDAY;
	}

	if (seconds >= SECPERHOUR)
	{
		if (!ret.empty())
			ret += L" ";

		DWORD val = seconds / SECPERHOUR;
		ret += to_wstring(val) + L" hr";
		if (val > 1)
			ret += L"s";
		seconds %= SECPERHOUR;
	}

	if (seconds >= SECPERMIN)
	{
		if (!ret.empty())
			ret += L" ";

		DWORD val = seconds / SECPERMIN;
		ret += to_wstring(val) + L" min";
		if (val > 1)
			ret += L"s";
		seconds %= SECPERMIN;
	}

	if (!ret.empty())
		ret += L" ";

	ret += to_wstring(seconds) + L" sec";
	if (seconds > 1)
		ret += L"s";

	return ret;
}


// Returns string version of DWORD version values
wstring ServerVersionToString(DWORD verMajor, DWORD verMinor)
{
	return to_wstring(MAJOR_VERSION_MASK & verMajor) + L"." + to_wstring(verMinor);
}


// Returns a descriptive text of a code page
wstring GetCodePageName(DWORD codepage)
{
	CPINFOEXW cpInfoEx;

	if (!GetCPInfoExW(codepage, 0, &cpInfoEx))
	{
		return ErrorString(GetLastError());
	}

	return wstring(cpInfoEx.CodePageName);
}


// Returns string form of a wstring
string WstringToString(const wstring& str)
{
	char defaultChar = '_';
	BOOL defaultUsed = FALSE;
	int size = WideCharToMultiByte(CP_ACP, 0, str.c_str(), -1, nullptr, 0, &defaultChar, &defaultUsed);
	vector<char> buff(size);
	WideCharToMultiByte(CP_ACP, 0, str.c_str(), -1, buff.data(), size, &defaultChar, &defaultUsed);
	return string(buff.data());
}


// Returns strings suitable for use in XML/JSON element tags
wstring ElementSafeString(const wstring& str)
{
	wstring ret(str);
	replace(ret.begin(), ret.end(), L' ', L'_');
	return StringLower(ret);
}


// Inputs a hidden password from the console
wstring GetPassword(const wstring& prompt, bool show_asterisk)
{
	wstring password;
	const wchar_t BACKSPACE = 8;
	const wchar_t RETURN = 13;
	wchar_t ch = 0;
	DWORD con_mode;
	DWORD dwRead;
	HANDLE hIn = GetStdHandle(STD_INPUT_HANDLE);
	GetConsoleMode(hIn, &con_mode);
	SetConsoleMode(hIn, con_mode & ~(ENABLE_ECHO_INPUT | ENABLE_LINE_INPUT));

	wcout << prompt;

	while (ReadConsoleW(hIn, &ch, 1, &dwRead, nullptr) && RETURN != ch)
	{
		if (BACKSPACE == ch)
		{
			if (password.length() != 0)
			{
				if (show_asterisk)
					cout << "\b \b";
				password.resize(password.length() - 1);
			}
		}
		else
		{
			password += ch;
			if (show_asterisk)
				wcout << L'*';
		}
	}

	wcout << endl;
	return password;
}


// Returns version string of the current executable
wstring GetVersionString()
{
	// Special runtime function returns pointer to global variable
	// holding executable path
	wchar_t* fileName = nullptr;
	int err = _get_wpgmptr(&fileName);
	if (0 != err)
	{
		return L"Error " + to_wstring(err) + L" getting current executable path";
	}

	DWORD handle;
	DWORD len = GetFileVersionInfoSizeW(fileName, &handle);
	if (0 == len)
		return wstring();

	vector<char> data(len);
	if (!GetFileVersionInfoW(
		fileName,
		0,
		len,
		data.data()))
	{
		DWORD err = GetLastError();
		return L"Error " + to_wstring(err) + L" retrieving version information: " + ErrorString(err);
	}

	VS_FIXEDFILEINFO *pFileInfo = nullptr;
	UINT verLen = 0;
	if (!VerQueryValueW(
		data.data(),
		L"\\",
		reinterpret_cast<LPVOID*>(&pFileInfo),
		&verLen))
	{
		DWORD err = GetLastError();
		return L"Error " + to_wstring(err) + L" retrieving version pointer: " + ErrorString(err);
	}

	return to_wstring(HIWORD(pFileInfo->dwFileVersionMS)) + L"." + 
		to_wstring(LOWORD(pFileInfo->dwFileVersionMS)) + L"." + 
		to_wstring(HIWORD(pFileInfo->dwFileVersionLS)) + L"." + 
		to_wstring(LOWORD(pFileInfo->dwFileVersionLS));
}

