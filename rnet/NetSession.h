#pragma once

/* NetSession.h - Wrappers around NetSession*() APIs */

#include <string>
#include <vector>

using namespace std;


extern vector<int> NetSessionEnumTryLevels;
extern vector<int> NetSessionGetInfoTryLevels;

int SessionEnum(const wstring& server, const wstring& computer, const wstring& user, int level);
int SessionDel(const wstring& server, const wstring& computer, const wstring& user);
int SessionGetInfo(const wstring& server, const wstring& computer, const wstring& user, int level);


