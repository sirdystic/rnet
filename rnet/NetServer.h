#pragma once

/* NetServer.h - Wrappers around NetServer*() */

#include <string>
#include <vector>

using namespace std;


extern vector<int> NetServerEnumTryLevels;
extern vector<int> NetServerGetInfoTryLevels;

int ServerEnum(const wstring& server, const wstring& domain, int level);
int ServerTransportEnum(const wstring& server);
int ServerSetInfo(const wstring& server, const vector<wstring>& options);
int ServerGetInfo(const wstring& server, int level);

