#pragma once

/* NetWksta.h - Wrappers around NetWksta* APIs */

#include <string>
#include <vector>

using namespace std;

extern vector<int> NetWkstaGetInfoTryLevels;

int WkstaGetInfo(const wstring& server, int level);


