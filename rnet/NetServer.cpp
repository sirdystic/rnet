/* NetServer.cpp - Wrappers around NetServer*() APIs */

#include "NetServer.h"
#include "Error.h"
#include "Utils.h"
#include "Types.h"
#include "Macros.h"
#include "Xml.h"
#include "Json.h"
#include "Globals.h"

#include <Windows.h>
#include <LM.h>

#include <iostream>


void GetServerInfoValues100(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetServerInfoValues101(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetServerInfoValues102(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetServerInfoValues103(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);

vector<int> NetServerEnumTryLevels = { 100, 101 };
vector<int> NetServerGetInfoTryLevels = { 100, 101, 102, 103 };
levelmap NetServerGetValuesMap = {
	{ 100, GetServerInfoValues100 },
	{ 101, GetServerInfoValues101 },
	{ 102, GetServerInfoValues102 },
	{ 103, GetServerInfoValues103 },
};


wstring ServerParamErrorToString(DWORD paramErr)
{
	switch (paramErr)
	{
	case SV_PLATFORM_ID_PARMNUM:
		return L"Platform ID";
	case SV_NAME_PARMNUM:
		return L"Name";
	case SV_VERSION_MAJOR_PARMNUM:
		return L"Version major";
	case SV_VERSION_MINOR_PARMNUM:
		return L"Version minor";
	case SV_TYPE_PARMNUM:
		return L"Type";
	case SV_COMMENT_PARMNUM:
		return L"Comment";
	case SV_USERS_PARMNUM:
		return L"Users";
	case SV_DISC_PARMNUM:
		return L"Disc";
	case SV_HIDDEN_PARMNUM:
		return L"Hidden";
	case SV_ANNOUNCE_PARMNUM:
		return L"Announce";
	case SV_ANNDELTA_PARMNUM:
		return L"Announce delta";
	case SV_USERPATH_PARMNUM:
		return L"User path";
	case SV_ULIST_MTIME_PARMNUM:
		return L"Ulist mtime";
	case SV_GLIST_MTIME_PARMNUM:
		return L"Glist mtime";
	case SV_ALIST_MTIME_PARMNUM:
		return L"Alist mtime";
	case SV_ALERTS_PARMNUM:
		return L"Alerts";
	case SV_SECURITY_PARMNUM:
		return L"Security";
	case SV_NUMADMIN_PARMNUM:
		return L"Num admin";
	case SV_LANMASK_PARMNUM:
		return L"LAN mask";
	case SV_GUESTACC_PARMNUM:
		return L"Guest account";
	case SV_CHDEVQ_PARMNUM:
		return L"Chdevq";
	case SV_CHDEVJOBS_PARMNUM:
		return L"Chdevjobs";
	case SV_CONNECTIONS_PARMNUM:
		return L"Connections";
	case SV_SHARES_PARMNUM:
		return L"Shares";
	case SV_OPENFILES_PARMNUM:
		return L"Open files";
	case SV_SESSOPENS_PARMNUM:
		return L"Sessions open";
	case SV_SESSVCS_PARMNUM:
		return L"Sessvcs";
	case SV_SESSREQS_PARMNUM:
		return L"Session requests";
	case SV_OPENSEARCH_PARMNUM:
		return L"Open search";
	case SV_ACTIVELOCKS_PARMNUM:
		return L"Active locks";
	case SV_NUMREQBUF_PARMNUM:
		return L"Num request buffers";
	case SV_SIZREQBUF_PARMNUM:
		return L"Size request buffer";
	case SV_NUMBIGBUF_PARMNUM:
		return L"Num big buffers";
	case SV_NUMFILETASKS_PARMNUM:
		return L"Num file tasks";
	case SV_ALERTSCHED_PARMNUM:
		return L"Alert schedule";
	case SV_ERRORALERT_PARMNUM:
		return L"Error alert";
	case SV_LOGONALERT_PARMNUM:
		return L"Logon alert";
	case SV_ACCESSALERT_PARMNUM:
		return L"Access alert";
	case SV_DISKALERT_PARMNUM:
		return L"Disk alert";
	case SV_NETIOALERT_PARMNUM:
		return L"Net IO alert";
	case SV_MAXAUDITSZ_PARMNUM:
		return L"Max audits";
	case SV_SRVHEURISTICS_PARMNUM:
		return L"Server heuristics";
	case SV_TIMESOURCE_PARMNUM:
		return L"Time source";
	}

	return L"Unknown";
}


wstring ServerPlatformIdToString(DWORD id)
{
	switch (id)
	{
	case PLATFORM_ID_DOS:
		return L"DOS";
	case PLATFORM_ID_OS2:
		return L"OS/2";
	case PLATFORM_ID_NT:
		return L"WinNT";
	case PLATFORM_ID_OSF:
		return L"OSF";
	case PLATFORM_ID_VMS:
		return L"VMS";
	}

	return to_wstring(id);
}


wstring ServerTypeFlagsToString(DWORD type)
{
	wstring ret;
	if (type & SV_TYPE_WORKSTATION)
		ret += L"WKS ";
	if (type & SV_TYPE_SERVER)
		ret += L"SRV ";
	if (type & SV_TYPE_SQLSERVER)
		ret += L"SQL ";
	if (type & SV_TYPE_DOMAIN_CTRL)
		ret += L"DOMCT ";
	if (type & SV_TYPE_DOMAIN_BAKCTRL)
		ret += L"DOMBK ";
	if (type & SV_TYPE_TIME_SOURCE)
		ret += L"TIME ";
	if (type & SV_TYPE_AFP)
		ret += L"AFP ";
	if (type & SV_TYPE_NOVELL)
		ret += L"NOV ";
	if (type & SV_TYPE_DOMAIN_MEMBER)
		ret += L"DOMMB ";
	if (type & SV_TYPE_PRINTQ_SERVER)
		ret += L"PRINT ";
	if (type & SV_TYPE_DIALIN_SERVER)
		ret += L"DIAL ";
	if (type & SV_TYPE_XENIX_SERVER)
		ret += L"XENIX ";
	if (type & SV_TYPE_NT)
		ret += L"NT ";
	if (type & SV_TYPE_WFW)
		ret += L"WFW ";
	if (type & SV_TYPE_SERVER_MFPN)
		ret += L"MFPN ";
	if (type & SV_TYPE_SERVER_NT)
		ret += L"NTSRV ";
	if (type & SV_TYPE_POTENTIAL_BROWSER)
		ret += L"PTBRW ";
	if (type & SV_TYPE_BACKUP_BROWSER)
		ret += L"BKBRW ";
	if (type & SV_TYPE_MASTER_BROWSER)
		ret += L"MSBRW ";
	if (type & SV_TYPE_DOMAIN_MASTER)
		ret += L"DMBRW ";
	if (type & SV_TYPE_SERVER_OSF)
		ret += L"OSF ";
	if (type & SV_TYPE_SERVER_VMS)
		ret += L"VMS ";
	if (type & SV_TYPE_WINDOWS)
		ret += L"WIN ";
	if (type & SV_TYPE_DFS)
		ret += L"DFS ";
	if (type & SV_TYPE_CLUSTER_NT)
		ret += L"CLUS ";
	if (type & SV_TYPE_TERMINALSERVER)
		ret += L"TS ";
	if (type & SV_TYPE_CLUSTER_VS_NT)
		ret += L"CLUSVS ";
	if (type & SV_TYPE_DCE)
		ret += L"DCE ";
	if (type & SV_TYPE_ALTERNATE_XPORT)
		ret += L"XPORT ";
	if (type & SV_TYPE_LOCAL_LIST_ONLY)
		ret += L"LIST ";
	if (type & SV_TYPE_DOMAIN_ENUM)
		ret += L"DOMAIN ";

	return ret;
}


wstring ServerCapabilitiesToString(DWORD capabilities)
{
	wstring ret;
	if (capabilities & SRV_SUPPORT_HASH_GENERATION)
		ret += L"HASH ";
	if (capabilities & SRV_HASH_GENERATION_ACTIVE)
		ret += L"ACTIVE ";
	return ret;
}


void GetServerInfoValues100(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	LPSERVER_INFO_100 pServerInfo = reinterpret_cast<LPSERVER_INFO_100>(buffer);
	descriptions.push_back(L"Plat");
	text.push_back(ServerPlatformIdToString(pServerInfo->sv100_platform_id));
	descriptions.push_back(L"Name");
	text.push_back(pServerInfo->sv100_name);
}


void GetServerInfoValues101(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetServerInfoValues100(buffer, descriptions, text);

	LPSERVER_INFO_101 pServerInfo = reinterpret_cast<LPSERVER_INFO_101>(buffer);
	descriptions.push_back(L"Ver");
	text.push_back(ServerVersionToString(pServerInfo->sv101_version_major, pServerInfo->sv101_version_minor));
	descriptions.push_back(L"Type flags");
	text.push_back(ServerTypeFlagsToString(pServerInfo->sv101_type));
	descriptions.push_back(L"Comment");
	text.push_back(pServerInfo->sv101_comment);
}


void GetServerInfoValues102(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetServerInfoValues101(buffer, descriptions, text);

	LPSERVER_INFO_102 pServerInfo = reinterpret_cast<LPSERVER_INFO_102>(buffer);
	descriptions.push_back(L"Max users");
	text.push_back(to_wstring(pServerInfo->sv102_users));
	descriptions.push_back(L"Disconnect");
	text.push_back(SV_NODISC == pServerInfo->sv102_disc ? L"No" : to_wstring(pServerInfo->sv102_disc));
	descriptions.push_back(L"Hidden");
	text.push_back(pServerInfo->sv102_hidden == SV_VISIBLE ? L"No" : L"Yes");
	descriptions.push_back(L"Ann sec");
	text.push_back(to_wstring(pServerInfo->sv102_announce));
	descriptions.push_back(L"Ann delt");
	text.push_back(to_wstring(pServerInfo->sv102_anndelta));
	descriptions.push_back(L"Users");
	text.push_back(to_wstring(pServerInfo->sv102_licenses));
	descriptions.push_back(L"User path");
	text.push_back(pServerInfo->sv102_userpath);
}


void GetServerInfoValues103(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetServerInfoValues102(buffer, descriptions, text);

	LPSERVER_INFO_103 pServerInfo = reinterpret_cast<LPSERVER_INFO_103>(buffer);
	descriptions.push_back(L"Capabilities");
	text.push_back(ServerCapabilitiesToString(pServerInfo->sv103_capabilities));
}


int ServerEnum(const wstring& server, const wstring& domain, int level)
{
	wcout << L"NetServerEnum(" << server << L", " << domain << L") level " << level << endl;

	LPBYTE buffer = nullptr;
	DWORD entriesRead = 0;
	DWORD totalEntries = 0;
	DWORD resumeHandle = 0;

	NET_API_STATUS status = NetServerEnum(
		server.c_str(),
		level,
		&buffer,
		MAX_PREFERRED_LENGTH,
		&entriesRead,
		&totalEntries,
		SV_TYPE_ALL,
		domain.empty() ? nullptr : domain.c_str(),
		&resumeHandle
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" enumerating servers: " << ErrorString(status) << endl;
		return status;
	}

	vector<wstring> headers;
	vector<vector<wstring>> values;

	char* ptr = reinterpret_cast<char*>(buffer);
	for (DWORD d = 0; d < entriesRead; d++)
	{
		vector<wstring> descriptions;
		vector<wstring> text;

		LEVELMAPCHECK(NetServerGetValuesMap, level);
		NetServerGetValuesMap[level](ptr, descriptions, text);

		if (0 == d)
		{
			headers = descriptions;
			values.resize(headers.size());
		}
		for (size_t n = 0; n < text.size(); n++)
		{
			values[n].push_back(text[n]);
		}

		switch (level)
		{
			POINTERSTRUCTINCCASE(100, ptr, SERVER);
			POINTERSTRUCTINCCASE(101, ptr, SERVER);
		default:
			wcerr << L"INTERNAL ERROR: MISSING LEVEL IN POINTER INCREMENT SWITCH" << endl;
			exit(ERROR_INTERNAL_ERROR);
			break;
		}
	}

	OutputTable(headers, values);

	if (!g_xmlFilename.empty())
	{
		XmlOutputEnum(g_xmlFilename, level, server, L"server", headers, values);
	}

	if (!g_jsonFilename.empty())
	{
		JsonOutputEnum(g_jsonFilename, level, server, L"server", headers, values);
	}

	NetApiBufferFree(buffer);

	return status;
}


bool SetServerInfo102Options(const wstring& server, LPSERVER_INFO_102 pServerInfo, const vector<wstring>& options, vector<wstring>& stringTable)
{
	for (size_t n = 0; n < options.size(); n++)
	{
		if (0 == StringUpper(options[n]).find(L"/SRVCOMMENT:"))
		{
			stringTable.push_back(options[n].substr(12));
			pServerInfo->sv102_comment = (LPWSTR)stringTable.rbegin()->c_str();
		}
		else if (0 == StringUpper(options[n]).find(L"/AUTODISCONNECT:"))
		{
			wstring val = options[n].substr(16);
			DWORD time = 0;
			if (L"-1" == val)
				time = SV_NODISC;
			else
				time = stoul(val);
			pServerInfo->sv102_disc = time;
		}
		else if (L"/HIDDEN:YES" == options[n])
		{
			pServerInfo->sv102_hidden = SV_HIDDEN;
		}
		else if (L"/HIDDEN:NO" == options[n])
		{
			pServerInfo->sv102_hidden = SV_VISIBLE;
		}
		else
		{
			wcerr << L"Bad option: " << options[n] << endl;
			return false;
		}
	}

	return true;
}


// TODO enumerate at all levels?
int ServerTransportEnum(const wstring& server)
{
	wcout << L"NetServerTransportEnum(" << server << L")" << endl;

	LPBYTE buffer = nullptr;
	DWORD entriesRead = 0;
	DWORD totalEntries = 0;
	DWORD resumeHandle = 0;

	NET_API_STATUS status = NetServerTransportEnum(
		(LPWSTR)server.c_str(),
		1,
		&buffer,
		MAX_PREFERRED_LENGTH,
		&entriesRead,
		&totalEntries,
		&resumeHandle
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" enumerating server transports: " << ErrorString(status) << endl;
		return status;
	}

	vector<wstring> numClients;
	vector<wstring> transportNames;
	vector<wstring> addresses;
	vector<wstring> domains;

	LPSERVER_TRANSPORT_INFO_1 pServerTransportInfo = reinterpret_cast<LPSERVER_TRANSPORT_INFO_1>(buffer);
	for (DWORD d = 0; d < entriesRead; d++)
	{
		numClients.push_back(to_wstring(pServerTransportInfo[d].svti1_numberofvcs));
		transportNames.push_back(pServerTransportInfo[d].svti1_transportname);
		addresses.push_back(pServerTransportInfo[d].svti1_networkaddress);
		domains.push_back(pServerTransportInfo[d].svti1_domain);
	}

	vector<wstring> headers;
	headers.push_back(L"Clients");
	headers.push_back(L"Transport name");
	headers.push_back(L"Address");
	headers.push_back(L"Domain");

	vector<vector<wstring>> values;
	values.push_back(numClients);
	values.push_back(transportNames);
	values.push_back(addresses);
	values.push_back(domains);

	OutputTable(headers, values);

	NetApiBufferFree(buffer);

	return status;
}


int ServerSetInfo(const wstring& server, const vector<wstring>& options)
{
	wcout << L"NetServerGetInfo(" << server << L") level 102" << endl;

	LPBYTE buffer = nullptr;
	NET_API_STATUS status = NetServerGetInfo(
		(LPWSTR)server.c_str(),
		102,
		&buffer
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" getting server info: " << ErrorString(status) << endl;
		return status;
	}

	vector<wstring> stringTable;
	stringTable.reserve(16);

	if (!SetServerInfo102Options(server, reinterpret_cast<LPSERVER_INFO_102>(buffer), options, stringTable))
	{
		return ERROR_BAD_ARGUMENTS;
	}

	wcout << L"NetServerSetInfo(" << server << L") level 102" << endl;

	DWORD paramErr = 0;
	status = NetServerSetInfo(
		(LPWSTR)server.c_str(),
		102,
		buffer,
		&paramErr
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" setting server info: " << ErrorString(status) << endl;
		if (ERROR_INVALID_PARAMETER == status)
		{
			wcerr << L"Parameter error " << paramErr << L": " << ServerParamErrorToString(paramErr) << endl;
		}
	}
	else
	{
		wcout << L"Server info set" << endl;
	}

	NetApiBufferFree(buffer);

	return status;
}


int ServerGetInfo(const wstring& server, int level)
{
	wcout << L"NetServerGetInfo(" << server << L") level " << level << endl;

	LPBYTE buffer = nullptr;

	NET_API_STATUS status = NetServerGetInfo(
		(LPWSTR)server.c_str(),
		level,
		&buffer
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" getting server info: " << ErrorString(status) << endl;
		return status;
	}

	vector<wstring> descriptions;
	vector<wstring> text;

	LEVELMAPCHECK(NetServerGetValuesMap, level);
	NetServerGetValuesMap[level](buffer, descriptions, text);

	vector<vector<wstring>> values;
	values.push_back(descriptions);
	values.push_back(text);

	OutputTable({}, values);

	wcout << endl;

	if (!g_xmlFilename.empty())
	{
		XmlOutputInfo(
			g_xmlFilename,
			level,
			server,
			L"server",
			{ },
			values
		);
	}

	if (!g_jsonFilename.empty())
	{
		JsonOutputInfo(
			g_jsonFilename,
			level,
			server,
			L"server",
			{ },
			values
		);
	}

	NetApiBufferFree(buffer);

	return ServerTransportEnum(server);
}



