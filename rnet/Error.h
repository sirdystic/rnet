#pragma once

/* Error.h - Error related functions */

#include <Windows.h>

#include <string>

using namespace std;

wstring ErrorString(DWORD error);

