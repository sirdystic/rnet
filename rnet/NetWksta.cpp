/* NetWksta.cpp - Wrappers around NetWksta*() APIs */

#include "NetWksta.h"
#include "Error.h"
#include "Utils.h"
#include "Types.h"
#include "Macros.h"
#include "Xml.h"
#include "Json.h"
#include "Globals.h"

#include <Windows.h>
#include <LM.h>

#include <iostream>


void GetWkstaInfoValues100(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetWkstaInfoValues101(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetWkstaInfoValues102(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);

vector<int> NetWkstaGetInfoTryLevels = { 100, 101, 102 };
levelmap NetWkstaGetValuesMap = {
	{ 100, GetWkstaInfoValues100 },
	{ 101, GetWkstaInfoValues101 },
	{ 102, GetWkstaInfoValues102 },
};


void GetWkstaInfoValues100(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	LPWKSTA_INFO_100 pWkstaInfo = reinterpret_cast<LPWKSTA_INFO_100>(buffer);
	descriptions.push_back(L"Name");
	text.push_back(pWkstaInfo->wki100_computername);
	descriptions.push_back(L"LAN group");
	text.push_back(pWkstaInfo->wki100_langroup);
	descriptions.push_back(L"Ver");
	text.push_back(ServerVersionToString(pWkstaInfo->wki100_ver_major, pWkstaInfo->wki100_ver_minor));
}


void GetWkstaInfoValues101(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetWkstaInfoValues100(buffer, descriptions, text);

	LPWKSTA_INFO_101 pWkstaInfo = reinterpret_cast<LPWKSTA_INFO_101>(buffer);
	descriptions.push_back(L"LAN root");
	text.push_back(pWkstaInfo->wki101_lanroot);
}


void GetWkstaInfoValues102(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetWkstaInfoValues101(buffer, descriptions, text);

	LPWKSTA_INFO_102 pWkstaInfo = reinterpret_cast<LPWKSTA_INFO_102>(buffer);
	descriptions.push_back(L"Users");
	text.push_back(to_wstring(pWkstaInfo->wki102_logged_on_users));
}


int WkstaTransportEnum(const wstring& server)
{
	wcout << L"NetWkstaTransportEnum(" << server << L")" << endl;

	LPBYTE buffer = nullptr;
	DWORD entriesRead = 0;
	DWORD totalEntries = 0;
	DWORD resumeHandle = 0;

	NET_API_STATUS status = NetWkstaTransportEnum(
		(LPWSTR)server.c_str(),
		0,
		&buffer,
		MAX_PREFERRED_LENGTH,
		&entriesRead,
		&totalEntries,
		&resumeHandle
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" enumerating workstation transports: " << ErrorString(status) << endl;
		return status;
	}

	LPWKSTA_TRANSPORT_INFO_0 pWkstaTransportInfo = reinterpret_cast<LPWKSTA_TRANSPORT_INFO_0>(buffer);

	vector<wstring> qoss;
	vector<wstring> numClients;
	vector<wstring> names;
	vector<wstring> addresses;
	vector<wstring> wanishs;

	for (DWORD d = 0; d < entriesRead; d++)
	{
		qoss.push_back(to_wstring(pWkstaTransportInfo[d].wkti0_quality_of_service));
		numClients.push_back(to_wstring(pWkstaTransportInfo[d].wkti0_number_of_vcs));
		names.push_back(pWkstaTransportInfo[d].wkti0_transport_name);
		addresses.push_back(pWkstaTransportInfo[d].wkti0_transport_address);
		wanishs.push_back(0 == pWkstaTransportInfo[d].wkti0_wan_ish ? L"No" : L"Yes");
	}

	vector<wstring> headers;
	headers.push_back(L"QOS");
	headers.push_back(L"Clients");
	headers.push_back(L"Name");
	headers.push_back(L"Addresses");
	headers.push_back(L"WANish");

	vector<vector<wstring>> values;
	values.push_back(qoss);
	values.push_back(numClients);
	values.push_back(names);
	values.push_back(addresses);
	values.push_back(wanishs);

	OutputTable(headers, values);

	NetApiBufferFree(buffer);

	return status;
}


int WkstaGetInfo(const wstring& server, int level)
{
	wcout << L"NetWkstaGetInfo(" << server << L") level " << level << endl;

	LPBYTE buffer = nullptr;

	NET_API_STATUS status = NetWkstaGetInfo(
		(LPWSTR)server.c_str(),
		level,
		&buffer
		);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" getting workstation info: " << ErrorString(status) << endl;
		return status;
	}

	vector<wstring> descriptions;
	vector<wstring> text;

	LEVELMAPCHECK(NetWkstaGetValuesMap, level);
	NetWkstaGetValuesMap[level](buffer, descriptions, text);

	vector<vector<wstring>> values;
	values.push_back(descriptions);
	values.push_back(text);

	OutputTable({}, values);

	wcout << endl;

	if (!g_xmlFilename.empty())
	{
		XmlOutputInfo(
			g_xmlFilename,
			level,
			server,
			L"workstation",
			{ },
			values
		);
	}

	if (!g_jsonFilename.empty())
	{
		JsonOutputInfo(
			g_jsonFilename,
			level,
			server,
			L"workstation",
			{ },
			values
		);
	}

	NetApiBufferFree(buffer);

	return WkstaTransportEnum(server);
}

