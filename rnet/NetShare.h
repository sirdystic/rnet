#pragma once

/* NetShare.h - Wrappers around NetShare*() */

#include <string>
#include <vector>

using namespace std;


extern vector<int> NetShareEnumTryLevels;
extern vector<int> NetShareGetInfoTryLevels;

int ShareEnum(const wstring& server, int level);
int ShareGetInfo(const wstring& server, const wstring& share, int level);
int ShareAdd(const wstring& server, const wstring& share, const wstring& path, vector<wstring>& options);
int ShareDel(const wstring& server, const wstring& share);
int ShareSetInfo(const wstring& server, const wstring& share, const vector<wstring>& options);


