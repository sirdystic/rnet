/* Json.cpp - JSON output */

#include "Json.h"
#include "Utils.h"

#include "json/json.h"

#include <iostream>
#include <fstream>


bool JsonOutputInfo(const wstring& filename, int level, const wstring& server, const wstring& object,
	const vector<pair<wstring, wstring>>& details, const vector<vector<wstring>>& values,
	const vector<pair<wstring, vector<wstring>>>& extraData)
{
	ofstream outfile(filename);
	if (!outfile.is_open())
	{
		wcerr << L"Unable to open JSON output file: " << filename << endl;
		return false;
	}

	Json::Value root;

	// Add the server name if present
	if (!server.empty())
		root["server"] = WstringToString(server);

	// Add the level if there was one
	if (-1 != level)
		root["level"] = level;

	// Add optional details
	for (const auto& attr : details)
	{
		root[WstringToString(get<0>(attr))] = WstringToString(get<1>(attr));
	}

	Json::Value element;

	// Add the values
	for (size_t n = 0; n < values[0].size(); n++)
	{
		Json::Value entry;
		entry[WstringToString(ElementSafeString(values[0][n]))] = WstringToString(values[1][n]);
		element.append(entry);
	}

	// Add optional extra data
	for (const auto& extra : extraData)
	{
		Json::Value list;
		for (size_t n = 0; n < get<1>(extra).size(); n++)
		{
			list[(int)n] = Json::Value(WstringToString(get<1>(extra)[n]));
		}
		Json::Value entry;
		entry[WstringToString(get<0>(extra) + L"_list")] = list;

		element.append(entry);
	}

	root[WstringToString(object)] = element;
	outfile << root;
	outfile.close();

	return true;
}


bool JsonOutputEnum(const wstring& filename, int level, const wstring& server, const wstring& object,
	const vector<wstring>& headers, const vector<vector<wstring>>& values)
{
	ofstream outfile(filename);
	if (!outfile.is_open())
	{
		wcerr << L"Unable to open JSON output file: " << filename << endl;
		return false;
	}

	Json::Value root;

	if (!server.empty())
		root["server"] = WstringToString(server);

	if (-1 != level)
		root["level"] = level;

	Json::Value element;

	for (size_t n = 0; n < values[0].size(); n++)
	{
		Json::Value entry;

		for (size_t x = 0; x < headers.size(); x++)
		{
			entry[WstringToString(ElementSafeString(headers[x]))] = WstringToString(values[x][n]);
		}

		// TODO: Change to list to prevent sorting on output?
		element[static_cast<int>(n)] = entry;
	}

	root[WstringToString(object + L"List")] = element;

	outfile << root;
	outfile.close();

	return true;
}

