#pragma once

/* NullString.h - wstring like object allowing nullptr to be returned if string has not been assigned */

#include <string>

using namespace std;

class nullstring
{
public:

	nullstring() noexcept
	{
	}

	nullstring(const nullstring& that)
	{
		m_isSet = that.m_isSet;
		m_str = that.m_isSet;
	}

	~nullstring()
	{
	}

	bool is_set() const
	{
		return m_isSet;
	}

	nullstring& operator=(const nullstring& that)
	{
		m_isSet = that.m_isSet;
		m_str = that.m_str;
		return *this;
	}

	const wchar_t* c_str() const
	{
		if (!m_isSet)
			return nullptr;

		return m_str.c_str();
	}

	const wstring& str() const
	{
		return m_str;
	}

	wstring string() const
	{
		if (!m_isSet)
			return L"nullptr";

		return m_str;
	}

	wstring& operator=(const wstring& str) 
	{
		m_isSet = true;
		m_str = str;
		return m_str;
	}

	bool operator==(const wstring& str) const
	{
		if (!m_isSet)
			return false;

		return m_str == str;
	}

private:
	wstring m_str;
	bool m_isSet = false;
	
};

