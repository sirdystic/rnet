#pragma once

/* Xml.h - XML output */

#include <string>
#include <vector>

using namespace std;


bool XmlOutputInfo(const wstring& filename, int level, const wstring& server, const wstring& object,
	const vector<pair<wstring, wstring>>& details, const vector<vector<wstring>>& values,
	const vector<pair<wstring, vector<wstring>>>& extraData = {});
bool XmlOutputEnum(const wstring& filename, int level, const wstring& server, const wstring& object,
	const vector<wstring>& headers, const vector<vector<wstring>>& values);

