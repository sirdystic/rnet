#pragma once

/* NetUse.h - Wrappers around NetUse*() APIs */

#include "NullString.h"

#include <string>
#include <vector>

using namespace std;

extern vector<int> NetUseEnumTryLevels;
extern vector<int> NetUseGetInfoTryLevels;

int UseEnum(const wstring& server, int level);
int UseAdd(const wstring& server, const wstring& local, const wstring& remote, const nullstring& user, const wstring& domain, const nullstring& password);
int UseDel(const wstring& server, const wstring use);
int UseGetInfo(const wstring& server, const wstring& use, int level);

