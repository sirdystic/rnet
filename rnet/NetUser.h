#pragma once

/* NetUser.h - NetUser*() wrappers */

#include <string>
#include <vector>

using namespace std;


extern vector<int> NetUserEnumTryLevels;
extern vector<int> NetUserGetInfoTryLevels;
extern vector<int> NetUserGetInfoValidLevels;

int UserEnum(const wstring& server, int level);
int UserGetInfo(const wstring& server, const wstring& user, int level);
int UserAdd(const wstring& server, const wstring& user, const wstring& password, const vector<wstring>& options);
int UserSetInfo(const wstring& server, const wstring& user, const wstring& password, bool setPassword, const vector<wstring>& options);
int UserDel(const wstring& server, const wstring& user);
int SetUserPassword(const wstring& server, const wstring& user, const wstring& password);

