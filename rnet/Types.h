#pragma once

/* Types.h - Internal types */

#include <string>
#include <map>
#include <functional>
#include <vector>

using namespace std;

typedef map<int, function<void(void*, vector<wstring>&, vector<wstring>&)>> levelmap;
typedef map<wstring, function<int(const wstring& server, const vector<wstring>& arguments)>> commandmap;


