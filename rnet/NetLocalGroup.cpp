/* NetLocalGroup.cpp - Wrappers around NetLocalGroup*() APIs */

#include "NetLocalGroup.h"
#include "Utils.h"
#include "Error.h"
#include "Types.h"
#include "Macros.h"
#include "Xml.h"
#include "Json.h"
#include "Globals.h"

#include <Windows.h>
#include <LM.h>

#include <iostream>


void GetLocalGroupInfoValues0(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetLocalGroupInfoValues1(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetLocalGroupInfoValues1002(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);

vector<int> NetLocalGroupEnumTryLevels = { 0, 1 };
vector<int> NetLocalGroupGetInfoTryLevels = { 0, 1 };
vector<int> NetLocalGroupGetInfoValidLevels = { 0, 1, 1002 };
levelmap NetLocalGroupGetValuesMap = {
	{ 0, GetLocalGroupInfoValues0 },
	{ 1, GetLocalGroupInfoValues1 },
	{ 1002, GetLocalGroupInfoValues1002 },
};


wstring LocalGroupParamErrorToString(DWORD paramErr)
{
	switch (paramErr)
	{
	case LOCALGROUP_NAME_PARMNUM:
		return L"Name";
	case LOCALGROUP_COMMENT_PARMNUM:
		return L"Comment";
	}

	return L"Unknown";
}


void GetLocalGroupInfoValues0(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	LPLOCALGROUP_INFO_0 pGroupInfo = reinterpret_cast<LPLOCALGROUP_INFO_0>(buffer);
	descriptions.push_back(L"Name");
	text.push_back(pGroupInfo->lgrpi0_name);
}


void GetLocalGroupInfoValues1(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetLocalGroupInfoValues0(buffer, descriptions, text);

	LPLOCALGROUP_INFO_1 pGroupInfo = reinterpret_cast<LPLOCALGROUP_INFO_1>(buffer);
	descriptions.push_back(L"Comment");
	text.push_back(pGroupInfo->lgrpi1_comment);
}


void GetLocalGroupInfoValues1002(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	LPLOCALGROUP_INFO_1002 pGroupInfo = reinterpret_cast<LPLOCALGROUP_INFO_1002>(buffer);
	descriptions.push_back(L"Comment");
	text.push_back(pGroupInfo->lgrpi1002_comment);
}


int LocalGroupEnum(const wstring& server, int level)
{
	wcout << L"NetLocalGroupEnum(" << server << L") level " << level << endl;

	LPBYTE buffer = nullptr;
	DWORD entriesRead = 0;
	DWORD totalEntries = 0;
	DWORD_PTR resumeHandle = 0;

	NET_API_STATUS status = NetLocalGroupEnum(
		server.c_str(),
		level,
		&buffer,
		MAX_PREFERRED_LENGTH,
		&entriesRead,
		&totalEntries,
		&resumeHandle
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" enumerating localgroups: " << ErrorString(status) << endl;
		return status;
	}

	vector<wstring> headers;
	vector<vector<wstring>> values;

	char* ptr = reinterpret_cast<char*>(buffer);
	for (DWORD d = 0; d < entriesRead; d++)
	{
		vector<wstring> descriptions;
		vector<wstring> text;

		LEVELMAPCHECK(NetLocalGroupGetValuesMap, level);
		NetLocalGroupGetValuesMap[level](ptr, descriptions, text);

		if (0 == d)
		{
			headers = descriptions;
			values.resize(headers.size());
		}
		for (size_t n = 0; n < text.size(); n++)
		{
			values[n].push_back(text[n]);
		}

		switch (level)
		{
			POINTERSTRUCTINCCASE(0, ptr, LOCALGROUP);
			POINTERSTRUCTINCCASE(1, ptr, LOCALGROUP);
			POINTERSTRUCTINCCASE(1002, ptr, LOCALGROUP);
		default:
			wcerr << L"INTERNAL ERROR: MISSING LEVEL IN POINTER INCREMENT SWITCH" << endl;
			exit(ERROR_INTERNAL_ERROR);
		}
	}

	NetApiBufferFree(buffer);

	OutputTable(headers, values);

	if (!g_xmlFilename.empty())
	{
		XmlOutputEnum(g_xmlFilename, level, server, L"localgroup", headers, values);
	}

	if (!g_jsonFilename.empty())
	{
		JsonOutputEnum(g_jsonFilename, level, server, L"localgroup", headers, values);
	}

	return status;
}


vector<wstring> LocalGroupGetMembers(const wstring& server, const wstring& group)
{
	wcout << L"NetLocalGroupGetMembers(" << server << L", " << group << L")" << endl;

	LPBYTE buffer = nullptr;
	DWORD entriesRead = 0;
	DWORD totalEntries = 0;
	DWORD_PTR resumeHandle = 0;

	NET_API_STATUS status = NetLocalGroupGetMembers(
		server.c_str(),
		group.c_str(),
		3,
		&buffer,
		MAX_PREFERRED_LENGTH,
		&entriesRead,
		&totalEntries,
		&resumeHandle
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" enumerating localgroup members: " << ErrorString(status) << endl;
		return {};
	}

	vector<wstring> ret;

	LPLOCALGROUP_MEMBERS_INFO_3 pLocalGroupMembers = reinterpret_cast<LPLOCALGROUP_MEMBERS_INFO_3>(buffer);
	for (DWORD d = 0; d < entriesRead; d++)
	{
		ret.push_back(pLocalGroupMembers[d].lgrmi3_domainandname);
	}

	NetApiBufferFree(buffer);

	return ret;
}


int LocalGroupAddMembers(const wstring& server, const wstring& group, vector<wstring>& members)
{
	wcout << L"NetLocalGroupAddMembers(" << server << L", " << group << L")" << endl;

	vector<LOCALGROUP_MEMBERS_INFO_3> localGroupMembersInfo(members.size());
	for (size_t n = 0; n < members.size(); n++)
	{
		localGroupMembersInfo[n].lgrmi3_domainandname = (LPWSTR)members[n].c_str();
	}

	NET_API_STATUS status = NetLocalGroupAddMembers(
		server.c_str(),
		group.c_str(),
		3,
		reinterpret_cast<LPBYTE>(localGroupMembersInfo.data()),
		(DWORD)localGroupMembersInfo.size()
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" adding members to localgroup: " << ErrorString(status) << endl;
	}
	else
	{
		wcout << localGroupMembersInfo.size() << L" members added to localgroup '" << group << L"'." << endl;
	}

	return status;
}


int LocalGroupDelMembers(const wstring& server, const wstring& group, vector<wstring>& members)
{
	wcout << L"NetLocalGroupDelMembers(" << server << L", " << group << L")" << endl;

	vector<LOCALGROUP_MEMBERS_INFO_3> localGroupMembersInfo(members.size());
	for (size_t n = 0; n < members.size(); n++)
	{
		localGroupMembersInfo[n].lgrmi3_domainandname = (LPWSTR)members[n].c_str();
	}

	NET_API_STATUS status = NetLocalGroupDelMembers(
		server.c_str(),
		group.c_str(),
		3,
		reinterpret_cast<LPBYTE>(localGroupMembersInfo.data()),
		(DWORD)localGroupMembersInfo.size()
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" deleting members from localgroup: " << ErrorString(status) << endl;
	}
	else
	{
		wcout << localGroupMembersInfo.size() << L" members deleted from localgroup '" << group << L"'." << endl;
	}

	return status;
}


int LocalGroupAdd(const wstring& server, const wstring& group, const wstring& comment)
{
	wcout << L"NetLocalGroupAdd(" << server << L")" << endl;

	LOCALGROUP_INFO_1 localGroupInfo;
	DWORD paramErr = 0;

	localGroupInfo.lgrpi1_name = (LPWSTR)group.c_str();
	localGroupInfo.lgrpi1_comment = (LPWSTR)comment.c_str();

	NET_API_STATUS status = NetLocalGroupAdd(
		server.c_str(),
		1,
		reinterpret_cast<LPBYTE>(&localGroupInfo),
		&paramErr
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" adding localgroup: " << ErrorString(status) << endl;
		if (ERROR_INVALID_PARAMETER == status)
		{
			wcerr << L"Parameter error " << paramErr << L": " << LocalGroupParamErrorToString(paramErr) << endl;
		}
	}
	else
	{
		wcout << L"Localgroup '" << group << L"' added." << endl;
	}

	return status;
}


int LocalGroupSetInfo(const wstring& server, const wstring& group, const wstring& comment)
{
	wcout << L"NetLocalGroupSetInfo(" << server << L", " << group << L")" << endl;

	LOCALGROUP_INFO_1002 localGroupInfo;
	DWORD paramErr = 0;

	localGroupInfo.lgrpi1002_comment = (LPWSTR)comment.c_str();

	NET_API_STATUS status = NetLocalGroupSetInfo(
		server.c_str(),
		group.c_str(),
		1002,
		reinterpret_cast<LPBYTE>(&localGroupInfo),
		&paramErr
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" setting localgroup comment: " << ErrorString(status) << endl;
		if (ERROR_INVALID_PARAMETER == status)
		{
			wcerr << L"Parameter error " << paramErr << L": " << LocalGroupParamErrorToString(paramErr) << endl;
		}
	}
	else
	{
		wcout << L"Localgroup '" << group << L"' comment set." << endl;
	}

	return status;
}


int LocalGroupDel(const wstring& server, const wstring& group)
{
	wcout << L"NetLocalGroupDel(" << server << L", " << group << L")" << endl;

	NET_API_STATUS status = NetLocalGroupDel(
		server.c_str(),
		group.c_str()
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" deleting localgroup: " << ErrorString(status) << endl;
	}
	else
	{
		wcout << L"Localgroup '" << group << L"' deleted." << endl;
	}

	return status;
}


int LocalGroupGetInfo(const wstring& server, const wstring& group, int level)
{
	wcout << L"NetLocalGroupGetInfo(" << server << L", " << group << L") level " << level << endl;

	LPBYTE buffer = nullptr;

	NET_API_STATUS status = NetLocalGroupGetInfo(
		server.c_str(),
		group.c_str(),
		level,
		&buffer
	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" getting localgroup info: " << ErrorString(status) << endl;
		return status;
	}

	vector<wstring> descriptions;
	vector<wstring> text;

	LEVELMAPCHECK(NetLocalGroupGetValuesMap, level);
	NetLocalGroupGetValuesMap[level](buffer, descriptions, text);

	NetApiBufferFree(buffer);

	vector<vector<wstring>> values;
	values.push_back(descriptions);
	values.push_back(text);

	OutputTable({}, values);

	wcout << endl;

	vector<wstring> members = LocalGroupGetMembers(server, group);

	wcout << L"Members" << endl;
	wcout << wstring(20, L'-') << endl;
	for (const auto& m : members)
		wcout << m << endl;
	wcout << endl;

	if (!g_xmlFilename.empty())
	{
		XmlOutputInfo(
			g_xmlFilename,
			level,
			server,
			L"localgroup",
			{ make_pair(L"localgroup", group) },
			values,
			{ make_pair(L"member", members) }
		);
	}

	if (!g_jsonFilename.empty())
	{
		JsonOutputInfo(
			g_jsonFilename,
			level,
			server,
			L"localgroup",
			{ make_pair(L"localgroup", group) },
			values,
			{ make_pair(L"member", members) }
		);
	}

	return status;
}


