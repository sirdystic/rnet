#pragma once

/* NetGroup.h - Wrappers around NetGroup*() APIs */

#include <string>
#include <vector>

using namespace std;


extern vector<int> NetGroupEnumTryLevels;
extern vector<int> NetGroupGetInfoTryLevels;

int GroupEnum(const wstring& server, int level);
int GroupGetInfo(const wstring& server, const wstring& group, int level);
int GroupAdd(const wstring& server, const wstring& group, const wstring& comment);
int GroupDel(const wstring& server, const wstring& group);
int GroupSetInfo(const wstring& server, const wstring& group, const wstring& comment);
int GroupAddUsers(const wstring& server, const wstring& group, const vector<wstring>& users);
int GroupDelUsers(const wstring& server, const wstring& group, const vector<wstring>& users);

