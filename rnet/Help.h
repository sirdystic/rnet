#pragma once

/* Help.h - Help related functions */

#include <string>
#include <vector>

using namespace std;

void Usage();
void ShowVersion();
int Levels(const wstring& command);
int Help(const wstring& server, const vector<wstring>& arguments);

