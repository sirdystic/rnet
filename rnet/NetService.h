#pragma once

/* NetService.h - Wrappers around NetService*() APIs */

#include <string>
#include <vector>

using namespace std;


extern vector<int> NetServiceEnumTryLevels;
extern vector<int> NetServiceGetInfoTryLevels;

int ServiceEnum(const wstring& server, int level);
int ServiceGetInfo(const wstring& server, const wstring& service, int level);

