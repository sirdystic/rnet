#pragma once

/* NetDomain.h - Wrappers around domain related APIs */

#include "NullString.h"

int GetDCName(const wstring& server, const wstring& domain);
int GetAnyDCName(const wstring& server, const wstring& domain);
int JoinDomain(const wstring& server, const wstring& domain, const nullstring& user, const nullstring& password);
int UnjoinDomain(const wstring& server, const nullstring& user, const nullstring& password);

