/* NetWkstaUser.cpp - Wrappers around NetWkstaUser*() APIs */

#include "NetWkstaUser.h"
#include "Types.h"
#include "Error.h"
#include "Utils.h"
#include "Macros.h"
#include "Xml.h"
#include "Json.h"
#include "Globals.h"

#include <Windows.h>
#include <LM.h>

#include <iostream>


void GetWkstaUserInfoValues0(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetWkstaUserInfoValues1(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);
void GetWkstaUserInfoValues1101(void* buffer, vector<wstring>& descriptions, vector<wstring>& text);

vector<int> NetWkstaUserEnumTryLevels = { 0, 1 };
levelmap NetWkstaUserGetValuesMap = {
	{ 0, GetWkstaUserInfoValues0 },
	{ 1, GetWkstaUserInfoValues1 },
	{ 1101, GetWkstaUserInfoValues1101 },
};


void GetWkstaUserInfoValues0(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	LPWKSTA_USER_INFO_0 pWkstaUserInfo = reinterpret_cast<LPWKSTA_USER_INFO_0>(buffer);
	descriptions.push_back(L"Username");
	text.push_back(pWkstaUserInfo->wkui0_username);
}


void GetWkstaUserInfoValues1(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	GetWkstaUserInfoValues0(buffer, descriptions, text);

	LPWKSTA_USER_INFO_1 pWkstaUserInfo = reinterpret_cast<LPWKSTA_USER_INFO_1>(buffer);
	descriptions.push_back(L"Logon domain");
	text.push_back(pWkstaUserInfo->wkui1_logon_domain);
	descriptions.push_back(L"Other domains");
	text.push_back(pWkstaUserInfo->wkui1_oth_domains);
	descriptions.push_back(L"Logon server");
	text.push_back(pWkstaUserInfo->wkui1_logon_server);
}


void GetWkstaUserInfoValues1101(void* buffer, vector<wstring>& descriptions, vector<wstring>& text)
{
	LPWKSTA_USER_INFO_1101 pWkstaUserInfo = reinterpret_cast<LPWKSTA_USER_INFO_1101>(buffer);
	descriptions.push_back(L"Other domains");
	text.push_back(pWkstaUserInfo->wkui1101_oth_domains);
}


int WkstaUserEnum(const wstring& server, int level)
{
	wcout << L"NetWkstaUserEnum(" << server << L") level " << level << endl;

	LPBYTE buffer = nullptr;
	DWORD entriesRead = 0;
	DWORD totalEntries = 0;
	DWORD resumeHandle = 0;

	NET_API_STATUS status = NetWkstaUserEnum(
		(LPWSTR)server.c_str(),
		level,
		&buffer,
		MAX_PREFERRED_LENGTH,
		&entriesRead,
		&totalEntries,
		&resumeHandle

	);

	if (NERR_Success != status)
	{
		wcerr << L"Error " << status << L" enumerating workstation users: " << ErrorString(status) << endl;
		return status;
	}

	vector<wstring> headers;
	vector<vector<wstring>> values;

	char* ptr = reinterpret_cast<char*>(buffer);
	for (DWORD d = 0; d < entriesRead; d++)
	{
		vector<wstring> descriptions;
		vector<wstring> text;

		LEVELMAPCHECK(NetWkstaUserGetValuesMap, level);
		NetWkstaUserGetValuesMap[level](ptr, descriptions, text);

		if (0 == d)
		{
			headers = descriptions;
			values.resize(headers.size());
		}
		for (size_t n = 0; n < text.size(); n++)
		{
			values[n].push_back(text[n]);
		}

		switch (level)
		{
			POINTERSTRUCTINCCASE(0, ptr, WKSTA_USER);
			POINTERSTRUCTINCCASE(1, ptr, WKSTA_USER);
			POINTERSTRUCTINCCASE(1101, ptr, WKSTA_USER);
		default:
			wcerr << L"INTERNAL ERROR: MISSING LEVEL IN POINTER INCREMENT SWITCH" << endl;
			exit(ERROR_INTERNAL_ERROR);
			break;
		}
	}

	OutputTable(headers, values);

	if (!g_xmlFilename.empty())
	{
		XmlOutputEnum(g_xmlFilename, level, server, L"workstationuser", headers, values);
	}

	if (!g_jsonFilename.empty())
	{
		JsonOutputEnum(g_jsonFilename, level, server, L"workstationuser", headers, values);
	}

	NetApiBufferFree(buffer);

	return status;
}
