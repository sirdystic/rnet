# RNET  
## Remote NET.exe - Lan Manager API command line interface  
#### Copyright 2019, Sir Dystic of Cult of the Dead Cow  

### Introduction  

RNET expands on the functionality of the Windows system tool NET.EXE providing
a console interface to the Lan Manager network management APIs.  RNET provides
access to most of the Net*() APIs including performing these calls on remote 
hosts (hence the R in RNET) and performs information query calls at any of the 
valid data levels.

Note that most of the Lan Manager APIs are documented as being deprecated,
however all accept a remote server parameter to perform on.  The support for
any specific API may vary for both local and remote operating system versions,
but almost all APIs are included in RNET for completeness.  If a remote command
attempt fails immediately it is likely not supported by the local operating 
system, if it fails after a brief pause it is likely not supported by the 
remote operating system, assuming it fails with an error level indicating 
such.

RNET should be a useful network administration tool as well as a useful tool 
for less privileged users in both domain and non-domain network environments 
and on a local host.

RNET supports optionally outputting the data retrieved from information query 
calls to a file in either XML or JSON format.  At higher levels a LOT of information
is returned so this may be the only useful way to use this data.

The syntax of RNET is very similar to NET (in most cases compatible) and contains
an extensive help system.  Use `RNET HELP` to get started.  `RNET HELP SYNTAX` will
explain the syntax used in the help, `RNET HELP REMOTE` will give some important
basic information you need to know about performing remote commands.

### Why write a wrapper around depracted APIs?  

The functionality of this program can be accomplished using PowerShell or other 
server tools, but I thought it would be useful to have a program with the familiar
syntax of NET.EXE that worked remotely.  Also, odd and interesting things can often 
be discovered by exploring Microsoft's deprecrated interfaces.  As I was
unable to find any examples for using most of these APIs online, I suspect they have
gone largely overlooked.

### Building RNET  

This repo contains a Visual Studio 2017 solution and project.  RNET has two external 
dependencies which should be installed with vcpkg for the desired target triplets:  
- jsoncpp  
- tinyxml  

The project contains DLL and static configurations for x86 and x64 platforms.  To 
build for your desired target, install the above packages for the appropriate 
desired vcpkg triplets:  
- x86-windows  
- x86-windows-static  
- x64-windows  
- x64-windows-static  

### Running RNET  

RNET makes use of stderr and stdout appropriately and returns Win32 error levels 
that so it can be integrated into scripts.

The information retrieval commands can operate at different 'levels', each of which 
retrieves a different set of information.  For the most part the lower values simply 
return a subset of the higher values but in some cases there are level values that
will return unique information that is not returned at other levels.  The 
information returned for each level value for each item type is can be shown using
`RNET HELP LEVELS COMMAND`.  If no specific level is provided using the `/LEVEL`
option RNET will attempt at a high level and keep lowering the level until data is
retrieved but for some commands with 'special' levels not all levels are tried.  The
levels that are tried when enumerating or getting a specific item info are also
displayed with `RNET HELP LEVELS`.  They are usually but not always the same, 
because durring experimentation some levels may have failed with enumeration that 
succeeded with item specific information retrieval.

### Operations that definately work remotely  

- Remote share manipulation  
- Remote user manipulation
- Remote local group manipulation
- Remote server configuration
- Remote session management

### Examples  

#### Share manipulation  
```
C:\src\rnet\x64\Release-static>RNET \\10.0.0.167 SHARE foo=c:\foo /GRANT:Everyone,FULL
Share 'foo' added.

C:\src\rnet\x64\Release-static>RNET \\10.0.0.167 SHARE foo
NetShareGetInfo(foo) level 503
Share name     foo
Type           DISK
Remark
Maximum users  No limit
Current users  0
Path           c:\foo
Server         *
SID            non-null
Name       Type            Permission
---------------------------------------
\Everyone  WellKnownGroup  FULL

C:\src\rnet\x64\Release-static>RNET \\10.0.0.167 SHARE foo /REMARK:"Foo share"
Share 'foo' info set.

C:\src\rnet\x64\Release-static>RNET \\10.0.0.167 SHARE
NetShareEnum() level 503
Share name  Type        Remark         Maximum users  Current users  Path        Server  SID
---------------------------------------------------------------------------------------------------
ADMIN$      DISK (Sys)  Remote Admin   No limit       0              C:\Windows  *       null
C$          DISK (Sys)  Default share  No limit       0              C:\         *       null
foo         DISK        Foo share      No limit       0              c:\foo      *       non-null
IPC$        IPC$ (Sys)  Remote IPC     No limit       1                          *       null

C:\src\rnet\x64\Release-static>RNET \\10.0.0.167 SHARE foo /DELETE
Share 'foo' deleted.
```

#### User and local group manipluation
```
C:\src\rnet\x64\Release-static>rnet \\10.0.0.167 USER newuser "This is a password" /ADD
User 'newuser' added.

C:\src\rnet\x64\Release-static>rnet \\10.0.0.167 LOCALGROUP Guests newuser /ADD
1 members added to localgroup 'Guests'.

C:\src\rnet\x64\Release-static>rnet \\10.0.0.167 USER newuser /FULLNAME:"Swampy Ratty"

C:\src\rnet\x64\Release-static>rnet \\10.0.0.167 USER newuser
NetUserGetInfo(newuser) level 3
Name             newuser
Password age     16:01:46 12-31-1969
Priv lvl         Guest
Home dir
Comment
Flags            ON
Script path
Full name        Swampy Ratty
User comment
Parms
Workstations
Last logon       16:00:00 12-31-1969
Account expires  Never
Max storrage     Unlimited
Bad pw count     0
Logon count      0
Logon server     \\*
CC               0
CP               0
UserID           1003
GroupID          513
Profile
PW expired       No

Local Group membership: Guests
Global Group membership: None
```

#### Server configuration
```
C:\src\rnet\x64\Release-static>rnet \\10.0.0.167 CONFIG SERVER
NetServerGetInfo(\\10.0.0.167) level 103
Plat          WinNT
Name          10.0.0.167
Ver           6.1
Type flags    WKS SRV NT PTBRW
Comment       Blah blah blah
Max users     20
Disconnect    15
Hidden        No
Ann sec       60
Ann delt      3000
Users         0
User path     c:\
Capabilities

Clients  Transport name                                              Address  Domain
-----------------------------------------------------------------------------------------
0        \Device\NetbiosSmb                                          WIN7VM   WORKGROUP
0        \Device\NetBT_Tcpip_{B5818FDB-FAAF-428A-AE64-0742E9BE09BE}  WIN7VM   WORKGROUP

C:\src\rnet\x64\Release-static>rnet \\10.0.0.167 CONFIG SERVER /SRVCOMMENT:"Stay away" /AUTODISCONNECT:30
Server info set

C:\src\rnet\x64\Release-static>rnet \\10.0.0.167 CONFIG SERVER
NetServerGetInfo(\\10.0.0.167) level 103
Plat          WinNT
Name          10.0.0.167
Ver           6.1
Type flags    WKS SRV NT PTBRW
Comment       Stay away
Max users     20
Disconnect    30
Hidden        No
Ann sec       60
Ann delt      3000
Users         0
User path     c:\
Capabilities

Clients  Transport name                                              Address  Domain
-----------------------------------------------------------------------------------------
0        \Device\NetbiosSmb                                          WIN7VM   WORKGROUP
0        \Device\NetBT_Tcpip_{B5818FDB-FAAF-428A-AE64-0742E9BE09BE}  WIN7VM   WORKGROUP
```

#### Session management
```
C:\src\rnet\x64\Release-static>rnet \\10.0.0.167 SESSION
NetSessionEnum(, ) level 502
Computer    User  Opens  Time           Idle   Flags  Client type  Transport
------------------------------------------------------------------------------
10.0.0.241  josh  1      16 mins 0 sec  0 sec

C:\src\rnet\x64\Release-static>rnet \\10.0.0.167 SESSION \\10.0.0.241 /DELETE
Error 1726 deleting session: The remote procedure call failed.


C:\src\rnet\x64\Release-static>RNET USE
NetUseEnum() level 2
Local  Remote             Status        Type  Opens  Uses  User name:  Remote domain name:
--------------------------------------------------------------------------------------------
       \\10.0.0.167\ipc$  Disconnected  IPC   0      1     josh        JOMEN
```
(The `/DELETE` operation fails because it disconnects the session we are performing 
the action over, but you can see the status is now 'Disconnected'.)

